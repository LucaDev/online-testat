package de.fh_bielefeld.online_testat;

import de.fh_bielefeld.online_testat.model.answer.ChoiceAnswer;
import de.fh_bielefeld.online_testat.model.examiner.Examiner;
import de.fh_bielefeld.online_testat.model.question.Choice;
import de.fh_bielefeld.online_testat.model.question.MultipleChoiceQuestion;
import de.fh_bielefeld.online_testat.repositories.ExaminerRepository;
import de.fh_bielefeld.online_testat.services.AnswerRatingService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import java.util.BitSet;
import java.util.List;


/**
 * Tests basic application functions
 *
 * @author Luca Kröger
 */
@SpringBootTest
@ActiveProfiles("test")
public class OnlineTestatTests {
    @Autowired
    private AnswerRatingService answerRatingService;
    @Autowired
    private ExaminerRepository examinerRepository;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /*
        Basic context load test
     */
    @Test
    public void createContext() {}

    /*
        MultipleChoiceRatingService min-max-tests
     */
    @Test
    public void MultipleChoiceRatingServiceBounds() {
        MultipleChoiceQuestion question = new MultipleChoiceQuestion("Text", List.of(
                new Choice("A", false),
                new Choice("B", true),
                new Choice("C", false),
                new Choice("D", true)
        ), 4.0);

        ChoiceAnswer answer = new ChoiceAnswer();
        answer.setAnswers(new BitSet());
        answer.setQuestion(question);

        answerRatingService.evaluate(answer);

        Assertions.assertEquals(0d, answer.getPoints());

        answer.getAnswers().set(1, true);
        answer.getAnswers().set(3, true);

        answerRatingService.evaluate(answer);

        Assertions.assertEquals(4d, answer.getPoints());
    }

    /*
        Examiner authentication test
     */
    private final static String TEST_USERNAME = "auth";
    private final static String TEST_PASSWORD = "password";

    @Test
    public void authenticationTest() {
        examinerRepository.deleteAll();

        var password = passwordEncoder.encode(TEST_PASSWORD);

        Assertions.assertFalse(password.contains(TEST_PASSWORD));

        examinerRepository.save(new Examiner(TEST_USERNAME, password));

        var auth = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(TEST_USERNAME, TEST_PASSWORD));

        Assertions.assertNotNull(auth);
        Assertions.assertTrue(auth.isAuthenticated());

        Assertions.assertThrows(BadCredentialsException.class, () -> {
            authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(TEST_USERNAME, ""));
        });

        Assertions.assertThrows(BadCredentialsException.class, () -> {
            authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken("", ""));
        });
    }
}
