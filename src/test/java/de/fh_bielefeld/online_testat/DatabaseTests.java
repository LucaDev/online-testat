package de.fh_bielefeld.online_testat;

import de.fh_bielefeld.online_testat.model.answer.ChoiceAnswer;
import de.fh_bielefeld.online_testat.model.answer.TextAnswer;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.model.exam.Examinee;
import de.fh_bielefeld.online_testat.model.examiner.Examiner;
import de.fh_bielefeld.online_testat.model.question.*;
import de.fh_bielefeld.online_testat.repositories.ExamRepository;
import de.fh_bielefeld.online_testat.repositories.ExaminerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Runs tests against all data models and checks for consistence after saving
 *
 * @author Luca Kröger
 */
@DataJpaTest(showSql = false)
@ActiveProfiles("test")
public class DatabaseTests {
    @Autowired
    private ExamRepository examRepository;

    @Autowired
    private ExaminerRepository examinerRepository;

    private final static int NUM_EXAMINEES = 5;
    private final static int NUM_QUESTIONS = 3;

    /**
     * Creates an {{@link Exam}}, saves it to the database and retrieves it
     */
    @Test
    public void createExam() {
        var exam = new Exam();
        exam.setName("Test-exam");
        exam.setDuration(90);

        var questions = new ArrayList<Question>();
        for (int i = 0; i < NUM_QUESTIONS; i++) {
            questions.add(new TextQuestion("TXTQuestion " + i, i+1d));
            questions.add(new MultipleChoiceQuestion("MCQuestion " + i, List.of(new Choice("MCQuestion " + i, false), new Choice("MCQuestion " + i, true)), i+1d));
            questions.add(new SingleChoiceQuestion("SCQuestion " + i, List.of(new Choice("SCQuestion " + i, false), new Choice("SCQuestion " + i, true)), i+1d));
        }
        exam.setQuestions(questions);

        var examinees = new ArrayList<Examinee>();
        for (int i = 0; i < NUM_EXAMINEES; i++) {
            examinees.add(new Examinee(1000000+i, "Tester " + i));
        }

        exam.addAppointment(new ExamAppointment(examinees, LocalDateTime.now()));

        examRepository.save(exam);

        Assertions.assertEquals(1, examRepository.findAll().size());
        Assertions.assertEquals(1, examRepository.deleteByCreateTimeBefore(LocalDateTime.now()));

        TextAnswer txtAnswer = new TextAnswer(exam.getQuestions().get(0), "Test");
        ChoiceAnswer choiceAnswer = new ChoiceAnswer(exam.getQuestions().get(1));
    }


    private final static String TEST_USERNAME = "user";
    private final static String TEST_PASSWORD = "{noop]password";

    /**
     * Creates an {{@link Examiner}}, saves it to the database and retrieves it
     */
    @Test
    public void createExaminer() {
        examinerRepository.deleteAll();
        examinerRepository.save(new Examiner(TEST_USERNAME, TEST_PASSWORD));

        Assertions.assertEquals(1, examinerRepository.findAll().size());
        Assertions.assertEquals(TEST_PASSWORD, examinerRepository.findByUsernameIgnoreCase(TEST_USERNAME).getPassword());
        Assertions.assertFalse(examinerRepository.findByUsernameIgnoreCase(TEST_USERNAME).isAdmin());
    }

    /**
     * Creates an {{@link Examiner}} with the "Admin" role, saves it to the database and retrieves it
     */
    @Test
    public void createExaminerAdmin() {
        examinerRepository.deleteAll();

        var examiner = new Examiner(TEST_USERNAME, TEST_PASSWORD);
        examiner.setAdmin(true);

        examinerRepository.save(examiner);

        Assertions.assertEquals(1, examinerRepository.findAll().size());
        Assertions.assertEquals(TEST_PASSWORD, examinerRepository.findByUsernameIgnoreCase(TEST_USERNAME).getPassword());
        Assertions.assertTrue(examinerRepository.findByUsernameIgnoreCase(TEST_USERNAME).isAdmin());
    }
}
