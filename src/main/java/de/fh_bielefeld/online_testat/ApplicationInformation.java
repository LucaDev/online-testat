package de.fh_bielefeld.online_testat;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Provides basic application information like version numbers, etc.
 * @author Luca Kröger
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "application")
@Component
public class ApplicationInformation {
    /**
     * The applications' version as defined in the pom.xml
     */
    private String version;

    /**
     * The git commit the application was build with.
     */
    private String gitVersion;
}
