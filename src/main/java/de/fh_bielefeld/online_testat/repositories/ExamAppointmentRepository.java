package de.fh_bielefeld.online_testat.repositories;

import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

/**
 * Serves as the JPA repository for the {@link ExamAppointment} class
 * @author Ole Niediek
 */
public interface ExamAppointmentRepository extends JpaRepository<ExamAppointment, Long> {
    Optional<ExamAppointment> findByID(Long ID);
}
