package de.fh_bielefeld.online_testat.repositories;

import de.fh_bielefeld.online_testat.model.exam.Exam;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Serves the JPA repository for the {@link Exam} class
 *
 * @author Luca Kröger
 */
public interface ExamRepository extends JpaRepository<Exam, Long> {
    /**
     * Allows to delete entries before a given datetime
     *
     * @param createTime date of initial exam creation
     * @return number of deleted entries
     */
    long deleteByCreateTimeBefore(LocalDateTime createTime);

    Optional<Exam> findByID(Long ID);
}
