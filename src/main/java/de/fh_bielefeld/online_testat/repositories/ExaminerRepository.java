package de.fh_bielefeld.online_testat.repositories;

import de.fh_bielefeld.online_testat.model.examiner.Examiner;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Serves as the JPA repository for the {@link Examiner} class
 *
 * @author Luca Kröger
 */
public interface ExaminerRepository extends JpaRepository<Examiner, Long> {
    /**
     * Finds a user by his username ignoring the case
     *
     * @param username a case-insensitive username
     * @return returns the user of null if no user was found
     */
    Examiner findByUsernameIgnoreCase(String username);
}
