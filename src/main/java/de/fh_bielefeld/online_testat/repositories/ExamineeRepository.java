package de.fh_bielefeld.online_testat.repositories;

import de.fh_bielefeld.online_testat.model.exam.Examinee;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

/**
 * Serves as the JPA repository for the {@link Examinee} class
 * @author Ole Niediek
 */
public interface ExamineeRepository extends JpaRepository<Examinee, Long> {
    Optional<Examinee> findByID(Long ID);
}
