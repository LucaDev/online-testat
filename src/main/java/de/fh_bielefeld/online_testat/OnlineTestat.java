package de.fh_bielefeld.online_testat;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.theme.Theme;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The entry point of the application. Starts up Spring Boot.
 *
 * @author Luca Kröger
 */
@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties
@ConfigurationPropertiesScan
@Push
@Theme("generalTheme")
public class OnlineTestat extends SpringBootServletInitializer implements AppShellConfigurator {

    public static void main(String[] args) {
        SpringApplication.run(OnlineTestat.class, args);
    }

    @Override
    public void configurePage(AppShellSettings settings) {
        // Add favicon links into every page
        settings.addLink("shortcut icon", "favicon.ico");
    }

}
