package de.fh_bielefeld.online_testat.ui.security;


import com.vaadin.flow.spring.security.VaadinWebSecurityConfigurerAdapter;
import de.fh_bielefeld.online_testat.services.ExaminerDetailsService;
import de.fh_bielefeld.online_testat.ui.general.LoginView;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


/**
 * {@inheritDoc}
 *
 * Sets protected routes and configures login URLs
 *
 * @author Luca Kröger
 */
@EnableWebSecurity
@Configuration
public class WebSecurityConfiguration extends VaadinWebSecurityConfigurerAdapter {
    final ExaminerDetailsService examinerDetailsService;
    final static String IMPERSONATION_EXIT_URL = "/logout/impersonate";
    final static String IMPERSONATION_START_URL = "/login/impersonate";

    public WebSecurityConfiguration(ExaminerDetailsService examinerDetailsService) {
        this.examinerDetailsService = examinerDetailsService;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Require login to access internal pages and configure login form.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            // Allow same-origin x-frames for the h2-console to work
            .headers().frameOptions().sameOrigin()
            // Add impersonation filter and authorize requests
            .and().addFilterAfter(getSwitchUserFilter(), FilterSecurityInterceptor.class)
            .authorizeRequests()
                .antMatchers(IMPERSONATION_START_URL+"*").hasRole(Roles.Constants.ADMIN_VALUE)
                .antMatchers(IMPERSONATION_EXIT_URL+"*").authenticated();

        // Set the login view for VAADINs WebSecurityConfigurerAdapter
        setLoginView(http, LoginView.class);

        super.configure(http);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // Configure spring security to ignore the h2-console
        web.ignoring().antMatchers(
                "/h2-console/**",
                "/actuator/**"
        );

        super.configure(web);
    }

    /**
     * Provides the application-wide password encoder
     * @return the current instance of the password encoder
     */
    @Bean
    public PasswordEncoder getEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    /**
     * Provides the switch user filter which is used for impersonation
     * @return the current instance of the filter
     */
    @Bean
    public SwitchUserFilter getSwitchUserFilter() {
        SwitchUserFilter filter = new SwitchUserFilter();
        filter.setUserDetailsService(examinerDetailsService);
        filter.setSwitchUserMatcher(new AntPathRequestMatcher(IMPERSONATION_START_URL, "GET"));
        filter.setExitUserMatcher(new AntPathRequestMatcher(IMPERSONATION_EXIT_URL, "GET"));
        filter.setTargetUrl("/");
        return filter;
    }
}
