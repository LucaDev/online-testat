package de.fh_bielefeld.online_testat.ui.security;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinServletRequest;
import de.fh_bielefeld.online_testat.model.examiner.Examiner;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

/**
 * SecurityUtils takes care of all such static operations that have to do with
 * security and querying rights from different beans of the UI.
 * @author Luca Kröger
 */
public final class SecurityUtils {
    private static final String LOGOUT_SUCCESS_URL = "/";

    /**
     * Tests if a user is authenticated. As Spring Security always will create an {@link AnonymousAuthenticationToken}
     * we have to ignore those tokens explicitly.
     */
    public static boolean isUserLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null
                && !(authentication instanceof AnonymousAuthenticationToken)
                && authentication.isAuthenticated();
    }

    /**
     * Checks whether a examiner is impersonating someone other examiner
     * @return true if the user is currently impersonating someone else
     */
    public static boolean isImpersonating() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_PREVIOUS_ADMINISTRATOR"));
    }

    /**
     * Returns the currently logged in user
     * @return the {@link Examiner} object of the currently logged-in user. null if the user is not logged in
     */
    public static Examiner getAuthenticatedUser() {
        var context = SecurityContextHolder.getContext();
        var principal = context.getAuthentication().getPrincipal();

        if (principal instanceof Examiner examiner) {
            return examiner;
        }

        // Not logged in
        return null;
    }

    /**
     * Logs out the current examiner or ends the current impersonation if currently active
     */
    public static void logoutUser(){
        if(isImpersonating()) {
            UI.getCurrent().getPage().setLocation(WebSecurityConfiguration.IMPERSONATION_EXIT_URL);
            return;
        }

        UI.getCurrent().getPage().setLocation(LOGOUT_SUCCESS_URL);

        SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
        logoutHandler.logout(
                VaadinServletRequest.getCurrent().getHttpServletRequest(),
                null,
                null
        );
    }
}
