package de.fh_bielefeld.online_testat.ui.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Provides all available roles (e.g. ADMIN)
 * @author Luca Kröger
 */
public enum Roles {
    ADMIN(Constants.ADMIN_VALUE);

    Roles(String adminValue) {}

    public static SimpleGrantedAuthority getAuthority(Roles role) {
        return new SimpleGrantedAuthority("ROLE_" + role.name());
    }

    public static class Constants {
        public static final String ADMIN_VALUE = "ADMIN";
    }
}
