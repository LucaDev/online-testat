package de.fh_bielefeld.online_testat.ui.utils;

import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;

import java.time.LocalDateTime;

/**
 * methods to generate specific badges
 *
 * @author Lucie Jahn
 */
public class BadgeUtils {
    /**
     * @return checkIcon
     */
    public static Icon getCheckIconBadge() {
        Icon checkIcon = VaadinIcon.CHECK.create();
        checkIcon.getStyle().set("padding", "var(--lumo-space-xs");
        checkIcon.getElement().getThemeList().add("badge success small");

        return checkIcon;
    }

    /**
     * @return closeIcon
     */
    public static Icon getCloseIconBadge() {
        Icon closeIcon = VaadinIcon.CLOSE.create();
        closeIcon.getStyle().set("padding", "var(--lumo-space-xs");
        closeIcon.getElement().getThemeList().add("badge error small");

        return closeIcon;
    }

    /**
     * generates badge for exam started status
     *
     * @param appointment
     * @return statusBadge
     */
    public static Span getExamStatusBadge(ExamAppointment appointment) {
        Span statusBadge = new Span();
        statusBadge.getStyle().set("padding", "var(--lumo-space-xs");

        if (LocalDateTime.now().isAfter(appointment.getPlannedStartTime()) && !appointment.hasStarted()) {
            statusBadge.add("Wird bald gestartet");
            statusBadge.getElement().getThemeList().add("badge contrast");
        } else if (appointment.hasStarted()) {
            statusBadge.add("gestartet");
            statusBadge.getElement().getThemeList().add("badge");
        }
        return statusBadge;
    }

    /**
     * @return liveBadge
     */
    public static Span getLiveBadge() {
        Span liveBadge = new Span(" Live ");
        liveBadge.getStyle().set("margin", "var(--lumo-space-xs");
        liveBadge.getElement().getThemeList().add("badge error");

        return liveBadge;
    }

    /**
     * configures a header span with given text  & count number
     *
     * @param text
     * @param count
     * @return headerSpan
     */
    public static Span getCountSummaryBadge(String text, double count) {
        Span countBadge = new Span();
        //only parse to int if there are no decimal places
        if (count % 1 == 0) {
            countBadge.add(String.valueOf((int) count));
        } else {
            countBadge.add(String.valueOf(count));
        }
        countBadge.getElement().getThemeList().add("badge pill small contrast");
        countBadge.getStyle().set("margin-inline-start", "var(--lumo-space-s)");

        Span headerSpan = new Span(text);
        headerSpan.add(countBadge);
        return headerSpan;
    }
}
