package de.fh_bielefeld.online_testat.ui.utils;

import com.vaadin.flow.component.crud.CrudI18n;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.richtexteditor.RichTextEditor;
import com.vaadin.flow.component.upload.UploadI18N;
import lombok.Getter;

import java.util.Arrays;

/**
 * Provides static translations for various VAADIN components
 *
 * @author Luca Kröger, Josh Knipping
 */
public class I18nUtils {
    @Getter
    private final static CrudI18n crudI18n;
    @Getter
    private final static LoginI18n loginI18n;
    @Getter
    private final static RichTextEditor.RichTextEditorI18n richtexteditorI18n;
    @Getter
    private final static UploadI18N uploadI18n;

    static {
        // CRUD Component
        crudI18n = CrudI18n.createDefault();

        crudI18n.setNewItem("Hinzufügen");
        crudI18n.setEditItem("Bearbeiten");
        crudI18n.setSaveItem("Speichern");
        crudI18n.setCancel("Abbrechen");
        crudI18n.setDeleteItem("Löschen");
        crudI18n.setEditLabel("Bearbeiten");

        CrudI18n.Confirmations.Confirmation delete = crudI18n.getConfirm().getDelete();
        delete.setTitle("Löschen");
        delete.setContent("Sind Sie sicher, dass Sie den Eintrag löschen wollen?");
        delete.getButton().setConfirm("Löschen");
        delete.getButton().setDismiss("Abbrechen");

        CrudI18n.Confirmations.Confirmation cancel = crudI18n.getConfirm().getCancel();
        cancel.setTitle("Abbrechen!");
        cancel.setContent("Sind Sie sicher, dass Sie abbrechen wollen?");
        cancel.getButton().setConfirm("Abbrechen");
        cancel.getButton().setDismiss("Weiter bearbeiten");


        // Login Component
        loginI18n = LoginI18n.createDefault();

        LoginI18n.Form i18nForm = loginI18n.getForm();
        i18nForm.setTitle("Login");
        i18nForm.setUsername("Benutzername");
        i18nForm.setPassword("Passwort");
        i18nForm.setSubmit("Anmelden");

        LoginI18n.ErrorMessage i18nErrorMessage = loginI18n.getErrorMessage();
        i18nErrorMessage.setTitle("Anmeldung fehlgeschlagen");
        i18nErrorMessage.setMessage("Benutzername oder Passwort falsch. Bitte überprüfen Sie Ihre Eingabe.");

        // Richtext Editor
        richtexteditorI18n = new RichTextEditor.RichTextEditorI18n();

        richtexteditorI18n.setUndo("Rückgängig");
        richtexteditorI18n.setRedo("Wiederholen");
        richtexteditorI18n.setBold("Fett");
        richtexteditorI18n.setItalic("Kursiv");
        richtexteditorI18n.setUnderline("Unterstreichen");
        richtexteditorI18n.setStrike("Durchstreichen");
        richtexteditorI18n.setH1("Überschrift 1");
        richtexteditorI18n.setH2("Überschrift 2");
        richtexteditorI18n.setH3("Überschrift 3");
        richtexteditorI18n.setSubscript("Tiefstellen");
        richtexteditorI18n.setSuperscript("Hochstellen");
        richtexteditorI18n.setListOrdered("Nummerierte Liste");
        richtexteditorI18n.setListBullet("Spiegelstrichliste");
        richtexteditorI18n.setAlignLeft("Linksbündig");
        richtexteditorI18n.setAlignCenter("Zentriert");
        richtexteditorI18n.setAlignRight("Rechtsbündig");
        richtexteditorI18n.setImage("Bild");
        richtexteditorI18n.setLink("Link");
        richtexteditorI18n.setBlockquote("Blockzitat");
        richtexteditorI18n.setCodeBlock("Codeblock");
        richtexteditorI18n.setClean("Formatierung entfernen");

        // Upload Component
        uploadI18n = new UploadI18N();
        uploadI18n.setDropFiles(new UploadI18N.DropFiles()
                .setOne("Datei hier ablegen")
                .setMany("Dateien hier ablegen"));
        uploadI18n.setAddFiles(new UploadI18N.AddFiles()
                .setOne("Dateien auswählen...")
                .setMany("Dateien hochladen..."));
        uploadI18n.setError(new UploadI18N.Error()
                .setTooManyFiles("Zu viele Dateien.")
                .setFileIsTooBig("Datei ist zu groß.")
                .setIncorrectFileType("Falscher Dateityp."));
        uploadI18n.setUploading(new UploadI18N.Uploading()
                .setStatus(new UploadI18N.Uploading.Status()
                        .setConnecting("Verbinde...")
                        .setStalled("Verzögert")
                        .setProcessing("Verarbeite Datei...")
                        .setHeld("In Warteschlage"))
                .setRemainingTime(new UploadI18N.Uploading.RemainingTime()
                        .setPrefix("Verbleibende Zeit: ")
                        .setUnknown("Verbleibende Zeit unbekannt"))
                .setError(new UploadI18N.Uploading.Error()
                        .setServerUnavailable("Server nicht verfügbar")
                        .setUnexpectedServerError("Unerwarteter Fehler")
                        .setForbidden("Abgelehnt")));
        uploadI18n.setUnits(new UploadI18N.Units()
                .setSize(Arrays.asList("B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")));
    }
}
