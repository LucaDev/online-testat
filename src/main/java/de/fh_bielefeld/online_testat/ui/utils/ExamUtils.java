package de.fh_bielefeld.online_testat.ui.utils;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.router.BeforeEnterEvent;
import de.fh_bielefeld.online_testat.model.answer.Answer;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.model.exam.Examinee;
import de.fh_bielefeld.online_testat.model.question.Choice;
import de.fh_bielefeld.online_testat.model.question.QuestionType;
import de.fh_bielefeld.online_testat.repositories.ExamAppointmentRepository;
import de.fh_bielefeld.online_testat.repositories.ExamRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Component
public class ExamUtils {
    private final ExamRepository examRepository;
    private final ExamAppointmentRepository examAppointmentRepository;
    private final Logger logger;

    public ExamUtils(ExamRepository examRepository, ExamAppointmentRepository examAppointmentRepository) {
        this.examRepository = examRepository;
        this.examAppointmentRepository = examAppointmentRepository;
        logger = LoggerFactory.getLogger(ExamUtils.class);
    }

    /**
     * format displayed text form for question types in answersGrid in EvautationExamView
     *
     * @param answer
     * @return html builder
     * @author Lucie Jahn
     */
    public static Html formatAnswer(Answer answer) {
        StringBuilder builder = new StringBuilder("<div>");
        if (answer.getQuestion().getType() == QuestionType.TEXT) {
            builder.append(formatTextAnswer(answer));
        } else {
            builder.append("...");
        }
        builder.append("</div>");
        return new Html(builder.toString());
    }

    /**
     * get only string from text answer
     *
     * @param answer
     * @return string builder
     * @author Lucie Jahn
     */
    public static StringBuilder formatTextAnswer(Answer answer) {
        StringBuilder builder = new StringBuilder();
        String[] splittedString = answer.toString().split("=", 2);
        builder.append(splittedString[1], 0, splittedString[1].length() - 1);

        return builder;
    }

    /**
     * format question type to icon
     *
     * @param answer
     * @return icon
     * @author Lucie Jahn
     */
    public static Icon formatQuestionTypeToIcon(Answer answer) {
        return switch (answer.getQuestion().getType()) {
            case TEXT -> VaadinIcon.INPUT.create();
            case SINGLE_CHOICE -> VaadinIcon.MINUS.create();
            case MULTIPLE_CHOICE -> VaadinIcon.MENU.create();
        };
    }

    /**
     * format Boolean true to Check icon & Boolean false to Close icon
     *
     * @param choice
     * @return icon
     * @author Lucie Jahn
     */
    public static Icon formatChoiceAnswersToIcon(Choice choice) {
        Icon icon;
        if (choice.isCorrectAnswer()) {
            icon = BadgeUtils.getCheckIconBadge();
        } else {
            icon = BadgeUtils.getCloseIconBadge();
        }
        return icon;
    }

    public Optional<Exam> getExamFromBeforeEvent(BeforeEnterEvent event) {
        Optional<String> examParam = event.getRouteParameters().get("examID");

        if (examParam.isEmpty())
            return Optional.empty();

        //  Exam was specified => try to find it, error otherwise
        try {
            long examID = Long.parseLong(examParam.get());
            Optional<Exam> loadedExam = examRepository.findByID(examID);

            if (loadedExam.isPresent())
                return loadedExam;
        } catch (NumberFormatException ex) {
            Notification.show("Prüfungsnummer-Format nicht valide!")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
            return Optional.empty();
        }

        Notification.show("Prüfung konnte nicht gefunden werden!")
                .addThemeVariants(NotificationVariant.LUMO_ERROR);

        return Optional.empty();
    }

    /**
     * @param event Method is getting called in views while they get build
     *              Loads examAppointments from the repository depending on the current route
     *              The method is used in views whose route contains examAppointmentIDs
     * @return returns the loaded examAppointment
     * @author Josh Knipping
     */
    public Optional<ExamAppointment> getExamAppointmentFromBeforeEvent(BeforeEnterEvent event) {
        Optional<String> examAppointmentParam = event.getRouteParameters().get("examAppointmentID");

        if (examAppointmentParam.isEmpty())
            return Optional.empty();

        try {
            long examAppointmentID = Long.parseLong(examAppointmentParam.get());
            Optional<ExamAppointment> loadedExamAppointment = examAppointmentRepository.findByID(examAppointmentID);

            if (loadedExamAppointment.isPresent())
                return loadedExamAppointment;

        } catch (NumberFormatException ex) {
            Notification.show("Terminnummer nicht valide!")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
            logger.error("Unable to parse examAppointmentID", ex);
        }
        Notification.show("Termin konnte nicht gefunden werden!")
                .addThemeVariants(NotificationVariant.LUMO_ERROR);
        return Optional.empty();
    }

    /**
     * @param examinee a specific examinee from an appointment
     *                 Calculates the result of an examinee in percent.
     *                 The reached points get divided by the possible Points per question/answer, summed up and multiplied by 100 to get the percentage value
     * @return returns calculated percentage value plus % as a String
     * @author Josh Knipping
     */
    public static String getPercentage(Examinee examinee) {
        double examineePoints = 0;
        double possiblePoints = 0;
        for (int i = 0; i < examinee.getAnswers().size(); i++) {
            if(examinee.getAnswers().get(i).getPoints() >= 0){
                examineePoints += examinee.getAnswers().get(i).getPoints();
            }
            possiblePoints += examinee.getAnswers().get(i).getQuestion().getMaxPoints();
        }
        if (examineePoints == 0) {
            return "0,0%";
        } else {
            double result = examineePoints / possiblePoints * 100;
            return String.format("%.1f", result) + "%";
        }
    }

    /**
     * @param examinee a specific examinee from an appointment
     *                 Calculates the grade of an examinee
     *                 The reached points get divided by the possible Points per question/answer, summed up and multiplied by 100 to get the percentage value
     *                 the grade results from the percent value
     * @return returns
     * @author Josh Knipping
     */
    public static Double getGrade(Examinee examinee) {
        double result = getResult(examinee);

        return getGrade(result);
    }

    private static double getResult(Examinee examinee) {
        double examineePoints = 0;
        double possiblePoints = 0;

        for (int i = 0; i < examinee.getAnswers().size(); i++) {
            if(examinee.getAnswers().get(i).getPoints() >= 0){
                examineePoints += examinee.getAnswers().get(i).getPoints();
            }
            possiblePoints += examinee.getAnswers().get(i).getQuestion().getMaxPoints();
        }
        double result = examineePoints / possiblePoints * 100;
        return result;
    }

    private static double getGrade(double result) {
        double grade = 0;
        if (result > 94.9) {
            grade = 1.0;
        } else if (result > 89.5) {
            grade = 1.3;
        } else if (result > 84.3) {
            grade = 1.7;
        } else if (result > 79.0) {
            grade = 2.0;
        } else if (result > 73.7) {
            grade = 2.3;
        } else if (result > 68.2) {
            grade = 2.7;
        } else if (result > 63.1) {
            grade = 3.0;
        } else if (result > 57.9) {
            grade = 3.3;
        } else if (result > 52.6) {
            grade = 3.7;
        } else if (result >= 50.0) {
            grade = 4.0;
        } else if (result < 50.0) {
            grade = 5.0;
        }
        return grade;
    }

    /**
     * @param examineeList a List of examinees
     *                     Calculates the average result of an all examinees in percent.
     *                     The combined reached points and the possible points for all examinees get summed.
     *                     The reached points divided by the possible points multiplied by 100 gets the average in percent
     * @return returns calculated average in percent from all examinees plus % as a String. If no examinee participant, a placeholder will get returned
     * @author Josh Knipping
     */
    public static String getPercentAverage(Collection<Examinee> examineeList) {
        double examineePoints;
        double totalExamineePoints = 0;
        double possiblePoints;
        double totalPossiblePoints = 0;

        for (Examinee examinee : examineeList) {
            if (examinee.getAnswers().size() != 0) {
                for (int i = 0; i < examinee.getAnswers().size(); i++) {
                    if(examinee.getAnswers().get(i).getPoints() >= 0){
                        examineePoints = examinee.getAnswers().get(i).getPoints();
                        totalExamineePoints += examineePoints;
                    }
                    possiblePoints = examinee.getAnswers().get(i).getQuestion().getMaxPoints();
                    totalPossiblePoints += possiblePoints;
                }
            }
        }

        double result = totalExamineePoints / totalPossiblePoints * 100;
        if (Double.isNaN(result)) {
            return "-";
        } else {
            return String.format("%.1f", result) + "%";
        }
    }

    /**
     * @param examineeList a List of examinees
     *                     Calculates the average grade of an all examinees
     *                     The combined reached points and the possible points for all examinees get summed.
     *                     The reached points divided by the possible points multiplied by 100 gets the average in percent
     *                     The average grade results from the percent value
     * @return returns calculated average grade for all examinees as a String. If no examinee participant, a placeholder will get returned
     * @author Josh Knipping
     */
    public static String getGradeAverage(Collection<Examinee> examineeList) {
        double totalGrade = 0;
        double examineeCount = 0;

        for (Examinee examinee : examineeList) {
            if (examinee.getAnswers().size() != 0) {
                double result = getResult(examinee);
                examineeCount++;

                double grade = getGrade(result);
                totalGrade += grade;
            }
        }
        double averageGrade = totalGrade / examineeCount;
        if (Double.isNaN(averageGrade)) {
            return "-";
        } else
            return String.valueOf(averageGrade).substring(0,3);
    }

    /**
     * calculation of endTime of an appointment
     *
     * @param startTime   -> can be the planned or actual start time of the appointment
     * @param appointment
     * @return endTime
     * @author Lucie Jahn
     */
    public static LocalDateTime getAppointmentEndTime(LocalDateTime startTime, ExamAppointment appointment) {
        Duration duration = Duration.ofMinutes(appointment.getExam().getDuration());

        return (LocalDateTime) duration.addTo(startTime);
    }
}
