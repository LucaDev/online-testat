package de.fh_bielefeld.online_testat.ui.utils;

import com.vaadin.flow.component.Html;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

/**
 * methods to formatting dates
 *
 * @author Lucie Jahn, Luca Kröger
 */
public class DateTimeUtils {

    /**
     * format displayed appointments for exams in MyExamsView
     * -> only not fully checked appointments will be added to displayed list
     * -> exam with formatted appointments will be dispalyed in the openExamsGrid
     *
     * @param exam
     * @return html
     * @author Lucie Jahn
     */
    public static Html formatOpenExamAppointments(Exam exam) {
        StringBuilder builder = new StringBuilder("<div>");
        int appointmentCount = 0;
        if (exam.getAppointments() != null) {
            Iterator<ExamAppointment> iterator = exam.getAppointments().iterator();

            while (iterator.hasNext()) {
                ExamAppointment appointment = iterator.next();
                appointmentCount++;
                if (!appointment.isFullyChecked()) {
                    builder.append(formatAppointmentDate(appointment));

                    if (iterator.hasNext()) {
                        builder.append("<br>");
                    }
                }
            }
            // check if length 5, because "<div>" = 5 chars
            if (builder.length() == 5 && appointmentCount > 0) {
                //if all appointments are fully checked
                builder.append("kein Termin");
            } else if (builder.length() == 5 && appointmentCount == 0) {
                //if exam has no appointments
                builder.append("-");
            }
        }
        builder.append("</div>");
        return new Html(builder.toString());
    }

    /**
     * format displayed appointments for exams in MyExamsView
     * -> only fully checked appointments will be added to displayed list
     * -> exam with formatted appointments will be dispalyed in the evaluatedExamsGrid
     *
     * @param exam
     * @return html
     * @author Lucie Jahn
     */
    public static Html formatEvaluatedExamAppointments(Exam exam) {
        StringBuilder builder = new StringBuilder("<div>");
        if (exam.getAppointments() != null) {
            Iterator<ExamAppointment> iterator = exam.getAppointments().iterator();
            while (iterator.hasNext()) {
                ExamAppointment appointment = iterator.next();
                if (appointment.isFullyChecked()) {
                    builder.append(formatAppointmentDate(appointment));

                    if (iterator.hasNext()) {
                        builder.append("<br>");
                    }
                }
            }
            // check if length 5, because "<div>" = 5 chars
            if (builder.length() == 5) {
                builder.append("kein Termin");
            }
        }
        builder.append("</div>");
        return new Html(builder.toString());
    }

    public static String formatAppointmentDate(ExamAppointment appointment) {
        return formatDateTime(appointment.getPlannedStartTime());
    }

    public static String formatDateTime(LocalDateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy - HH:mm");
        return formatter.format(dateTime);
    }

    public static String formatTime(LocalDateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        return formatter.format(dateTime);
    }

    public static String formatDuration(Exam exam) {
        return exam.getDuration() + " " + (exam.getDuration() == 1 ? "Minute" : "Minuten");
    }
}
