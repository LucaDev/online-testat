package de.fh_bielefeld.online_testat.ui.examiner.evaluateExam;

import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import de.fh_bielefeld.online_testat.model.answer.Answer;
import de.fh_bielefeld.online_testat.model.answer.ChoiceAnswer;
import de.fh_bielefeld.online_testat.model.answer.TextAnswer;
import de.fh_bielefeld.online_testat.model.question.Choice;
import de.fh_bielefeld.online_testat.model.question.ChoiceQuestion;
import de.fh_bielefeld.online_testat.ui.utils.BadgeUtils;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;
import org.jsoup.Jsoup;

import java.util.stream.Stream;

/**
 * set up form layout to display all the answer details in the answersGrid of EvaluationExamView
 *
 * @author Lucie Jahn
 */
public class AnswerDetailsFormLayout extends FormLayout {
    private final TextField type = new TextField("Aufgabentyp");
    private final TextField question = new TextField("Aufgabe");
    private final Grid<Choice> choices = new Grid<>(Choice.class);
    private final TextField textAnswer = new TextField("Antwort (Freitext)");

    /**
     * set Layout for answer details form
     */
    public AnswerDetailsFormLayout() {
        Stream.of(type, question).forEach(field -> {
            field.setReadOnly(true);
            field.addThemeVariants(TextFieldVariant.LUMO_SMALL);
            add(field);
        });

        setResponsiveSteps(new ResponsiveStep("0", 2));
        setColspan(type, 3);
        setColspan(question, 3);
    }

    /**
     * set content for answer details
     * @param answer
     */
    public void setAnswerDetails(Answer answer) {
        question.setValue(Jsoup.parse(answer.getQuestion().getQuestionHTML()).text());
        type.setValue(answer.getQuestion().getType().getName());

        if(answer instanceof ChoiceAnswer choiceAnswer){
            getChoiceAnswerDetails(answer, choiceAnswer);
        } else if(answer instanceof TextAnswer){
            getTextAnswerDetails((TextAnswer) answer);
        }
        choices.setAllRowsVisible(true);
    }

    /**
     * set up content for text answer details
     * @param answer
     */
    private void getTextAnswerDetails(TextAnswer answer) {
        textAnswer.setValue(answer.getAnswer());
        textAnswer.setReadOnly(true);
        setColspan(textAnswer, 3);
        add(textAnswer);
    }

    /**
     * set up content for choice answer details
     * -> all correct answers & examinee answers will be displayed side by side in a grid
     * @param answer
     * @param choiceAnswer
     */
    private void getChoiceAnswerDetails(Answer answer, ChoiceAnswer choiceAnswer) {
        var gridTitle = new Span("Antwort");
        choices.setItems(((ChoiceQuestion) answer.getQuestion()).getChoices());
        choices.setColumns("text");
        choices.getColumnByKey("text").setHeader("Aufgabe");
        choices.addComponentColumn(ExamUtils::formatChoiceAnswersToIcon).setHeader("Richtige Antwort");

        choices.addComponentColumn(choice -> {
            var studentChoiceAnswer = choiceAnswer.getAnswers().get(((ChoiceQuestion) answer.getQuestion()).getChoices().indexOf(choice));

            if(studentChoiceAnswer){
                return BadgeUtils.getCheckIconBadge();
            } else {
                return BadgeUtils.getCloseIconBadge();
            }
        }).setHeader("Antwort Teilnehmer");
        setColspan(choices, 3);
        add(gridTitle, choices);
    }
}
