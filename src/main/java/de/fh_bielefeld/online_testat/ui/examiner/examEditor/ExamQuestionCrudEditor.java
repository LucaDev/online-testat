package de.fh_bielefeld.online_testat.ui.examiner.examEditor;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.crud.CrudEditor;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.dnd.GridDropLocation;
import com.vaadin.flow.component.grid.dnd.GridDropMode;
import com.vaadin.flow.component.gridpro.GridPro;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.richtexteditor.RichTextEditor;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import de.fh_bielefeld.online_testat.model.question.*;
import de.fh_bielefeld.online_testat.ui.utils.I18nUtils;
import org.jsoup.Jsoup;

import java.util.ArrayList;

/**
 * @author Luca Kröger
 */
public class ExamQuestionCrudEditor implements CrudEditor<Question> {
    private Choice dragChoice;
    private final FormLayout layout = new FormLayout();
    private VerticalLayout choiceLayout;
    private Question question;

    private final BeanValidationBinder<Question> questionBinder = new BeanValidationBinder<>(Question.class);
    private final BeanValidationBinder<ChoiceQuestion> choiceBinder = new BeanValidationBinder<>(ChoiceQuestion.class);

    private final RichTextEditor questionEditor;

    private final boolean mixUp;

    public ExamQuestionCrudEditor(boolean draggable) {
        this.mixUp = draggable;
        layout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 2));

        Label questionLabel = new Label("Frage");
        questionEditor = new RichTextEditor();
        questionEditor.setI18n(I18nUtils.getRichtexteditorI18n());

        NumberField maxPointField = new NumberField("Maximale Punktzahl");
        maxPointField.setStep(0.5);
        maxPointField.setMin(0.5);
        maxPointField.setHasControls(true);
        questionBinder.bind(maxPointField, "maxPoints");

        ComboBox<QuestionType> questionType = new ComboBox<>("Typ");
        questionType.setItems(QuestionType.values());
        questionType.setItemLabelGenerator(QuestionType::getName);
        questionBinder.bind(questionType, Question::getType, (question, type) -> {});

        /* Add everything to the form layout */
        layout.add(questionLabel, questionEditor, maxPointField, questionType);
        layout.setColspan(questionLabel, 2);
        layout.setColspan(questionEditor, 2);

        // Convert between the question types, preserving the most values
        questionType.addValueChangeListener(valueChangeEvent -> {
            removeChoiceLayout();

            switch (valueChangeEvent.getValue()) {
                case SINGLE_CHOICE -> {
                    if(question.getType() == QuestionType.TEXT) {
                        question = new SingleChoiceQuestion(question.getQuestionHTML(), new ArrayList<>(), question.getMaxPoints());
                        questionBinder.setBean(question);
                    } else if (question.getType() == QuestionType.MULTIPLE_CHOICE) {
                        question = new SingleChoiceQuestion(question.getQuestionHTML(), ((MultipleChoiceQuestion)question).getChoices(), question.getMaxPoints());
                        questionBinder.setBean(question);
                    }

                    addChoiceLayout();
                }
                case MULTIPLE_CHOICE -> {
                    if(question.getType() == QuestionType.TEXT) {
                        question = new MultipleChoiceQuestion(question.getQuestionHTML(), new ArrayList<>(), question.getMaxPoints());
                        questionBinder.setBean(question);
                    } else if (question.getType() == QuestionType.SINGLE_CHOICE) {
                        question = new MultipleChoiceQuestion(question.getQuestionHTML(), ((SingleChoiceQuestion)question).getChoices(), question.getMaxPoints());
                        questionBinder.setBean(question);
                    }

                    addChoiceLayout();
                }
                case TEXT -> {
                    if(question.getType() != QuestionType.TEXT)
                        question = new TextQuestion(question.getQuestionHTML(), question.getMaxPoints());
                    questionBinder.setBean(question);
                }
            }
        });
    }

    private void addChoiceLayout() {
        setupChoiceLayout();
        layout.add(choiceLayout);
        layout.setColspan(choiceLayout, 2);
    }

    private void removeChoiceLayout() {
        if(choiceLayout != null) {
            layout.remove(choiceLayout);
            choiceLayout = null;
        }
    }

    private void setupChoiceLayout() {
        choiceBinder.setBean((ChoiceQuestion) question);

        choiceLayout = new VerticalLayout();

        GridPro<Choice> choiceGrid = new GridPro<>();
        choiceGrid.setSelectionMode(Grid.SelectionMode.NONE);
        choiceGrid.setAllRowsVisible(true);

        /* Row Drag */
        // Make rows draggable when mixing is disabled
        choiceGrid.setRowsDraggable(!mixUp);

        choiceGrid.addDragStartListener(event -> {
            choiceGrid.setDropMode(GridDropMode.BETWEEN);

            dragChoice = event.getDraggedItems().get(0);
        });

        choiceGrid.addDragEndListener(event -> {
            choiceGrid.setDropMode(null);

            dragChoice = null;
        });

        choiceGrid.addDropListener(event -> {
            if(event.getDropTargetItem().isEmpty())
                return;

            Choice choice = event.getDropTargetItem().get();

            if(choice.equals(dragChoice))
                return;

            var choices = ((ChoiceQuestion)question).getChoices();

            choices.remove(dragChoice);
            int dropIndex = choices.indexOf(choice) + (event.getDropLocation() == GridDropLocation.BELOW ? 1 : 0);
            choices.add(dropIndex, dragChoice);
            choiceGrid.getDataProvider().refreshAll();
        });

        /* Setup columns */
        choiceGrid.removeAllColumns();
        choiceGrid.addEditColumn(Choice::getText)
                .text((choice, text) -> {
                    choice.setText(text);
                    choiceGrid.recalculateColumnWidths();
                })
                .setHeader("Möglichkeit")
                .setAutoWidth(true);
        choiceGrid.addEditColumn(choice -> choice.isCorrectAnswer() ? "Richtig" : "Falsch").
                checkbox((selectedChoice, answer) -> {
                    if(answer && question.getType() == QuestionType.SINGLE_CHOICE) {
                        ((SingleChoiceQuestion)question).getChoices().forEach(choice -> choice.setCorrectAnswer(false));
                    }

                    selectedChoice.setCorrectAnswer(answer);

                    choiceGrid.getDataProvider().refreshAll();
                })
                .setHeader("Antwort")
                .setAutoWidth(true);
        choiceGrid.addComponentColumn(choice -> {
                Button deleteBtn = new Button(VaadinIcon.TRASH.create());

                var choices = ((ChoiceQuestion)question).getChoices();

                deleteBtn.addClickListener(event -> {
                    choices.remove(choice);
                    choiceGrid.getDataProvider().refreshAll();
                });

                return deleteBtn;
            })
            .setAutoWidth(true)
            .setTextAlign(ColumnTextAlign.END);

        choiceGrid.setEditOnClick(true);
        choiceGrid.setItems(((ChoiceQuestion)question).getChoices());

        Button addChoiceBtn = new Button(VaadinIcon.PLUS.create());
        addChoiceBtn.addClickListener(buttonClickEvent -> {
            ((ChoiceQuestion)question).getChoices().add(new Choice("Neue Antwortmöglichkeit", false));
            choiceGrid.getDataProvider().refreshAll();
        });

        choiceLayout.add(choiceGrid, addChoiceBtn);
    }

    @Override
    public void setItem(Question question, boolean validateOnLoad) {
        this.question = question;
        questionBinder.setBean(question);
        questionEditor.asHtml().setValue(question.getQuestionHTML());

        if(validateOnLoad)
            questionBinder.validate();
    }

    @Override
    public Question getItem() {
        return question;
    }

    @Override
    public void clear() {}

    @Override
    public boolean validate() {
        if(question.getType() == QuestionType.SINGLE_CHOICE || question.getType() == QuestionType.MULTIPLE_CHOICE) {
            if(((ChoiceQuestion)question).getChoices().size() < 2) {
                Notification.show("Es muss mindestens zwei Antwortmöglichkeiten geben")
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);;
                return false;
            }
        }

        if (question.getType() == QuestionType.SINGLE_CHOICE) {
            var correctChoices = ((SingleChoiceQuestion) question).getChoices().stream().filter(Choice::isCorrectAnswer).count();

            if (correctChoices != 1) {
                Notification.show("Es darf nur eine korrekte Antwort geben")
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);;
                return false;
            }
        }

        if(questionEditor.asHtml().getValue().isEmpty() || Jsoup.parse(questionEditor.asHtml().getValue()).text().length() < 1) {
            Notification.show("Der Fragetext darf nicht leer sein.")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
            return false;
        }

        return questionBinder.validate().isOk() && choiceBinder.validate().isOk();
    }

    @Override
    public void writeItemChanges() {
        question.setQuestionHTML(questionEditor.asHtml().getValue());
    }

    @Override
    public Component getView() {
        return layout;
    }
}
