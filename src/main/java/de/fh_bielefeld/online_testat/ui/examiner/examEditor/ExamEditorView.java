package de.fh_bielefeld.online_testat.ui.examiner.examEditor;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.router.*;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.repositories.ExamRepository;
import de.fh_bielefeld.online_testat.services.ExamSerializationService;
import de.fh_bielefeld.online_testat.ui.examiner.MyExamsView;
import de.fh_bielefeld.online_testat.ui.general.GeneralLayout;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.PermitAll;

/**
 * Provides a view that allows to create and edit exams and appointments
 *
 * @author Josh Knipping
 */
@PageTitle("Prüfung erstellen")
@Route(value = "examiner/exam/:examID", layout = GeneralLayout.class)
@PermitAll
public class ExamEditorView extends VerticalLayout implements BeforeEnterObserver, AfterNavigationObserver, BeforeLeaveObserver {
    private VerticalLayout content;
    private Tab examTasks;
    private Tab examAppointments;

    private final ExamRepository examRepository;
    private Exam exam;
    private final BeanValidationBinder<Exam> examBinder;

    private final ExamSerializationService serializationService;

    private final Logger logger;

    private final ExamUtils examUtils;
    private boolean skipLeaveConfirmation;

    public ExamEditorView(ExamRepository examRepository, ExamSerializationService serializationService, ExamUtils examUtils) {
        logger = LoggerFactory.getLogger(ExamEditorView.class);

        this.examRepository = examRepository;
        this.serializationService = serializationService;
        this.examUtils = examUtils;

        examBinder = new BeanValidationBinder<>(Exam.class);
    }

    /**
     * Constructs the view
     * @param afterNavigationEvent Gets called after entering the view
     */
    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        examBinder.setBean(exam);
        setSizeFull();

        HorizontalLayout header = new HorizontalLayout();

        TextField examName = new TextField();
        examName.setClearButtonVisible(true);
        examName.setWidth("30%");
        examName.setPlaceholder("Prüfungsname");
        examBinder.bind(examName, "name");

        examTasks = new Tab("Prüfungsaufgaben");
        examAppointments = new Tab("Termine");

        Tabs tabs = new Tabs(examAppointments, examTasks);
        tabs.addSelectedChangeListener(event ->
                setContent(event.getSelectedTab())
        );
        tabs.addThemeVariants(TabsVariant.LUMO_ICON_ON_TOP);
        tabs.getStyle().set("margin", "auto");

        content = new VerticalLayout();
        content.setSizeFull();

        header.setWidthFull();
        header.add(examName, tabs);
        header.setAlignSelf(Alignment.CENTER, tabs);

        add(header, content, getFooter());
        setContent(tabs.getSelectedTab());
    }

    /**
     * Adds content to the view depending on the selected tab.
     * @param tab either appointment or question tab
     */
    private void setContent(Tab tab) {
        content.removeAll();

        if (tab.equals(examAppointments)) {
            content.add(new ExamEditorAppointmentTab(exam, serializationService, examBinder));
        } else if (tab.equals(examTasks)) {
            content.add(new ExamEditorQuestionTab(exam, serializationService, examBinder));
        }
    }

    /**
     * Creates the footer of the view including abort and save buttons.
     * Add functionality and aligns them to the bottom of the layout.
     * @return horizontalLayout of the footer
     */
    private HorizontalLayout getFooter() {
        Button abortButton = new Button("Abbrechen");
        abortButton.addClickListener(event -> UI.getCurrent().navigate(MyExamsView.class));

        Button saveButton = new Button("Speichern");
        saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        saveButton.addClickListener(e -> {
            // ToDo: Refactor
            if(exam.getQuestions().size() == 0) {
                Notification.show("Die Prüfung muss mindestens eine Frage besitzen!")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
                return;
            }

            var validationStatus = examBinder.validate();

            if(validationStatus.isOk()) {
                try {
                    examRepository.save(exam);
                } catch (Exception ex) {
                    Notification.show("Unbekannter Fehler beim Speichern der Prüfung!")
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
                    logger.error("Unable to save Exam", ex);
                    return;
                }

                skipLeaveConfirmation = true;
                UI.getCurrent().navigate(MyExamsView.class);

                Notification.show("Prüfung erfolgreich gespeichert")
                    .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
            } else {
                Notification.show("Fehler beim Speichern der Prüfung: " + validationStatus.getValidationErrors().get(0).getErrorMessage())
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
            }
        });

        var abortSave = new HorizontalLayout();

        abortSave.add(abortButton, saveButton);
        abortSave.setJustifyContentMode(JustifyContentMode.END);
        abortSave.setWidthFull();

        return abortSave;
    }

    /**
     * Loads content about the sites exam from the repository
     * @param event Gets called while entering the view
     */
    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if(event.getRouteParameters().get("examID").orElseGet(() -> "new").equalsIgnoreCase("new")) {
            exam = new Exam();
            return;
        }

        var loadedExam = examUtils.getExamFromBeforeEvent(event);

        loadedExam.ifPresentOrElse(exam -> {
            this.exam = exam;
        }, () -> {
            event.rerouteTo(MyExamsView.class);
        });
    }

    /**
     * Prevents navigation and asks for confirmation
     */
    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        if(skipLeaveConfirmation)
            return;

        var postponation = beforeLeaveEvent.postpone();

        ConfirmDialog leaveConfirmation = new ConfirmDialog();
        leaveConfirmation.setCancelable(true);
        leaveConfirmation.setCancelText("Weiter bearbeiten");
        leaveConfirmation.setConfirmText("Verlassen");
        leaveConfirmation.setHeader("Wollen Sie die Seite wirklich verlassen?");
        leaveConfirmation.setText("Alle ungespeicherten Änderungen gehen verloren.");
        leaveConfirmation.addConfirmListener(event -> postponation.proceed());

        leaveConfirmation.open();
    }
}
