package de.fh_bielefeld.online_testat.ui.examiner.evaluateExam;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.router.*;
import de.fh_bielefeld.online_testat.model.answer.Answer;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.model.exam.Examinee;
import de.fh_bielefeld.online_testat.repositories.ExamRepository;
import de.fh_bielefeld.online_testat.ui.examiner.MyExamsView;
import de.fh_bielefeld.online_testat.ui.general.GeneralLayout;
import de.fh_bielefeld.online_testat.ui.utils.DateTimeUtils;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;
import org.jsoup.Jsoup;

import javax.annotation.security.PermitAll;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * View for displaying the answers from all examinees for all the appointments of the selected exam
 *
 * @author Lucie Jahn
 */
@PageTitle("Prüfung korrigieren")
@Route(value="examiner/evaluate/:examID", layout = GeneralLayout.class)
@PermitAll
public class EvaluationExamView extends VerticalLayout implements BeforeEnterObserver, AfterNavigationObserver {
    private final Grid<Answer> answersGrid = new Grid<>(Answer.class);
    private final ExamRepository examRepository;
    private final ExamUtils examUtils;
    private final Select<ExamAppointment> selectAppointment = new Select<>();
    private final Select<Examinee> selectExaminee = new Select<>();
    private final Span notParticipatedStatus = new Span("Nicht teilgenommen");
    private final Span examDurationStatus = new Span();
    private Exam exam;

    public EvaluationExamView(ExamRepository examRepository, ExamUtils examUtils) {
        this.examRepository = examRepository;
        this.examUtils = examUtils;
    }

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        Binder<Exam> examBinder = new Binder<>();
        examBinder.setBean(exam);

        setScrollableMainLayout();
        showNotParticipatedStatus();
        showExamDurationStatus();
    }

    /**
     * adding the layout elements (header, scroller, footer) to the main layout
     */
    private void setScrollableMainLayout() {
        setAlignItems(Alignment.STRETCH);
        setMaxWidth("100%");
        setMaxHeight("100%");

        add(getHeaderLayout(), getScrollerLayout(), getFooterLayout());
    }

    /**
     * set up header with select for appointment & examinee + not participated status
     *
     * @return Header
     */
    private Header getHeaderLayout() {
        notParticipatedStatus.setVisible(false);
        notParticipatedStatus.getElement().getThemeList().add("badge error");
        notParticipatedStatus.getStyle().set("display", "flex").set("align-items", "center").set("justify-content", "center");

        examDurationStatus.setVisible(false);
        examDurationStatus.getStyle().set("display", "flex").set("align-items", "center").set("justify-content", "center");
        Header header = new Header(getExamNameTitle(), getDetailsForm(), examDurationStatus, notParticipatedStatus);
        header.getStyle()
                .set("border-bottom", "1px solid var(--lumo-contrast-20pct)")
                .set("display", "block")
                .set("padding", "var(--lumo-space-m)");
        return header;
    }

    /**
     * set up answersGrid ass scrollable layout
     *
     * @return Scroller
     */
    private Scroller getScrollerLayout() {
        Scroller scroller = new Scroller((new Div(getAnswersGrid())));
        scroller.setScrollDirection(Scroller.ScrollDirection.VERTICAL);
        scroller.getStyle()
                .set("border-bottom", "1px solid var(--lumo-contrast-20pct)")
                .set("padding", "var(--lumo-space-m)");
        return scroller;
    }

    /**
     * set up footer layout with save & cancel button + click listeners
     *
     * @return Footer
     */
    private Footer getFooterLayout() {
        Button cancelButton = new Button("Abbrechen");
        cancelButton.addClickListener(buttonClickEvent -> UI.getCurrent().navigate(MyExamsView.class));

        Button saveButton = new Button("Speichern");
        saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        saveButton.addClickListener(click -> saveEvaluations());

        //have to set the buttons in seperate horizontallayout and then in footer to provide the space between the buttons when displayed
        HorizontalLayout horizontalLayout = new HorizontalLayout(cancelButton, saveButton);
        final Footer footer = new Footer(horizontalLayout);
        footer.getStyle().set("display", "flex").set("align-items", "center").set("justify-content", "end");

        return footer;
    }

    /**
     * save all answers with their evaluations to the exam repository
     */
    private void saveEvaluations() {
        var numberOfElements = answersGrid.getDataCommunicator().getItemCount();
        List<Answer> answerList = new ArrayList<>();

        try {
            for (int i = 0; i < numberOfElements; i++) {
                Answer answer = answersGrid.getDataCommunicator().getItem(i);
                answerList.add(answer);
            }
            getSelectedExaminee().setAnswers(answerList);

            checkFullyCheckedAndParticipatedStatusForAppointment();
            examRepository.save(exam);

        } catch (Exception ex) {
            Notification.show("Fehler beim Speichern der Bewertung")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
            return;
        }
        UI.getCurrent().getPage().reload();
        Notification.show("Bewertung erfolgreich gespeichert")
                .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
    }

    /**
     * get exam name to display as title in header
     * @return Span
     */
    private Span getExamNameTitle() {
        Span examName = new Span(exam.getName());
        examName.getStyle().set("font-weight", "bold");

        return examName;
    }

    private void setSelectedExaminee(Examinee examinee){ selectExaminee.setValue(examinee);}
    private Examinee getSelectedExaminee(){
        return selectExaminee.getValue();
    }

    private void setSelectedExamAppointment(ExamAppointment appointment){selectAppointment.setValue(appointment);}
    private ExamAppointment getSelectedExamAppointment(){
        return selectAppointment.getValue();
    }

    /**
     *  set up details form for header with select menu for appointment & examinee + change listeners
     *
     * @return FormLayout
     */
    private FormLayout getDetailsForm(){
        FormLayout detailsForm = new FormLayout();
        setupSelectAppointment();
        setupSelectExaminee();

        detailsForm.add(selectAppointment, selectExaminee);
        return detailsForm;
    }

    /**
     * set up select examinee with content and checked badge
     */
    private void setupSelectExaminee() {
        selectExaminee.setLabel("Teilnehmer");
        selectExaminee.setEmptySelectionAllowed(false);
        setSelectExamineeContent();
        selectExaminee.addValueChangeListener(examineeChange -> {
            if(examineeChange.isFromClient()){
                setSelectedExaminee(examineeChange.getValue());
                answersGrid.setItems(getSelectedExaminee().getAnswers());
            } else {
                selectExaminee.setPlaceholder("Wähle einen Teilnehmer aus");
            }
            showNotParticipatedStatus();
            showExamDurationStatus();
        });
    }

    /**
     * set up select appointment with content and checked badge
     */
    private void setupSelectAppointment() {
        selectAppointment.setLabel("Termin");
        selectAppointment.setEmptySelectionAllowed(false);
        setSelectAppointmentContent();
        selectAppointment.addValueChangeListener(appointmentChange -> {
            setSelectedExamAppointment(appointmentChange.getValue());
            selectExaminee.setItems(getSelectedExamAppointment().getExaminees());
        });
    }

    /**
     * not participated status will be visible for examinees, that haven't participated
     * -> as soon as the appointment duration is over
     */
    private void showNotParticipatedStatus() {
        if(getSelectedExaminee() != null){
            LocalDateTime startTime = getSelectedExamAppointment().getPlannedStartTime();
            if(getSelectedExamAppointment().getActualStartTime() != null){
                startTime = getSelectedExamAppointment().getActualStartTime();
            }
            LocalDateTime endTime = ExamUtils.getAppointmentEndTime(startTime, getSelectedExamAppointment());

            if (getSelectedExaminee().getAnswers().size() == 0) {
                if(LocalDateTime.now().isAfter(endTime)) {
                    notParticipatedStatus.setVisible(true);
                }
            } else {
                notParticipatedStatus.setVisible(false);
            }
        } else {
            notParticipatedStatus.setVisible(false);
        }
    }

    /**
     *
     */
    private void showExamDurationStatus() {
        examDurationStatus.removeAll();
        if(getSelectedExaminee() != null){
            if(getSelectedExamAppointment().hasStarted()) {
                LocalDateTime startTime = getSelectedExamAppointment().getActualStartTime();
                LocalDateTime endTime = ExamUtils.getAppointmentEndTime(startTime, getSelectedExamAppointment());
                examDurationStatus.add("Die Prüfung endet um: " + DateTimeUtils.formatTime(endTime));
                examDurationStatus.getElement().getThemeList().add("badge");

                if (LocalDateTime.now().isBefore(endTime)) {
                    examDurationStatus.setVisible(true);
                } else {
                    examDurationStatus.setVisible(false);
                }
            }
        } else {
            examDurationStatus.setVisible(false);
        }
    }

    /**
     * set content for the appointment select menu
     */
    private void setSelectAppointmentContent() {
        selectAppointment.setRenderer(new ComponentRenderer<>(appointment -> {
            FlexLayout wrapper = new FlexLayout();

            Div appointmentDiv = new Div();
            appointmentDiv.setText(DateTimeUtils.formatAppointmentDate(appointment));
            appointmentDiv.setWidthFull();

            if(appointment.isFullyChecked()){
                wrapper.setAlignItems(Alignment.CENTER);
                wrapper.add(appointmentDiv, getCheckIcon());
            } else {
                wrapper.add(appointmentDiv);
            }

            return wrapper;
        }));
        selectAppointment.setItems(exam.getAppointments());
        selectAppointment.setValue(exam.getAppointments().iterator().next());

        //disable appointments if it has no examinees
        selectAppointment.setItemEnabledProvider(appointment -> appointment.getExaminees().size() > 0);
    }

    private Icon getCheckIcon() {
        Icon checkIcon = VaadinIcon.CHECK.create();
        checkIcon.getStyle().set("padding", "var(--lumo-space-xs");
        checkIcon.getElement().getThemeList().add("badge small");
        return checkIcon;
    }

    /**
     * set content for the examinee select menu
     */
    private void setSelectExamineeContent() {
        selectExaminee.setRenderer(new ComponentRenderer<>(examinee -> {
            FlexLayout wrapper = new FlexLayout();

            Div appointmentDiv = new Div();
            appointmentDiv.setText(examinee.getName());
            appointmentDiv.setWidthFull();

            System.out.println(examinee.getName());
            System.out.println(examinee.isReviewed());
            if(examinee.isReviewed()){
                wrapper.setAlignItems(Alignment.CENTER);
                wrapper.add(appointmentDiv, getCheckIcon());
            } else {
                wrapper.add(appointmentDiv);
            }

            return wrapper;
        }));
        selectExaminee.setItems(getSelectedExamAppointment().getExaminees());
        selectExaminee.setValue(getSelectedExamAppointment().getExaminees().get(0));
    }

    /**
     * set up grid to display all answers & sort by question type
     * @return Grid<Answer>
     */
    private Grid<Answer> getAnswersGrid(){
        answersGrid.setItems(getSelectedExaminee().getAnswers());

        answersGrid.removeAllColumns();
        answersGrid.addComponentColumn(ExamUtils::formatQuestionTypeToIcon).setKey("type").setComparator(Comparator.comparingInt(answer -> answer.getQuestion().getType().ordinal())).setWidth("6%");
        answersGrid.addColumn(answer -> Jsoup.parse(answer.getQuestion().getQuestionHTML()).text()).setKey("question").setHeader("Aufgabe").setWidth("30%");
        answersGrid.addColumn(new ComponentRenderer<>(ExamUtils::formatAnswer)).setKey("answer").setHeader("Antwort").setWidth("30%");
        answersGrid.addColumn(answer -> answer.getQuestion().getMaxPoints()).setKey("maxPoints").setHeader("Punkte").setTextAlign(ColumnTextAlign.CENTER).setWidth("12%");
        answersGrid.addComponentColumn(this::getPointRegulator).setKey("pointRegulator").setHeader("Bewertung").setWidth("12%");
        answersGrid.getColumnByKey("pointRegulator").onEnabledStateChanged(false);
        answersGrid.addColumn(createToggleDetailsRenderer(answersGrid)).setKey("toggleDetails").setWidth("10%");

        answersGrid.setDetailsVisibleOnClick(false);
        answersGrid.setItemDetailsRenderer(createAnswerDetailsRenderer());

        var sort = List.of(
                new GridSortOrder<>(answersGrid.getColumnByKey("type"), SortDirection.ASCENDING)
        );
        answersGrid.sort(sort);
        answersGrid.setAllRowsVisible(true);
        return answersGrid;
    }

    /**
     * show Details for selected answer row
     *
     * @param grid
     * @return LitRenderer
     */
    private static Renderer<Answer> createToggleDetailsRenderer(Grid<Answer> grid){
        return LitRenderer.<Answer>of(
                "<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">Details</vaadin-button>")
                .withFunction("handleClick", answer -> grid
                        .setDetailsVisible(answer,
                                !grid.isDetailsVisible(answer)));
    }

    private static ComponentRenderer<AnswerDetailsFormLayout, Answer> createAnswerDetailsRenderer(){
        return new ComponentRenderer<>(AnswerDetailsFormLayout::new, AnswerDetailsFormLayout::setAnswerDetails);
    }

    /**
     * set up pointRegulator for answer rows
     *
     * @param answer
     * @return NumberField
     */
    private NumberField getPointRegulator(Answer answer) {
        NumberField pointRegulator = new NumberField();

        if(answer.isReviewed()){
            pointRegulator.setValue((double) answer.getPoints());
        }
        pointRegulator.setHasControls(true);
        pointRegulator.setStep(0.5);
        pointRegulator.setMin(0);
        pointRegulator.setMax(answer.getQuestion().getMaxPoints());
        pointRegulator.addValueChangeListener(onChange -> answer.setPoints(pointRegulator.getValue().floatValue()));
        return pointRegulator;
    }

    /**
     * set appointment as fully checked if all examinees are reviewed
     */
    private void checkFullyCheckedAndParticipatedStatusForAppointment() {
        checkReviewStatusForExaminees();
        var reviewedExaminees = 0;
        for(int i=0; i<getSelectedExamAppointment().getExaminees().size(); i++){
            LocalDateTime startTime = getSelectedExamAppointment().getPlannedStartTime();
            Duration duration = Duration.ofMinutes(getSelectedExamAppointment().getExam().getDuration());
            LocalDateTime endTime = (LocalDateTime) duration.addTo(startTime);

            if(getSelectedExamAppointment().getExaminees().get(i).isReviewed() || getSelectedExamAppointment().getExaminees().get(i).getAnswers().isEmpty() && LocalDateTime.now().isAfter(endTime)){
                reviewedExaminees++;
            }
        }

        if(reviewedExaminees == getSelectedExamAppointment().getExaminees().size()){
            getSelectedExamAppointment().setFullyChecked(true);
        }
    }

    /**
     * set examinee as reviewed if all answers are evaluated
     */
    private void checkReviewStatusForExaminees() {
        getSelectedExamAppointment().getExaminees().forEach(examinee -> {
            var reviewedAnswers = 0;
            for(Answer answer : examinee.getAnswers()){
                if(answer.isReviewed()){
                    reviewedAnswers++;
                }
            }

            if(getSelectedExamAppointment().getActualStartTime() != null){
                LocalDateTime start;
                if(getSelectedExamAppointment().hasStarted()){
                    start = getSelectedExamAppointment().getActualStartTime();
                } else {
                    start = getSelectedExamAppointment().getPlannedStartTime();
                }
                LocalDateTime end = ExamUtils.getAppointmentEndTime(start, getSelectedExamAppointment());

                if(reviewedAnswers == examinee.getAnswers().size()){
                    //when examinees getting corrected while the exam duration, the examinees without answers should not be set to reviewed; only when the exam duration is over
                    if(examinee.getAnswers().size() == 0){
                        if(LocalDateTime.now().isAfter(end)){
                            examinee.setReviewed(true);
                        }
                    } else {
                        examinee.setReviewed(true);
                    }
                }
            }
        });
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Optional<Exam> loadedExam = examUtils.getExamFromBeforeEvent(event);

        loadedExam.ifPresentOrElse(exam -> {
            this.exam = exam;
        }, () -> {
            event.forwardTo(MyExamsView.class);
        });
    }

}
