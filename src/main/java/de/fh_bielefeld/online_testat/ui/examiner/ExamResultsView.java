package de.fh_bielefeld.online_testat.ui.examiner;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.StreamResource;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.model.exam.Examinee;
import de.fh_bielefeld.online_testat.services.ExamSerializationService;
import de.fh_bielefeld.online_testat.ui.general.GeneralLayout;
import de.fh_bielefeld.online_testat.ui.utils.BadgeUtils;
import de.fh_bielefeld.online_testat.ui.utils.DateTimeUtils;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;

import javax.annotation.security.PermitAll;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * View to display the results of every examinee already evaluated examAppointments
 * The View contains 2 grid for examinee who either participated or not
 *
 * @author Josh Knipping, Lucie Jahn
 */

@PageTitle("Prüfungsergebnisse")
@Route(value="examiner/results/:examID", layout = GeneralLayout.class)
@PermitAll
public class ExamResultsView extends VerticalLayout implements BeforeEnterObserver, AfterNavigationObserver {

    private final Grid<Examinee> examineeGrid = new Grid<>(Examinee.class);
    private final Grid<Examinee> notParticipatedExamineeGrid = new Grid<>(Examinee.class);
    private final Exam examOnlyCheckedAppointments = new Exam();
    private final ExamSerializationService serializationService;
    private final TextField searchbar = new TextField();
    private final ExamUtils examUtils;
    private Exam exam;
    private GridListDataView<Examinee> examineeDataView;
    private GridListDataView<Examinee> notParticipatedExamineeDataView;
    private ComboBox<ExamAppointment> appointmentSelector;
    private Checkbox oneAppointment = new Checkbox("Einzelansicht");
    private Checkbox allAppointments = new Checkbox("Gesamtansicht");
    private Span examineeGridHeader = new Span();
    private Span notParticipatedExamineeGridHeader = new Span();
    private int examineesNum = 0;
    private int notParticipatedExamineesNum = 0;

    public ExamResultsView(ExamUtils examUtils, ExamSerializationService serializationService){
        this.examUtils = examUtils;
        this.serializationService = serializationService;
    }

    /**
     * set up View
     * @param afterNavigationEvent Gets called after entering the view
     */
    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        Label title = new Label(exam.getName());
        title.getStyle().set("font-weight", "bold");

        setupAppointmentDropdown();
        setupSearchbar();
        setupCheckboxes();

        examineeGridHeader.add("Teilnehmer");
        examineeGridHeader.getStyle().set("font-weight", "bold");
        notParticipatedExamineeGridHeader.add("Nicht teilgenommen");
        notParticipatedExamineeGridHeader.getStyle().set("font-weight", "bold");
        updateCountBadges();

        setupExamineeGrid();
        setupNotParticipatedExamineeGrid();

        add(title, getHeaderLayout(),getScrollerLayout(), getFooterLayout());
        setAlignItems(Alignment.STRETCH);
        setSizeFull();
    }

    /**
     * set up scroller with grids
     * @return gridLayoutScroller
     */
    private Scroller getScrollerLayout() {
        VerticalLayout participantLayout = new VerticalLayout(examineeGridHeader, examineeGrid);
        participantLayout.setWidth("65%");
        VerticalLayout notParticipantLayout = new VerticalLayout(notParticipatedExamineeGridHeader, notParticipatedExamineeGrid);
        notParticipantLayout.setWidth("35%");

        HorizontalLayout gridLayout = new HorizontalLayout(participantLayout, notParticipantLayout);
        gridLayout.setWidthFull();

        Scroller gridLayoutScroller = new Scroller();
        gridLayoutScroller.setContent((new Div(gridLayout)));
        gridLayoutScroller.setScrollDirection(Scroller.ScrollDirection.VERTICAL);
        gridLayoutScroller.getStyle()
                .set("border-bottom", "1px solid var(--lumo-contrast-20pct)");
        gridLayoutScroller.setWidthFull();
        return gridLayoutScroller;
    }

    /**
     * sets up the grid that contains the examinees that did not participated  in an appointment but where listed as participants for one/all appointments.
     * contains only the examinee name, since no results are available.
     *
     * @author Josh Knipping, Lucie Jahn
     */
    private void setupNotParticipatedExamineeGrid(){
        if(oneAppointment.getValue() == true){
            notParticipatedExamineeDataView = notParticipatedExamineeGrid.setItems(getSelectedExamAppointment().getExaminees());
        } else {
            notParticipatedExamineeDataView = notParticipatedExamineeGrid.setItems(getExamineesFromAllFullyCheckedAppointments());
        }
        notParticipatedExamineeGrid.removeAllColumns();
        notParticipatedExamineeGrid.setColumns("name");
        notParticipatedExamineeGrid.getColumnByKey("name").setHeader("Name");
        filterNotParticipatedExamineeGrid();
    }

    /**
     *  sets up the grid that contains the examinees for one/all appointments
     *  contains the examinee name, exam result in percent, the grade and averages per examinee
     *
     * @author Josh Knipping, Lucie Jahn
     */
    private void setupExamineeGrid(){
        List<Examinee> examinees;
        if(oneAppointment.getValue() == true){
            examineeDataView = examineeGrid.setItems(getSelectedExamAppointment().getExaminees());
            examinees = getSelectedExamAppointment().getExaminees();
        } else {
            examineeDataView = examineeGrid.setItems(getExamineesFromAllFullyCheckedAppointments());
            examinees = getExamineesFromAllFullyCheckedAppointments();
        }
        examineeGrid.removeAllColumns();
        examineeGrid.setColumns("name");
        examineeGrid.getColumnByKey("name").setHeader("Name").setFooter("Durchschnitt");

        examineeGrid.addColumn(ExamUtils::getPercentage).setKey("percentage").setHeader("Ergebnis in %").setFooter(ExamUtils.getPercentAverage(examinees));
        examineeGrid.addColumn(ExamUtils::getGrade).setKey("grade").setHeader("Note").setFooter(ExamUtils.getGradeAverage(examinees));
        filterExamineeGrid();
    }

    /***
     * returns list of all examinees from all fully checked appointments
     * @return examineeList
     * @author Lucie Jahn
     */
    private List<Examinee> getExamineesFromAllFullyCheckedAppointments() {
        List<Examinee> examineeList = new ArrayList<>();
        getFullyCheckedAppointments().forEach(appointment -> {
            appointment.getExaminees().forEach(examinee -> {examineeList.add(examinee);});
        });
        return examineeList;
    }

    /**
     * set the calculated sums of examinees for each grid to display as badges
     * @author Lucie Jahn
     */
    private void setExamineeCounts(){
        if(oneAppointment.getValue() == true){
            if(getSelectedExamAppointment() != null){
                countExamineeNums(getSelectedExamAppointment().getExaminees());
            }
        } else {
            countExamineeNums(getExamineesFromAllFullyCheckedAppointments());
        }
    }

    /**
     * calculate the sum of examinees for each grid to display as badges
     * @author Lucie Jahn
     */
    private void countExamineeNums(List<Examinee> Examinees) {
        examineesNum = 0;
        notParticipatedExamineesNum = 0;
        for(var examinee: Examinees) {
            if (examinee.getAnswers().size() != 0) {
                examineesNum++;
            } else {
                notParticipatedExamineesNum++;
            }
        }
    }

    /**
     * allows to filter the examineeDataView that contains the examineeGrid, through the searchbar by name
     * returns examinees, that participated in the appointment, because the list of the examinee answers is only empty if the examinee didn't participated.
     */
    private void filterExamineeGrid(){
        examineeDataView.addFilter(examinee -> {
            String searchBarValue = searchbar.getValue().trim();

            if (examinee.getAnswers().size() == 0){
                return false;
            }else
                return examinee.getName().toLowerCase().contains(searchBarValue.toLowerCase());
        });
    }

    /**
     * to filter the notParticipatedExamineeDataView that contains the notParticipatedExamineeGrid, through the searchbar by name
     * returns examinees, that did not participated in the appointment, because the list of the examinee answers is only empty if the examinee didn't participated.
     */
    private void filterNotParticipatedExamineeGrid(){
        notParticipatedExamineeDataView.addFilter(examinee -> {
            String searchBarValue = searchbar.getValue().trim();

            if (examinee.getAnswers().size() == 0){
                return examinee.getName().toLowerCase().contains(searchBarValue.toLowerCase());
            }else
                return false;
        });
    }

    /**
     * configures the searchbar visually and add actionslisteners to it, that refreshed the grid everytime the value of the searchbar is changed
     */
    private void setupSearchbar(){
        searchbar.setPlaceholder("Suche");
        searchbar.setClearButtonVisible(true);
        searchbar.setPrefixComponent(VaadinIcon.SEARCH.create());
        searchbar.setWidthFull();
        searchbar.setValueChangeMode(ValueChangeMode.EAGER);
        searchbar.addValueChangeListener(e -> examineeDataView.refreshAll());
        searchbar.addValueChangeListener(e -> notParticipatedExamineeDataView.refreshAll());
    }

    /**
     * returns the value of the appointment that is currently selected in the combobox
     */
    private ExamAppointment getSelectedExamAppointment(){
        return appointmentSelector.getValue();
    }

    /**
     * sets up the combobox that contains all evaluated appointments of the exam
     * sets the value of the combobox automatically to the first item in it
     * contains a actionListener that update both grids after the selection is changed
     */
    public void setupAppointmentDropdown(){
        appointmentSelector = new ComboBox<>("Termin");
        appointmentSelector.setItemLabelGenerator(DateTimeUtils::formatAppointmentDate);
        appointmentSelector.setAllowCustomValue(true);

        setFullyCheckedAppointments();
        appointmentSelector.setItems(getFullyCheckedAppointments());
        appointmentSelector.setValue(getFullyCheckedAppointments().get(0));
        appointmentSelector.addValueChangeListener(appointment -> {
            examineeGrid.setItems(getSelectedExamAppointment().getExaminees());
            examineeGrid.getColumnByKey("percentage").setFooter(ExamUtils.getPercentAverage(getSelectedExamAppointment().getExaminees()));
            examineeGrid.getColumnByKey("grade").setFooter(ExamUtils.getGradeAverage(getSelectedExamAppointment().getExaminees()));
            examineeDataView = examineeGrid.setItems(getSelectedExamAppointment().getExaminees());
            notParticipatedExamineeDataView = notParticipatedExamineeGrid.setItems(getSelectedExamAppointment().getExaminees());
            oneAppointment.setValue(true);
            allAppointments.setValue(false);
            filterExamineeGrid();
            filterNotParticipatedExamineeGrid();
            updateCountBadges();
        });
    }

    /**
     * set fully checked appointments to display in drop down
     */
    private void setFullyCheckedAppointments() {
        for (ExamAppointment appointment : exam.getAppointments()) {
            if (appointment.isFullyChecked()) {
                examOnlyCheckedAppointments.addAppointment(appointment);
            }
        }
    }

    private List<ExamAppointment> getFullyCheckedAppointments(){return examOnlyCheckedAppointments.getAppointments();}

    /**
     * checkboxes to display results for one or all fully checked appointments
     * @author Lucie Jahn
     */
    private void setupCheckboxes(){
        //Default
        oneAppointment.setValue(true);
        allAppointments.setEnabled(false);

        if(getFullyCheckedAppointments().size() > 1){
            allAppointments.setEnabled(true);
        }

        oneAppointment.addClickListener(click -> {
            if(oneAppointment.getValue() == true){
                allAppointments.setValue(false);
            } else if(oneAppointment.getValue() == false && allAppointments.getValue() == false){
                oneAppointment.setValue(true);
            }
            setupExamineeGrid();
            setupNotParticipatedExamineeGrid();
            updateCountBadges();
        });

        allAppointments.addClickListener(click -> {
            if(allAppointments.getValue() == true){
                oneAppointment.setValue(false);
            } else if(allAppointments.getValue() == false && oneAppointment.getValue() == false){
                allAppointments.setValue(true);
            }
            setupExamineeGrid();
            setupNotParticipatedExamineeGrid();
            updateCountBadges();
        });
    }

    /**
     * creates and configures a horizontal layout that contains the combobox for appointment selection and the searchbar
     * @return returns the configured headerLayout
     */
    private Header getHeaderLayout() {
        HorizontalLayout selectLayout = new HorizontalLayout(appointmentSelector, searchbar);
        selectLayout.setAlignItems(Alignment.END);
        selectLayout.setWidthFull();

        HorizontalLayout checkboxLayout = new HorizontalLayout(oneAppointment, allAppointments);
        return new Header(selectLayout, checkboxLayout);
    }

    /**
     * creates and configures a horizontal layout adds and configures an ok-button and a download-button
     * @return returns the confiured footerLayout
     */
    private HorizontalLayout getFooterLayout() {
        HorizontalLayout footerLayout = new HorizontalLayout();

        // OK Button
        Button okButton = new Button("Ok");
        okButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        okButton.addClickListener(buttonClickEvent -> UI.getCurrent().navigate(MyExamsView.class));

        /*
            Export Button (Excel)
            Creates Anchor with Button in it. Gets path of the users desktop.
            Creates a .xls file and writes the data which is currently displayed in the view into the file.
        */
        Anchor exportAnchor = new Anchor(new StreamResource(
                "Ergebnisse.xlsx",
                () -> new ByteArrayInputStream(
                        serializationService.exportResultsToExcel(((ListDataProvider<Examinee>) examineeGrid.getDataProvider()).getItems())
                )
        ).setContentType(ExamSerializationService.EXCEL_FORMAT), "");
        exportAnchor.getElement().setAttribute("download", true);
        Button exportButton = new Button(new Icon(VaadinIcon.DOWNLOAD));
        exportButton.addThemeVariants(ButtonVariant.LUMO_ICON);
        exportAnchor.add(exportButton);

        // add to Layout
        footerLayout.setJustifyContentMode(JustifyContentMode.END);
        footerLayout.setAlignItems(Alignment.END);
        footerLayout.add(exportAnchor, okButton);
        return footerLayout;
    }

    /**
     * Loads content about the sites exam from the repository
     * @param event Gets called while entering the view
     */
    @Override
    public void beforeEnter(BeforeEnterEvent event){
        Optional<Exam> loadedExam = examUtils.getExamFromBeforeEvent(event);

        loadedExam.ifPresentOrElse(exam -> {
            this.exam = exam;
        }, () -> {
            event.forwardTo(MyExamsView.class);
        });
    }

    /**
     * update sum number of badges for grids
     * @author Lucie Jahn
     */
    private void updateCountBadges() {
        setExamineeCounts();
        examineeGridHeader.removeAll();
        examineeGridHeader.add(BadgeUtils.getCountSummaryBadge("Teilnehmer", examineesNum));

        notParticipatedExamineeGridHeader.removeAll();
        notParticipatedExamineeGridHeader.add(BadgeUtils.getCountSummaryBadge("Nicht teilgenommen", notParticipatedExamineesNum));
    }
}
