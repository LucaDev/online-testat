package de.fh_bielefeld.online_testat.ui.examiner.examEditor;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.crud.BinderCrudEditor;
import com.vaadin.flow.component.crud.Crud;
import com.vaadin.flow.component.crud.CrudEditor;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.selection.SelectionListener;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.model.exam.Examinee;
import de.fh_bielefeld.online_testat.services.ExamSerializationService;
import de.fh_bielefeld.online_testat.services.ImportException;
import de.fh_bielefeld.online_testat.ui.utils.BadgeUtils;
import de.fh_bielefeld.online_testat.ui.utils.DateTimeUtils;
import de.fh_bielefeld.online_testat.ui.utils.I18nUtils;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @author Josh Knipping & Luca Kröger
 */
public class ExamEditorAppointmentTab extends VerticalLayout {
    private final Crud<Examinee> examineeCrud;
    private final Crud<ExamAppointment> appointmentCrud;
    private final Grid<Examinee> examineeGrid = new Grid<>(Examinee.class);

    private final ExamSerializationService serializationService;

    private final BeanValidationBinder<Exam> examBinder;

    private final Exam exam;

    Span titleAppointmentGrid = new Span();
    Span titleExamineeGrid = new Span();
    IntegerField duration = new IntegerField();

    private MenuItem exportButton;
    private Button addExamineeButton;

    public ExamEditorAppointmentTab(Exam exam, ExamSerializationService serializationService, BeanValidationBinder<Exam> examBinder) {
        this.serializationService = serializationService;
        this.examBinder = examBinder;
        this.exam = exam;

        //Layout of the Tab "Termine"
        setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);

        //Header of ExamineeGrid
        titleExamineeGrid.add("Teilnehmer");
        titleExamineeGrid.getStyle().set("font-weight", "bold");
        HorizontalLayout examineeCrudHeader = new HorizontalLayout(titleExamineeGrid);
        examineeCrudHeader.setAlignItems(FlexComponent.Alignment.BASELINE);
        examineeCrudHeader.setFlexGrow(1, titleExamineeGrid);

        //Examinees Crud
        examineeCrud = new Crud<>(
                Examinee.class,
                examineeGrid,
                createExamineeEditor()
        );
        examineeCrud.setI18n(I18nUtils.getCrudI18n());

        //Appointments Crud
        Grid<ExamAppointment> appointmentGrid = new Grid<>(ExamAppointment.class);
        appointmentGrid.setItems(exam.getAppointments());
        appointmentCrud = new Crud<>(
                ExamAppointment.class,
                appointmentGrid,
                createAppointmentEditor()
        );
        appointmentCrud.setI18n(I18nUtils.getCrudI18n());

        //Examinee Grid
        examineeGrid.removeAllColumns();
        examineeGrid.addColumn("name").setHeader("Name");
        examineeGrid.addColumn("matriculationNumber").setHeader("Matrikelnummer");
        Crud.addEditColumn(examineeGrid);

        //Examinee Crud - Listeners
        examineeCrud.addSaveListener(event -> {
            var appointment = getSelectedAppointment();
            if(appointment.isEmpty())
                return;

            if(!appointment.get().getExaminees().contains(event.getItem()))
                appointment.get().getExaminees().add(event.getItem());

            appointmentGrid.getDataProvider().refreshAll();
            updateCountBadges();
        });

        examineeCrud.addDeleteListener(event -> {
            var appointment = getSelectedAppointment();
            if(appointment.isEmpty())
                return;

            appointment.get().getExaminees().remove(event.getItem());

            examineeCrud.getDataProvider().refreshAll();
            appointmentCrud.getDataProvider().refreshItem(appointment.get());

            updateCountBadges();
        });

        setupExamineeToolbar();

        // Arrow
        Icon arrow = VaadinIcon.ARROW_RIGHT.create();

        //Header of AppointmentGrid
        titleAppointmentGrid.add("Termine");
        titleAppointmentGrid.getStyle().set("font-weight", "bold");
        HorizontalLayout existingAppointmentsGridHeader = new HorizontalLayout(titleAppointmentGrid);
        existingAppointmentsGridHeader.setAlignItems(FlexComponent.Alignment.BASELINE);
        existingAppointmentsGridHeader.setFlexGrow(1, titleAppointmentGrid);

        //
        appointmentCrud.addSaveListener(event -> exam.addAppointment(event.getItem()));

        appointmentCrud.addDeleteListener(event -> {
            exam.getAppointments().remove(event.getItem());
            appointmentCrud.getDataProvider().refreshAll();
        });

        /* Appointment Grid Columns */
        appointmentGrid.removeAllColumns();
        appointmentGrid.addColumn(DateTimeUtils::formatAppointmentDate).setHeader("Termin").setKey("appointmentHeader");
        appointmentGrid.addColumn(appointment -> appointment.getExaminees().size()).setHeader("Teilnehmer");
        appointmentGrid.addComponentColumn(appointment -> {
            if(appointment.hasStarted())
                return new Span();

            var button = new Button(VaadinIcon.EDIT.create());
            button.addThemeVariants(ButtonVariant.LUMO_SMALL);
            button.addThemeVariants(ButtonVariant.MATERIAL_OUTLINED);
            button.addClickListener(clickEvent -> appointmentCrud.edit(appointment, Crud.EditMode.EXISTING_ITEM));
            return button;
        }).setFlexGrow(0);

        updateCountBadges();

        appointmentCrud.addDeleteListener(delete -> appointmentGrid.deselectAll());

        appointmentGrid.addSelectionListener((SelectionListener<Grid<ExamAppointment>, ExamAppointment>) selectionEvent -> {
            updateCountBadges();

            if (selectionEvent.getFirstSelectedItem().isEmpty()) {
                exportButton.setEnabled(false);
                examineeGrid.setItems(List.of());

                return;
            }

            var appointment = selectionEvent.getFirstSelectedItem().get();

            exportButton.setEnabled(true);
            addExamineeButton.setEnabled(!appointment.hasStarted());

            if (appointment.hasStarted() && Crud.hasEditColumn(examineeGrid)) {
                Crud.removeEditColumn(examineeGrid);
            } else if (!appointment.hasStarted() && !Crud.hasEditColumn(examineeGrid)) {
                Crud.addEditColumn(examineeGrid);
            }

            examineeGrid.setItems(appointment.getExaminees());
        });

        setupAppointmentToolbar();

        //Layout for Appointment Grid + Header
        VerticalLayout appointmentTotal = new VerticalLayout(existingAppointmentsGridHeader, appointmentCrud);
        appointmentTotal.setSizeFull();
        appointmentTotal.setWidth("50%");

        //Layout for Examinee Grid + Header
        VerticalLayout examineeTotal = new VerticalLayout(examineeCrudHeader, examineeCrud);
        examineeTotal.setSizeFull();
        examineeTotal.setWidth("50%");

        //Layout for all Grids (next to each other
        HorizontalLayout gridTotal = new HorizontalLayout(appointmentTotal, arrow, examineeTotal);
        gridTotal.setSizeFull();
        gridTotal.setAlignSelf(FlexComponent.Alignment.CENTER, arrow);

        //Adding all Components to the layout
        add(gridTotal);
    }

    // Appointment Editor
    public CrudEditor<ExamAppointment> createAppointmentEditor() {
        DateTimePicker appointmentPicker = new DateTimePicker("Datum und Uhrzeit auswählen");
        var dateTimeWithFullHour = LocalDateTime.now().minusMinutes(LocalDateTime.now().getMinute());
        appointmentPicker.setMin(dateTimeWithFullHour);

        FormLayout formLayout = new FormLayout(appointmentPicker);

        Binder<ExamAppointment> binder = new Binder<>(ExamAppointment.class);
        binder.forField(appointmentPicker).asRequired().bind(ExamAppointment::getPlannedStartTime,ExamAppointment::setPlannedStartTime);

        return new BinderCrudEditor<>(binder, formLayout);
    }

    // Examinee Editor
    private CrudEditor<Examinee> createExamineeEditor() {
        TextField nameField = new TextField("Name");
        IntegerField matriculationNumberField = new IntegerField("Matrikelnummer");

        FormLayout examineeEditorLayout = new FormLayout(nameField, matriculationNumberField);

        BeanValidationBinder<Examinee> binder = new BeanValidationBinder<>(Examinee.class);

        binder.bind(nameField, "name");
        binder.bind(matriculationNumberField, "matriculationNumber");

        return new BinderCrudEditor<>(binder, examineeEditorLayout);
    }

    private void setupAppointmentToolbar() {
        Button createAppointmentButton = new Button("Termin hinzufügen", VaadinIcon.PLUS.create());
        createAppointmentButton.addClickListener(event -> appointmentCrud.edit(new ExamAppointment(), Crud.EditMode.NEW_ITEM));
        appointmentCrud.addSaveListener(save -> updateCountBadges());
        appointmentCrud.addDeleteListener(delete -> updateCountBadges());
        createAppointmentButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        duration.setTitle("Dauer in Minuten");
        examBinder.bind(duration, "duration");
        duration.getStyle().set("padding-top", "0px");
        duration.setHasControls(true);
        duration.setMin(1);
        duration.setMax(1440);
        duration.setRequiredIndicatorVisible(false);

        exam.getAppointments().forEach(appointment -> {
            if(appointment.hasStarted()){duration.setEnabled(false);}
        });

        FlexLayout toolbar = new FlexLayout(duration, createAppointmentButton);
        toolbar.setSizeFull();
        toolbar.setFlexWrap(FlexLayout.FlexWrap.WRAP);
        toolbar.setJustifyContentMode(JustifyContentMode.BETWEEN);
        toolbar.setAlignItems(Alignment.CENTER);
        appointmentCrud.setToolbar(toolbar);
    }

    private void setupExamineeToolbar() {
        addExamineeButton = new Button("Teilnehmer hinzufügen", VaadinIcon.PLUS.create());
        addExamineeButton.addClickListener(event -> {
            if(getSelectedAppointment().isPresent())
                examineeCrud.edit(new Examinee(), Crud.EditMode.NEW_ITEM);
        });
        addExamineeButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        // Excel Export
        final StreamResource excelExportRessource = new StreamResource(
                "Teilnehmer.xlsx",
                () -> new ByteArrayInputStream(
                        serializationService.exportExamineesToExcel(((ListDataProvider<Examinee>) examineeCrud.getGrid().getDataProvider()).getItems())
                )).setContentType(ExamSerializationService.EXCEL_FORMAT);
        final StreamRegistration excelRegistration = VaadinSession.getCurrent().getResourceRegistry().registerResource(excelExportRessource);

        final StreamResource excelTemplateExportRessource = new StreamResource(
                "Teilnehmer Vorlage.xlsx",
                () -> getClass().getClassLoader().getResourceAsStream("templates/Teilnehmer.xlsx")
        ).setContentType(ExamSerializationService.EXCEL_FORMAT);
        final StreamRegistration excelTemplateRegistration = VaadinSession.getCurrent().getResourceRegistry().registerResource(excelTemplateExportRessource);

        // CSV Export
        final StreamResource csvExportRessource = new StreamResource(
                "Teilnehmer.csv",
                () -> IOUtils.toInputStream(
                        serializationService.exportExamineesToCSV(getSelectedAppointment().orElseThrow().getExaminees()),
                        StandardCharsets.UTF_8
                )
        ).setContentType(ExamSerializationService.CSV_FORMAT);
        final StreamRegistration csvRegistration = VaadinSession.getCurrent().getResourceRegistry().registerResource(csvExportRessource);

        final StreamResource csvTemplateExportRessource = new StreamResource(
                "Teilnehmer Vorlage.csv",
                () -> getClass().getClassLoader().getResourceAsStream("templates/Teilnehmer.csv")
        ).setContentType(ExamSerializationService.CSV_FORMAT);
        final StreamRegistration csvTemplateRegistration = VaadinSession.getCurrent().getResourceRegistry().registerResource(csvTemplateExportRessource);


        // Export Menu
        MenuBar menuBar = new MenuBar();
        exportButton = menuBar.addItem(VaadinIcon.DOWNLOAD.create());
        exportButton.setEnabled(false);
        SubMenu subMenu = exportButton.getSubMenu();
        subMenu.addItem("Excel").addClickListener(clickEvent -> {
            UI.getCurrent().getPage().open(excelRegistration.getResourceUri().toString(), "_blank");
        });
        subMenu.addItem("CSV").addClickListener(clickEvent -> {
            UI.getCurrent().getPage().open(csvRegistration.getResourceUri().toString(), "_blank");
        });
        subMenu.add(new Hr());
        subMenu.addItem("Excel Vorlage").addClickListener(clickEvent -> {
            UI.getCurrent().getPage().open(excelTemplateRegistration.getResourceUri().toString(), "_blank");
        });
        subMenu.addItem("CSV Vorlage").addClickListener(clickEvent -> {
            UI.getCurrent().getPage().open(csvTemplateRegistration.getResourceUri().toString(), "_blank");
        });

        // CSV/Excel Upload Component
        MemoryBuffer buffer = new MemoryBuffer();
        Upload examineeUpload = new Upload(buffer);
        examineeUpload.setI18n(I18nUtils.getUploadI18n());
        examineeUpload.addSucceededListener(e -> {
            try {
                var examinees = serializationService.readExamineesFromFile(buffer.getFileName(), buffer.getInputStream());

                var appointment = getSelectedAppointment();
                if(appointment.isEmpty())
                    return;

                appointment.get().getExaminees().addAll(examinees);
                examineeCrud.getDataProvider().refreshAll();
                updateCountBadges();

                Notification.show(buffer.getFileName() + " erfolgreich importiert");
            } catch (ImportException ex) {
                Notification.show(buffer.getFileName() + " konnte nicht importiert werden: " + ex.getMessage())
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
            }
        });
        examineeUpload.setAcceptedFileTypes(ExamSerializationService.ACCEPTED_FORMATS);
        examineeUpload.setUploadButton(new Button("Teilnehmer hochladen"));
        examineeUpload.setDropLabel(new Label("Datei hier ablegen"));

        FlexLayout toolbar = new FlexLayout(examineeUpload, addExamineeButton, menuBar);
        toolbar.setSizeFull();
        toolbar.setFlexWrap(FlexLayout.FlexWrap.WRAP);
        toolbar.setJustifyContentMode(JustifyContentMode.BETWEEN);
        toolbar.setAlignItems(Alignment.CENTER);
        examineeCrud.setToolbar(toolbar);
    }

    private Optional<ExamAppointment> getSelectedAppointment() {
        var iterator = appointmentCrud.getGrid().getSelectedItems().iterator();

        if(!iterator.hasNext()) {
            Notification.show("Bitte wählen Sie zunächst einen Termin")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
            return Optional.empty();
        }

        return Optional.of(iterator.next());
    }


    /**
     * @author Lucie Jahn
     */
    private void updateCountBadges() {
        titleAppointmentGrid.removeAll();
        titleAppointmentGrid.add(BadgeUtils.getCountSummaryBadge("Termine", exam.getAppointments().size()));

        titleExamineeGrid.removeAll();
        if(appointmentCrud.getGrid().getSelectedItems().size() > 0){
            titleExamineeGrid.add(BadgeUtils.getCountSummaryBadge("Teilnehmer", getSelectedAppointment().get().getExaminees().size()));
        } else {
            titleExamineeGrid.add(BadgeUtils.getCountSummaryBadge("Teilnehmer", 0));
        }
    }
}
