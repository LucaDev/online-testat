package de.fh_bielefeld.online_testat.ui.examiner;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.router.RouteParameters;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.repositories.ExamRepository;
import de.fh_bielefeld.online_testat.ui.examiner.evaluateExam.EvaluationExamView;
import de.fh_bielefeld.online_testat.ui.examiner.examEditor.ExamEditorView;
import de.fh_bielefeld.online_testat.ui.general.GeneralLayout;
import de.fh_bielefeld.online_testat.ui.utils.BadgeUtils;
import de.fh_bielefeld.online_testat.ui.utils.DateTimeUtils;

import javax.annotation.security.PermitAll;
import java.util.ArrayList;
import java.util.List;

/**
 * View for displaying all exams from user (examiner)
 * exams are splitted in 2 grids, one for open exams & one for evaluated exams
 *
 * @author Lucie Jahn, Luca Kröger
 */
@PageTitle("Aktuelle Prüfungen")
@Route(value = "examiner/myExams", layout = GeneralLayout.class)
@RouteAlias(value = "examiner", layout = GeneralLayout.class)
@PermitAll
public class MyExamsView extends VerticalLayout {
    private final Grid<Exam> openExamsGrid = new Grid<>(Exam.class);
    private final Grid<Exam> finishedExamsGrid = new Grid<>(Exam.class);
    private final TextField searchField = new TextField();
    private final ExamRepository examRepository;
    private GridListDataView<Exam> openExamsDataView;
    private GridListDataView<Exam> finishedExamsDataView;

    private final Span examsText = new Span();
    private final Span finishedExamsText = new Span();

    private int openExamsNum = 0;
    private int finishedExamsNum = 0;

    public MyExamsView(ExamRepository examRepository) {
        this.examRepository = examRepository;

        examsText.add("Offene Prüfungen");
        examsText.getStyle().set("font-weight", "bold");

        finishedExamsText.add("Bewertete Prüfungen");
        finishedExamsText.getStyle().set("font-weight", "bold");

        add(getToolbar(), getOpenExamsGrid(), finishedExamsText, getFinishedExamsGrid());
        updateCountBadges();
    }

    /**
     * set up grid for displaying all exams with not fully checked appointments
     * @return openExamsGrid
     *
     * @author Lucie Jahn
     */
    private Grid<Exam> getOpenExamsGrid() {
        openExamsDataView = openExamsGrid.setItems(examRepository.findAll());
        filterOpenExams();

        openExamsGrid.setSelectionMode(Grid.SelectionMode.NONE);
        openExamsGrid.setColumns("name");
        openExamsGrid.getColumnByKey("name").setHeader("Prüfung").setAutoWidth(true);
        openExamsGrid.addColumn(DateTimeUtils::formatDuration).setHeader("Dauer").setAutoWidth(true);
        openExamsGrid.addColumn(new ComponentRenderer<>(DateTimeUtils::formatOpenExamAppointments)).setHeader("Termin/e").setAutoWidth(true);
        setGeneralComponentColumns(openExamsGrid);

        return openExamsGrid;
    }

    /**
     * filter to display
     * -> only appointments in openExamsGrid, that aren't fully checked
     * -> exam names matching the content in the search bar (if used)
     *
     * @author Lucie Jahn, Luca Kröger
     */
    private void filterOpenExams() {
        openExamsDataView.addFilter(exam -> {
            String searchTerm = searchField.getValue().trim();

            if(DateTimeUtils.formatOpenExamAppointments(exam).getInnerHtml().equals("kein Termin")) return false;

            return searchTerm.isEmpty() || exam.getName().toLowerCase().contains(searchTerm.toLowerCase());
        });
    }

    /**
     * get exam sum of filtered open exams grid for badge
     * @author Lucie Jahn
     */
    private void setOpenExamsNum() {
        openExamsNum = 0;
        List<Long> examIDList = new ArrayList<>();
        for(var exam : examRepository.findAll()){
            for(var appointment : exam.getAppointments()){
                if(!appointment.isFullyChecked()){
                    if(!examIDList.contains(exam.getID())) {
                        examIDList.add(exam.getID());
                        openExamsNum++;
                    }
                }
            }
        }
    }

    /**
     * set up grid for displaying all exams with fully checked appointments
     * @return finishedExamsGrid
     *
     * @author Lucie Jahn
     */
    private Grid<Exam> getFinishedExamsGrid() {
        finishedExamsDataView = finishedExamsGrid.setItems(examRepository.findAll());
        filterFinishedExams();

        finishedExamsGrid.setSelectionMode(Grid.SelectionMode.NONE);
        finishedExamsGrid.setColumns("name");
        finishedExamsGrid.getColumnByKey("name").setHeader("Prüfung").setAutoWidth(true);
        finishedExamsGrid.addColumn(DateTimeUtils::formatDuration).setHeader("Dauer").setAutoWidth(true);
        finishedExamsGrid.addColumn(new ComponentRenderer<>(DateTimeUtils::formatEvaluatedExamAppointments)).setHeader("Termin/e").setAutoWidth(true);

        setComponentColumnResults(finishedExamsGrid);
        setGeneralComponentColumns(finishedExamsGrid);

        return finishedExamsGrid;
    }

    /**
     * filter to display
     * -> only appointments in finishedExamsGrid, that are fully checked
     * -> exam names matching the content in the search bar (if used)
     *
     * @author Lucie Jahn, Luca Kröger
     */
    private void filterFinishedExams() {
        finishedExamsDataView.addFilter(exam -> {
            String searchTerm = searchField.getValue().trim();

            if(DateTimeUtils.formatEvaluatedExamAppointments(exam).getInnerHtml().equals("kein Termin"))
                return false;

            if(searchTerm.isEmpty())
                return true;

            return exam.getName().toLowerCase().contains(searchTerm.toLowerCase());
        });
    }

    /***
     * get exam sum of filtered finished exams grid for badge
     * @author Lucie Jahn
     */
    private void setFinishedExamsNum() {
        finishedExamsNum = 0;
        List<Long> examIDList = new ArrayList<>();
        for(var exam : examRepository.findAll()){
            for(var appointment : exam.getAppointments()){
                if(appointment.isFullyChecked()){
                    if(!examIDList.contains(exam.getID())) {
                        examIDList.add(exam.getID());
                        finishedExamsNum++;
                    }
                }
            }
        }
    }

    /**
     * set up component columns, that will be shown in both grids
     * -> delete, edit & evaluate button
     * @param grid
     *
     * @author Lucie Jahn
     */
    private void setGeneralComponentColumns(Grid<Exam> grid) {
        grid.addComponentColumn(exam -> {
            Button deleteExamButton = new Button(VaadinIcon.TRASH.create());
            deleteExamButton.addClickListener(e -> openDeleteDialog(exam));
            deleteExamButton.getElement().setProperty("title", "Prüfung löschen");
            return deleteExamButton;
        }).setAutoWidth(true);
        grid.addComponentColumn(exam -> {
            Button editExamButton = new Button(VaadinIcon.EDIT.create());
            editExamButton.addClickListener(e -> UI.getCurrent().navigate(ExamEditorView.class, new RouteParameters("examID", exam.getID().toString())));
            editExamButton.getElement().setProperty("title", "Prüfung bearbeiten");
            return editExamButton;
        }).setAutoWidth(true);
        grid.addComponentColumn(exam -> {
            Button evaluateExamButton = new Button(VaadinIcon.CHECK.create());
            evaluateExamButton.addClickListener(e -> checkExamContentAndRedirect(exam));
            evaluateExamButton.getElement().setProperty("title", "Prüfung korrigieren");
            return evaluateExamButton;
        }).setAutoWidth(true);
    }

    /**
     * check if exam has appointments & at least one appointment has examinees to redirect to EvaluationExamsView
     * @param exam
     *
     * @author Lucie Jahn
     */
    private void checkExamContentAndRedirect(Exam exam) {
        if(!exam.getAppointments().isEmpty()) {
            int appointmentsNumber = exam.getAppointments().size();
            int appointmentsWithoutExaminees = 0;

            for(var appointment : exam.getAppointments()){
                if(appointment.getExaminees().size() == 0){
                    appointmentsWithoutExaminees++;
                }
            }

            if(appointmentsWithoutExaminees == appointmentsNumber){
                Notification.show("Es gibt noch keinen Inhalt zum korrigieren.")
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
            } else {
                UI.getCurrent().navigate(EvaluationExamView.class, new RouteParameters("examID", exam.getID().toString()));
            }
        } else {
            Notification.show("Es gibt noch keinen Inhalt zum korrigieren.")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
        }
    }

    /**
     * opens delete dialog when clicking on delete button in grid
     * @param exam
     *
     * @author Lucie Jahn
     */
    private void openDeleteDialog(Exam exam) {
        ConfirmDialog deleteDialog = new ConfirmDialog();
        deleteDialog.setHeader("Prüfung löschen");
        deleteDialog.setText("Die ausgewählte Prüfung wird in beiden Tabellen gelöscht. Sind Sie sicher, dass Sie die Prüfung und alle damit verbundenen Daten löschen wollen?");

        deleteDialog.setCancelable(true);
        deleteDialog.setCancelText("Abbrechen");

        deleteDialog.setConfirmText("Löschen");
        deleteDialog.addConfirmListener(event -> {
            examRepository.delete(exam);
            openExamsGrid.setItems(examRepository.findAll());
            filterOpenExams();
            finishedExamsGrid.setItems(examRepository.findAll());
            filterFinishedExams();
            updateCountBadges();

            Notification.show("Prüfung erfolgreich gelöscht.")
                    .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
        });

        deleteDialog.open();
    }

    /**
     * set up component column to see exam results
     * -> will only be displayed in finishedExamsGrid
     * @param grid
     *
     * @author Lucie Jahn
     */
    private void setComponentColumnResults(Grid<Exam> grid) {
        grid.addComponentColumn(exam -> {
            Button viewExamButton = new Button(VaadinIcon.SEARCH.create());
            viewExamButton.addClickListener(e -> UI.getCurrent().navigate(ExamResultsView.class, new RouteParameters("examID", exam.getID().toString())));
            viewExamButton.getElement().setProperty("title", "Prüfungsergebnisse ansehen");
            return viewExamButton;
        }).setAutoWidth(true);
    }

    /**
     * set up toolbar with search bar and plus button to create new exams
     * @return toolbar
     *
     * @author Luca Kröger
     */
    private HorizontalLayout getToolbar() {
        searchField.setWidthFull();
        searchField.setPlaceholder("Suche");
        searchField.setPrefixComponent(new Icon(VaadinIcon.SEARCH));
        searchField.setValueChangeMode(ValueChangeMode.EAGER);
        searchField.addValueChangeListener(e -> finishedExamsDataView.refreshAll());
        searchField.addValueChangeListener(e -> openExamsDataView.refreshAll());

        Button addExamButton = new Button(VaadinIcon.PLUS.create());
        addExamButton.addClickListener(e -> UI.getCurrent().navigate(ExamEditorView.class, new RouteParameters("examID", "new")));

        HorizontalLayout toolbar = new HorizontalLayout(searchField, addExamButton);
        toolbar.setJustifyContentMode(JustifyContentMode.END);
        toolbar.setWidth("30%");
        HorizontalLayout toolbarTotal = new HorizontalLayout(examsText, toolbar);
        toolbarTotal.setAlignSelf(Alignment.CENTER,examsText);
        toolbarTotal.setJustifyContentMode(JustifyContentMode.BETWEEN);
        toolbarTotal.setWidthFull();

        return toolbarTotal;
    }

    /**
     * @author Lucie Jahn
     */
    private void updateCountBadges() {
        setOpenExamsNum();
        setFinishedExamsNum();

        examsText.removeAll();
        examsText.add(BadgeUtils.getCountSummaryBadge("Offene Prüfungen", openExamsNum));
        examsText.setHeight("50%");

        finishedExamsText.removeAll();
        finishedExamsText.add(BadgeUtils.getCountSummaryBadge("Bewertete Prüfungen", finishedExamsNum));
    }
}
