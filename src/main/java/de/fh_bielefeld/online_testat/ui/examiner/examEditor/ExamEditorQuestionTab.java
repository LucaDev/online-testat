package de.fh_bielefeld.online_testat.ui.examiner.examEditor;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.crud.Crud;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.dnd.GridDropLocation;
import com.vaadin.flow.component.grid.dnd.GridDropMode;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.model.question.Question;
import de.fh_bielefeld.online_testat.model.question.TextQuestion;
import de.fh_bielefeld.online_testat.services.ExamSerializationService;
import de.fh_bielefeld.online_testat.services.ImportException;
import de.fh_bielefeld.online_testat.ui.utils.BadgeUtils;
import de.fh_bielefeld.online_testat.ui.utils.I18nUtils;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

/**
 * @author Josh Knipping & Luca Kröger
 * @author Lucie Jahn -> changed some things for usage of badges & changed layout to scroller layout
 */
public class ExamEditorQuestionTab extends VerticalLayout {
    private Crud<Question> questionCrud;
    private final Grid<Question> questionGrid = new Grid<>(Question.class);
    private Question dragQuestion;
    private final ExamSerializationService serializationService;
    private final Exam exam;

    private final Checkbox mixupQuestionOrder;
    private final Checkbox mixChoices;

    private boolean isEditable = true;

    public ExamEditorQuestionTab(Exam exam, ExamSerializationService serializationService, BeanValidationBinder<Exam> examBinder) {
        this.serializationService = serializationService;
        this.exam = exam;

        mixupQuestionOrder = new Checkbox("Aufgaben bei jeder Prüfung zufällig anordnen");
        mixChoices = new Checkbox("Antwortmöglichkeiten zufällig anordnen");

        exam.getAppointments().forEach(appointment -> {
            if(appointment.hasStarted())
                isEditable = false;
        });

        setAlignItems(Alignment.STRETCH);
        setMaxWidth("100%");
        setMaxHeight("100%");
        add(getHeaderLayout(examBinder), getScrollerLayout());
    }

    /**
     * without scroller layout, the crud footer will not always displayed
     * @return scroller
     *
     * @author Lucie Jahn
     */
    private Scroller getScrollerLayout() {
        Scroller scroller = new Scroller((new Div(getQuestionsGrid(exam))));
        scroller.setScrollDirection(Scroller.ScrollDirection.VERTICAL);
        scroller.getStyle()
                .set("border-bottom", "1px solid var(--lumo-contrast-20pct)")
                .set("padding", "var(--lumo-space-xs)");
        return scroller;
    }

    private Crud<Question> getQuestionsGrid(Exam exam) {
        questionGrid.setItems(new ListDataProvider<>(exam.getQuestions()));

        questionCrud = new Crud<>(
                Question.class,
                questionGrid,
                new ExamQuestionCrudEditor(mixChoices.getValue())
        );

        questionCrud.setSizeFull();
        questionCrud.setI18n(I18nUtils.getCrudI18n());

        questionCrud.addSaveListener(questionSaveEvent -> {
            if(!exam.getQuestions().contains(questionSaveEvent.getItem())) // ToDo Refactor
                exam.getQuestions().add(questionSaveEvent.getItem());
            questionGrid.getDataProvider().refreshAll();

            updateCountBadges();
        });

        questionCrud.addDeleteListener(questionDeleteEvent -> {
            exam.getQuestions().remove(questionDeleteEvent.getItem());

            questionGrid.getDataProvider().refreshAll();
            updateCountBadges();
        });

        /* Drag & Drop */
        questionGrid.setSelectionMode(Grid.SelectionMode.NONE);
        questionGrid.setRowsDraggable(!mixupQuestionOrder.getValue() && this.isEditable);
        questionGrid.setAllRowsVisible(true);

        questionGrid.addDragStartListener(event -> {
            questionGrid.setDropMode(GridDropMode.BETWEEN);

            dragQuestion = event.getDraggedItems().get(0);
        });

        questionGrid.addDragEndListener(event -> {
            questionGrid.setDropMode(null);

            dragQuestion = null;
        });

        questionGrid.addDropListener(event -> {
            if(event.getDropTargetItem().isEmpty())
                return;

            Question question = event.getDropTargetItem().get();

            if(question.equals(dragQuestion))
                return;

            exam.getQuestions().remove(dragQuestion);
            int dropIndex = exam.getQuestions().indexOf(question) + (event.getDropLocation() == GridDropLocation.BELOW ? 1 : 0);
            exam.getQuestions().add(dropIndex, dragQuestion);
            questionGrid.getDataProvider().refreshAll();
        });

        /* Columns */
        questionGrid.removeAllColumns();
        questionGrid.addColumn(question -> Jsoup.parse(question.getQuestionHTML()).text()).setHeader(BadgeUtils.getCountSummaryBadge("Aufgabe", 0)).setKey("taskHeader");
        questionGrid.addColumn(question -> question.getType().getName()).setHeader("Aufgabentyp");
        questionGrid.addColumn(Question::getMaxPoints).setHeader(BadgeUtils.getCountSummaryBadge("Punktzahl", 0)).setKey("pointHeader");
        updateCountBadges();
        if(isEditable)
            Crud.addEditColumn(questionGrid);

        setupQuestionToolbar();

        return questionCrud;
    }

    /**
     * configure header for scroller layout
     * @param examBinder
     * @return
     *
     * @author Lucie Jahn
     */
    private Header getHeaderLayout(BeanValidationBinder<Exam> examBinder) {
        Span notEditableBadgeSpan = new Span("Keine Bearbeitung mehr möglich, da mindestens einer der Prüfungstermine bereits gestartet/beendet wurde.");
        notEditableBadgeSpan.getStyle().set("margin", "var(--lumo-space-xs");
        notEditableBadgeSpan.getElement().getThemeList().add("badge error");

        Span titleQuestionGrid = new Span("Aufgaben ");
        titleQuestionGrid.getStyle().set("font-weight", "bold");
        if(!isEditable)
            titleQuestionGrid.add(notEditableBadgeSpan);

        mixupQuestionOrder.setReadOnly(!isEditable);
        mixupQuestionOrder.addValueChangeListener(event -> questionGrid.setRowsDraggable(!event.getValue()));
        examBinder.bind(mixupQuestionOrder, Exam::isMixupQuestionsEnabled, Exam::setMixupQuestionsEnabled);
        mixChoices.setReadOnly(!isEditable);
        examBinder.bind(mixChoices, Exam::isRandomlyMixChoices, Exam::setRandomlyMixChoices);

        var checkboxLayout = new FlexLayout(mixupQuestionOrder, mixChoices);
        checkboxLayout.setFlexWrap(FlexLayout.FlexWrap.WRAP);

        VerticalLayout verticalLayout = new VerticalLayout(titleQuestionGrid, checkboxLayout);

        Header header = new Header(verticalLayout);
        header.getStyle()
                .set("display", "block")
                .set("padding", "var(--lumo-space-xs)");

        return header;
    }

    private void setupQuestionToolbar() {
        updateCountBadges();

        Button createQuestionButton = new Button("Aufgabe hinzufügen", VaadinIcon.PLUS.create());
        createQuestionButton.addClickListener(event -> questionCrud.edit(new TextQuestion(), Crud.EditMode.NEW_ITEM));
        createQuestionButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        // Excel Export
        final StreamResource excelExportResource = new StreamResource(
                "Fragen.xlsx",
                () -> new ByteArrayInputStream(
                        serializationService.exportQuestionsToExcel(((ListDataProvider<Question>) questionCrud.getGrid().getDataProvider()).getItems())
                )
        ).setContentType(ExamSerializationService.EXCEL_FORMAT);
        final StreamRegistration excelRegistration = VaadinSession.getCurrent().getResourceRegistry().registerResource(excelExportResource);

        final StreamResource excelTemplateExportResource = new StreamResource(
                "Fragen Vorlage.xlsx",
                () -> getClass().getClassLoader().getResourceAsStream("templates/Fragen.xlsx")
        ).setContentType(ExamSerializationService.EXCEL_FORMAT);
        final StreamRegistration excelTemplateRegistration = VaadinSession.getCurrent().getResourceRegistry().registerResource(excelTemplateExportResource);


        // CSV Export
        final StreamResource csvExportResource = new StreamResource(
                "Fragen.csv",
                () -> IOUtils.toInputStream(
                        serializationService.exportQuestionsToCSV(exam.getQuestions()),
                        StandardCharsets.UTF_8
                )
        ).setContentType(ExamSerializationService.CSV_FORMAT);
        final StreamRegistration csvRegistration = VaadinSession.getCurrent().getResourceRegistry().registerResource(csvExportResource);

        final StreamResource csvTemplateExportResource = new StreamResource(
                "Fragen Vorlage.csv",
                () -> getClass().getClassLoader().getResourceAsStream("templates/Fragen.csv")
        ).setContentType(ExamSerializationService.CSV_FORMAT);
        final StreamRegistration csvTemplateRegistration = VaadinSession.getCurrent().getResourceRegistry().registerResource(csvTemplateExportResource);

        // Export Menu
        MenuBar menuBar = new MenuBar();
        MenuItem item = menuBar.addItem(VaadinIcon.DOWNLOAD.create());
        SubMenu subMenu = item.getSubMenu();
        subMenu.addItem("Excel").addClickListener(clickEvent -> {
            UI.getCurrent().getPage().open(excelRegistration.getResourceUri().toString(), "_blank");
        });
        subMenu.addItem("CSV").addClickListener(clickEvent -> {
            UI.getCurrent().getPage().open(csvRegistration.getResourceUri().toString(), "_blank");
        });
        subMenu.add(new Hr());
        subMenu.addItem("Excel Vorlage").addClickListener(clickEvent -> {
            UI.getCurrent().getPage().open(excelTemplateRegistration.getResourceUri().toString(), "_blank");
        });
        subMenu.addItem("CSV Vorlage").addClickListener(clickEvent -> {
            UI.getCurrent().getPage().open(csvTemplateRegistration.getResourceUri().toString(), "_blank");
        });

        // Question Upload Component
        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);
        upload.setI18n(I18nUtils.getUploadI18n());
        upload.addSucceededListener(e -> {
            try {
                exam.getQuestions().addAll(serializationService.readQuestionsFromFile(buffer.getFileName(), buffer.getInputStream()));
                questionCrud.getDataProvider().refreshAll();
                updateCountBadges();
                Notification.show(buffer.getFileName() + " erfolgreich Importiert");
            } catch (ImportException ex) {
                Notification.show("Import Fehlgeschlagen: " + ex.getMessage())
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
            }
        });
        upload.setAcceptedFileTypes(ExamSerializationService.ACCEPTED_FORMATS);
        upload.setUploadButton(new Button("Fragen hochladen"));
        upload.setDropLabel(new Label("Datei hier ablegen"));

        FlexLayout toolbar = new FlexLayout();
        if(isEditable)
            toolbar.add(upload, createQuestionButton);
        toolbar.add(menuBar);
        toolbar.setSizeFull();
        toolbar.setAlignItems(FlexComponent.Alignment.CENTER);
        toolbar.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        toolbar.setFlexWrap(FlexLayout.FlexWrap.WRAP);

        questionCrud.setToolbar(toolbar);
    }

    /**
     * @author Lucie Jahn
     */
    private void updateCountBadges() {
        questionGrid.getColumnByKey("taskHeader").setHeader(BadgeUtils.getCountSummaryBadge("Aufgabe", exam.getQuestions().size()));
        questionGrid.getColumnByKey("pointHeader").setHeader(BadgeUtils.getCountSummaryBadge("Punkte", exam.getQuestions().stream().mapToDouble(Question::getMaxPoints).sum()));
    }
}
