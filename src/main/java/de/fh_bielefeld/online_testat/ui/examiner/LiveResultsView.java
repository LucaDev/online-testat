package de.fh_bielefeld.online_testat.ui.examiner;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.charts.model.ChartType;
import com.vaadin.flow.component.charts.model.DataSeries;
import com.vaadin.flow.component.charts.model.DataSeriesItem;
import com.vaadin.flow.component.charts.model.PlotOptionsPie;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.gridpro.GridPro;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.shared.Registration;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.repositories.ExamAppointmentRepository;
import de.fh_bielefeld.online_testat.services.ExamStatusService;
import de.fh_bielefeld.online_testat.ui.examinee.CurrentExamsView;
import de.fh_bielefeld.online_testat.ui.general.GeneralLayout;
import de.fh_bielefeld.online_testat.ui.utils.BadgeUtils;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;

import javax.annotation.security.PermitAll;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * A live view of a currently running exam appointment
 * @author Josh Knipping, Luca Kröger, (Lucie Jahn)
 */
@PageTitle("Live Prüfungsfortschritt")
@Route(value = "examiner/live/:examAppointmentID", layout = GeneralLayout.class)
@PermitAll
public class LiveResultsView extends VerticalLayout implements BeforeEnterObserver, AfterNavigationObserver {

    private final ExamAppointmentRepository examAppointmentRepository;
    private final ExamStatusService statusService;
    private final ExamUtils examUtils;
    private final Grid<ExamStatusService.ExamStatus> progressGrid;
    private final Chart chart;
    private final Span examineeNameHeader = new Span();
    private final Span submitHeader = new Span();
    private ExamAppointment examAppointment;
    private Registration broadcasterRegistration;

    public LiveResultsView(ExamAppointmentRepository examAppointmentRepository, ExamStatusService statusService, ExamUtils examUtils) {
        this.statusService = statusService;
        this.examAppointmentRepository = examAppointmentRepository;
        this.examUtils = examUtils;

        progressGrid = new GridPro<>(ExamStatusService.ExamStatus.class);
        chart = new Chart(ChartType.PIE);
    }

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        //Title
        var title = new Span(examAppointment.getExam().getName());
        title.getStyle().set("font-weight", "bold");
        title.getStyle().set("font-size", "20px");

        //Live badge
        Span liveBadge = BadgeUtils.getLiveBadge();

        //Start Appointment Button
        Button startButton = new Button("Starten");
        startButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL);
        startButton.setDisableOnClick(true);
        startButton.setEnabled(!examAppointment.hasStarted());

        startButton.addClickListener(e -> {
            if (!examAppointment.hasStarted()) {
                examAppointment.setActualStartTime(LocalDateTime.now());
                examAppointmentRepository.save(examAppointment);

                Notification.show("Prüfung wurde gestartet.")
                        .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
            }
        });

        //Pie chart
        var options = new PlotOptionsPie();
        options.setInnerSize("0");
        options.setSize("60%");
        chart.getConfiguration().setPlotOptions(options);

        //Progress Grid
        progressGrid.removeAllColumns();
        progressGrid.addColumn(statusUpdate -> statusUpdate.getExaminee().getName()).setHeader(examineeNameHeader);
        progressGrid.addColumn(ExamStatusService.ExamStatus::getQuestionsAnswered).setHeader("Beantwortete Fragen");
        progressGrid.addComponentColumn(examinee -> {
            if(examinee.isFinished()){
                return BadgeUtils.getCheckIconBadge();
            } else {
                return BadgeUtils.getCloseIconBadge();
            }
        }).setHeader(submitHeader);

        //Top Layout
        HorizontalLayout headerAndChart = new HorizontalLayout(title, liveBadge, startButton);
        headerAndChart.setSizeFull();

        //General Layout
        add(headerAndChart, chart, progressGrid);
    }

    private void updateCounts(Collection<ExamStatusService.ExamStatus> examStatuses) {
        DataSeries series = new DataSeries();
        var finished = examStatuses.stream().filter(ExamStatusService.ExamStatus::isFinished).count();
        var outstanding = examAppointment.getExaminees().size() - examStatuses.size();
        var working = examStatuses.stream().filter(Predicate.not(ExamStatusService.ExamStatus::isFinished)).count();

        series.add(new DataSeriesItem("Abgegeben", finished));
        series.add(new DataSeriesItem("Ausstehend ", outstanding));
        series.add(new DataSeriesItem("in Bearbeitung", working));

        chart.getConfiguration().setSeries(series);
        chart.drawChart();

        updateCountBadges(examStatuses, finished);
    }

    /**
     * update number of count badges
     * @param examStatuses
     * @param finished
     *
     * @author Lucie Jahn
     */
    private void updateCountBadges(Collection<ExamStatusService.ExamStatus> examStatuses, long finished) {
        examineeNameHeader.removeAll();
        examineeNameHeader.add(BadgeUtils.getCountSummaryBadge("Name", examStatuses.size()));

        submitHeader.removeAll();
        submitHeader.add(BadgeUtils.getCountSummaryBadge("Abgegeben", finished));
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Optional<ExamAppointment> loadedExamAppointment = examUtils.getExamAppointmentFromBeforeEvent(event);

        loadedExamAppointment.ifPresentOrElse(examAppointment -> {
            this.examAppointment = examAppointment;

            if (examAppointment.isOver()) {
                Notification.show("Die Prüfung ist bereits vorüber.")
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
                event.rerouteTo(CurrentExamsView.class);
            }
        }, () -> {
            event.forwardTo(CurrentExamsView.class);
        });
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = attachEvent.getUI();
        broadcasterRegistration = statusService.register(examAppointment, statusMessage -> ui.access(() -> {
            progressGrid.getDataProvider().refreshAll();
            updateCounts(statusMessage);
        }));

        progressGrid.setItems(statusService.getCurrentStatus(examAppointment));
        updateCounts(statusService.getCurrentStatus(examAppointment));
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        broadcasterRegistration.remove();
        broadcasterRegistration = null;
    }
}
