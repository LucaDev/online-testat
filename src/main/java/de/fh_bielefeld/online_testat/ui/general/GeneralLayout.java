package de.fh_bielefeld.online_testat.ui.general;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.RouterLink;
import de.fh_bielefeld.online_testat.ApplicationInformation;
import de.fh_bielefeld.online_testat.ui.admin.UserManagementView;
import de.fh_bielefeld.online_testat.ui.examinee.CurrentExamsView;
import de.fh_bielefeld.online_testat.ui.examiner.ExamResultsView;
import de.fh_bielefeld.online_testat.ui.examiner.LiveResultsView;
import de.fh_bielefeld.online_testat.ui.examiner.MyExamsView;
import de.fh_bielefeld.online_testat.ui.examiner.evaluateExam.EvaluationExamView;
import de.fh_bielefeld.online_testat.ui.examiner.examEditor.ExamEditorView;
import de.fh_bielefeld.online_testat.ui.security.Roles;
import de.fh_bielefeld.online_testat.ui.security.SecurityUtils;

import java.util.HashMap;
import java.util.Objects;

/**
 * The main view is a top-level placeholder for other views.
 *
 * @author Lucie Jahn, Luca Kröger
 */
public class GeneralLayout extends AppLayout implements BeforeEnterObserver {
    private final ApplicationInformation applicationInformation;
    private Tabs tabs;

    private final HashMap<Class<?>, Tab> tabMapper;

    public GeneralLayout(ApplicationInformation applicationInformation) {
        this.applicationInformation = applicationInformation;
        tabMapper = new HashMap<>();

        createTabs();
        addToNavbar(getTitle(), tabs, getVersion());
    }

    /**
     * set up title of application
     *
     * @return title
     * @author Lucie Jahn
     */
    private H1 getTitle() {
        H1 title = new H1("Online Testat");
        title.getStyle()
                .set("font-size", "var(--lumo-font-size-l)")
                .set("margin", "var(--lumo-space-m) var(--lumo-space-l)");
        return title;
    }

    /**
     * set up version of application
     *
     * @return version
     * @author Lucie Jahn, Ole Niediek
     */
    private H1 getVersion() {
        H1 version = new H1("v" + applicationInformation.getVersion());
        version.getStyle()
                .set("font-size", "var(--lumo-font-size-l)")
                .set("margin", "var(--lumo-space-m) var(--lumo-space-l)");
        return version;
    }

    /**
     * set up tabs
     * @author Lucie Jahn, Luca Kröger
     */
    private void createTabs() {
        Tab currentExams = new Tab(
                VaadinIcon.MAILBOX.create(),
                new RouterLink("Aktuelle Prüfungen", CurrentExamsView.class)
        );

        Tab myExams = new Tab(
                VaadinIcon.FOLDER_OPEN.create(),
                new RouterLink("Mein Prüfungsverzeichnis", MyExamsView.class)
        );

        Tab userManagement = new Tab(
                VaadinIcon.USERS.create(),
                new RouterLink("Nutzerverwaltung", UserManagementView.class)
        );

        Tab login = new Tab(
                VaadinIcon.USER.create(),
                new RouterLink("Anmelden", LoginView.class)
        );

        tabMapper.put(CurrentExamsView.class, currentExams);
        tabMapper.put(LiveResultsView.class, currentExams);
        tabMapper.put(MyExamsView.class, myExams);
        tabMapper.put(ExamEditorView.class, myExams);
        tabMapper.put(ExamResultsView.class, myExams);
        tabMapper.put(EvaluationExamView.class, myExams);
        tabMapper.put(UserManagementView.class, userManagement);

        tabs = new Tabs(currentExams);
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL);
        tabs.getStyle().set("margin", "auto");
        tabs.setAutoselect(false);

        if (SecurityUtils.isUserLoggedIn()) {
            Tab logout;

            if (SecurityUtils.isImpersonating()) {
                logout = new Tab(
                        VaadinIcon.ARROW_BACKWARD.create(),
                        new Span("Zurückkehren (" + Objects.requireNonNull(SecurityUtils.getAuthenticatedUser()).getUsername() + ")")
                );
            } else {
                logout = new Tab(
                        VaadinIcon.POWER_OFF.create(),
                        new Span("Abmelden (" + Objects.requireNonNull(SecurityUtils.getAuthenticatedUser()).getUsername() + ")")
                );
            }

            logout.getElement().addEventListener("click", event -> SecurityUtils.logoutUser());

            tabs.add(myExams);

            if (Objects.requireNonNull(SecurityUtils.getAuthenticatedUser()).getAuthorities().contains(Roles.getAuthority(Roles.ADMIN)))
                tabs.add(userManagement);

            tabs.add(logout);
        } else {
            tabs.add(login);
        }
    }

    /**
     * Sets selected tab based on the current page mapped by the tabMapper
     * {@inheritDoc}
     */
    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        tabs.setSelectedTab(tabMapper.get(beforeEnterEvent.getNavigationTarget()));
    }
}
