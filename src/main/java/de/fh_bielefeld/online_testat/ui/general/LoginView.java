package de.fh_bielefeld.online_testat.ui.general;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import de.fh_bielefeld.online_testat.ui.examiner.MyExamsView;
import de.fh_bielefeld.online_testat.ui.security.SecurityUtils;
import de.fh_bielefeld.online_testat.ui.utils.I18nUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Provides the login view for an {@link de.fh_bielefeld.online_testat.model.examiner.Examiner} to log in.
 *
 * @author Luca Kröger
 */
@Route("login")
@PageTitle("Anmeldung")
@AnonymousAllowed
public class LoginView extends VerticalLayout implements BeforeEnterObserver {
    private final LoginForm login = new LoginForm();

    public LoginView(AuthenticationManager authenticationManager) {
        setAlignItems(Alignment.CENTER);

        login.setForgotPasswordButtonVisible(false);
        login.setI18n(I18nUtils.getLoginI18n());

        login.addLoginListener(loginEvent -> {
            try {
                final Authentication authentication = authenticationManager
                        .authenticate(new UsernamePasswordAuthenticationToken(loginEvent.getUsername(), loginEvent.getPassword()));

                // in case authentication was successful we will update the security context, set the login status to true and redirect to the current exams view
                if (authentication != null) {
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    UI.getCurrent().navigate("");
                }
            } catch (AuthenticationException ex) {
                login.setError(true);
            }
        });

        add(new H1("Online-Testat"), login);
    }

    // In case the user is authenticated: re-route to the MyExams view
    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (SecurityUtils.isUserLoggedIn())
            beforeEnterEvent.forwardTo(MyExamsView.class);
    }
}
