package de.fh_bielefeld.online_testat.ui.admin;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.crud.BinderCrudEditor;
import com.vaadin.flow.component.crud.Crud;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import de.fh_bielefeld.online_testat.model.examiner.Examiner;
import de.fh_bielefeld.online_testat.repositories.ExaminerRepository;
import de.fh_bielefeld.online_testat.ui.general.GeneralLayout;
import de.fh_bielefeld.online_testat.ui.security.Roles;
import de.fh_bielefeld.online_testat.ui.security.SecurityUtils;
import de.fh_bielefeld.online_testat.ui.utils.DateTimeUtils;
import de.fh_bielefeld.online_testat.ui.utils.I18nUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.security.RolesAllowed;

/**
 * Provides a way to manage users
 * Only usable by admins
 *
 * @author Luca Kröger
 */
@Route(value = "admin/users", layout = GeneralLayout.class)
@PageTitle("Benutzerverwaltung")
@RolesAllowed(Roles.Constants.ADMIN_VALUE)
public class UserManagementView extends VerticalLayout {
    private final ExaminerRepository examinerRepository;

    private final Crud<Examiner> examinerCrud;

    private final PasswordEncoder passwordEncoder;

    private Text userCountText;

    public UserManagementView(ExaminerRepository examinerRepository, PasswordEncoder passwordEncoder) {
        this.examinerRepository = examinerRepository;
        this.passwordEncoder = passwordEncoder;

        Span title = new Span("Benutzerverwaltung");

        examinerCrud = new Crud<>(
                Examiner.class,
                new Grid<>(Examiner.class),
                createEditor()
        );

        setupToolbar();

        /* Grid Columns */
        examinerCrud.getGrid().removeAllColumns();
        examinerCrud.getGrid().addColumn("username").setHeader("Benutzername");
        examinerCrud.getGrid().addColumn(examiner -> examiner.isEnabled() ? "Ja" : "Nein").setHeader("Ist Aktiv");
        examinerCrud.getGrid().addColumn(examiner -> examiner.isAdmin() ? "Ja" : "Nein").setHeader("Ist Admin");
        examinerCrud.getGrid().addColumn(examiner -> DateTimeUtils.formatDateTime(examiner.getCreateTime())).setHeader("Hinzugefügt");
        examinerCrud.getGrid().addColumn(examiner -> DateTimeUtils.formatDateTime(examiner.getUpdateTime())).setHeader("Letzte Änderung");
        examinerCrud.getGrid().addComponentColumn(examiner -> {
            if (examiner.equals(SecurityUtils.getAuthenticatedUser()))
                return new Div();

            var impersonateBtn = new Button("Imitieren");
            impersonateBtn.addThemeVariants(ButtonVariant.LUMO_SMALL);
            impersonateBtn.addClickListener(event -> {
                UI.getCurrent().getPage().setLocation("/login/impersonate?username=" + examiner.getUsername());
            });
            return impersonateBtn;
        });
        Crud.addEditColumn(examinerCrud.getGrid());

        examinerCrud.setI18n(I18nUtils.getCrudI18n());

        examinerCrud.addSaveListener(e -> {
            examinerRepository.save(e.getItem());

            Notification.show("Nutzer erfolgreich gespeichert.")
                    .addThemeVariants(NotificationVariant.LUMO_SUCCESS);

            updateCrud();
        });

        examinerCrud.addDeleteListener(e -> {
            if (e.getItem().equals(SecurityUtils.getAuthenticatedUser())) {
                Notification.show("Sie können sich nicht selbst löschen. \uD83E\uDD26")
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
                return;
            }

            examinerRepository.delete(e.getItem());
            updateCrud();
        });

        updateCrud();

        this.setSizeFull();
        examinerCrud.setSizeFull();

        add(title, examinerCrud);
    }

    private void updateCrud() {
        examinerCrud.getGrid().setItems(examinerRepository.findAll());
        userCountText.setText(String.valueOf(examinerRepository.count()));
    }

    private BinderCrudEditor<Examiner> createEditor() {
        FormLayout form = new FormLayout();
        form.setMaxWidth("400px");
        form.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1));

        BeanValidationBinder<Examiner> binder = new BeanValidationBinder<>(Examiner.class);

        /* Username Field */
        TextField username = new TextField();
        username.setPlaceholder("Nutzername");
        binder.bind(username, "username");

        /* Password fields */
        PasswordField passwordField = new PasswordField();
        passwordField.setPlaceholder("Passwort");
        passwordField.addValueChangeListener(e -> binder.validate());

        PasswordField confirmPasswordField = new PasswordField();
        confirmPasswordField.setPlaceholder("Passwort");
        confirmPasswordField.addValueChangeListener(e -> binder.validate());

        binder.forField(passwordField).withValidator((password, valueContext) -> {
            // User is going to be edited. We allow changes without touching the password ToDo: fix
            if (binder.getBean() != null && !binder.getBean().getPassword().isEmpty() && password.isEmpty())
                return ValidationResult.ok();

            if (password.length() < 8)
                return ValidationResult.error("Das Passwort muss mindestens 8 Zeichen besitzen.");

            return ValidationResult.ok();
        }).bind(
                // We never present the password to the UI
                (ValueProvider<Examiner, String>) examiner -> "",
                // If we have an empty password, we're currently editing an existing user. We don't set it.
                (examiner, password) -> {
                    if (password.length() != 0) {
                        examiner.setPassword(passwordEncoder.encode(password));
                    }
                }
        );

        binder.forField(confirmPasswordField).withValidator((password, valueContext) -> {
            if (!password.equals(passwordField.getValue()))
                return ValidationResult.error("Die Passwörter stimmen nicht überein.");
            return ValidationResult.ok();
        }).bind(examiner -> "", (examiner, s) -> {
        });

        /* Enabled Checkbox */
        Checkbox enabledCheckbox = new Checkbox();
        binder.bind(enabledCheckbox, Examiner::isEnabled, Examiner::setEnabled);

        /* Admin Checkbox */
        Checkbox adminCheckbox = new Checkbox();
        binder.bind(adminCheckbox, Examiner::isAdmin, Examiner::setAdmin);

        form.addFormItem(username, "Benutzername");
        form.addFormItem(passwordField, "Passwort");
        form.addFormItem(confirmPasswordField, "Passwort wiederholen");
        form.addFormItem(enabledCheckbox, "Ist Aktiv");
        form.addFormItem(adminCheckbox, "Ist Admin");

        return new BinderCrudEditor<>(binder, form);
    }

    private void setupToolbar() {
        userCountText = new Text("?");

        Text usersText = new Text("Nutzer: ");
        HorizontalLayout userCountLayout = new HorizontalLayout(usersText, userCountText);

        Button button = new Button("Nutzer hinzufügen", VaadinIcon.PLUS.create());
        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        button.addClickListener(event -> examinerCrud.edit(new Examiner(), Crud.EditMode.NEW_ITEM));

        HorizontalLayout toolbar = new HorizontalLayout(userCountLayout, button);
        toolbar.setAlignItems(FlexComponent.Alignment.CENTER);
        toolbar.setFlexGrow(1, toolbar);
        toolbar.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        toolbar.setSpacing(false);

        examinerCrud.setToolbar(toolbar);
    }
}
