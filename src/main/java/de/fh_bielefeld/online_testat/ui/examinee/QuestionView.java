package de.fh_bielefeld.online_testat.ui.examinee;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.checkbox.CheckboxGroupVariant;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.radiobutton.RadioGroupVariant;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import de.fh_bielefeld.online_testat.ApplicationInformation;
import de.fh_bielefeld.online_testat.model.answer.Answer;
import de.fh_bielefeld.online_testat.model.answer.ChoiceAnswer;
import de.fh_bielefeld.online_testat.model.answer.TextAnswer;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.model.exam.Examinee;
import de.fh_bielefeld.online_testat.model.question.*;
import de.fh_bielefeld.online_testat.repositories.ExamineeRepository;
import de.fh_bielefeld.online_testat.services.AnswerRatingService;
import de.fh_bielefeld.online_testat.services.ExamStatusService;
import de.fh_bielefeld.online_testat.services.QuestionViewDisplayQuestionsService;
import de.fh_bielefeld.online_testat.services.QuestionViewNavbarService;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 * View that displays the different questions and tasks in the exam, so that the student can answer them
 *
 * @author Ole Niediek
 */
@PageTitle("Prüfung")
@AnonymousAllowed
@Route(value = "exam/:examID")
public class QuestionView extends AppLayout implements BeforeEnterObserver, AfterNavigationObserver, BeforeLeaveObserver {
    private final ExamineeRepository examineeRepository;
    private final ApplicationInformation applicationInformation;
    private final ExamUtils examUtils;

    private Exam exam;
    private int questionCount;
    private int currentQuestion = 1;
    private Examinee examinee;
    private LocalDateTime endTime;

    private final Timer timer = new Timer("SubmitTimer");
    private final QuestionViewDisplayQuestionsService services = new QuestionViewDisplayQuestionsService();

    private Tabs tabs;

    private final VerticalLayout pageLayout = new VerticalLayout(); // The main layout of this view to which all other components are added
    private final VerticalLayout borderLayout = new VerticalLayout(); // Displays a border
    private final VerticalLayout questionLayout = new VerticalLayout(); // questionLayout is for the area where the Questions are being displayed
    private VerticalLayout localLayout = new VerticalLayout(); // localLayout will display the questions

    private Label pageLabel;

    private List<Question> testQuestions;
    private List<Answer> testAnswers;

    private final ExamStatusService examStatusService;

    /**
     * Instance-variables are being set
     * @param examineeRepository Here the examinee and their answers will be saved
     * @param applicationInformation Information about the applications version and the gitVersion
     * @param examUtils Necessary for getting the exam
     * @param examStatusService Used for broadcasting information about currently running exams
     */
    public QuestionView(ExamineeRepository examineeRepository, ApplicationInformation applicationInformation, ExamUtils examUtils, ExamStatusService examStatusService) {
        this.examineeRepository = examineeRepository;
        this.applicationInformation = applicationInformation;
        this.examUtils = examUtils;
        this.examStatusService = examStatusService;
    }

    /**
     * More Instance-variables are being set with information from UI attributes
     * If attributes do not exist the user gets directed back to the start and can pick their test again
     * @param event Method gets called while entering the View
     */
    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Optional<Exam> loadedExam = examUtils.getExamFromBeforeEvent(event);

        if(loadedExam.isEmpty()) {
            event.rerouteTo(CurrentExamsView.class);
            Notification.show("Ein Fehler ist aufgetreten. Bitte wählen Sie Ihre Prüfung erneut")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
            return;
        }

        exam = loadedExam.get();
        testQuestions = services.randomiseOrderOfQuestions(exam.isMixupQuestionsEnabled(), exam.getQuestions());
        questionCount = testQuestions.size();
        examinee = (Examinee) UI.getCurrent().getSession().getAttribute("examinee");
        endTime = (LocalDateTime) UI.getCurrent().getSession().getAttribute("submitTime");

        if(examinee == null || endTime == null) {
            event.rerouteTo(CurrentExamsView.class);
            Notification.show("Ein Fehler ist aufgetreten. Bitte wählen Sie Ihre Prüfung erneut")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
        }
    }

    /**
     * View is being constructed
     * @param afterNavigationEvent Method gets called immediately after entering the View
     */
    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        /* Set style for pageLayout */
        pageLayout.setSizeFull();
        pageLayout.setAlignItems(FlexComponent.Alignment.CENTER);

        createSubmitTimer();
        createToggleAndNavbar();
        testAnswers = services.setAnswerList(testQuestions);

        /* Set style for borderLayout */
        borderLayout.getStyle().set("border", "1px solid black");
        borderLayout.setMinWidth("350px");
        borderLayout.setWidth("50%");
        borderLayout.setSpacing(false);

        /* Set style for questionLayout */
        questionLayout.getStyle().set("padding", "0px").set("margin-top", "0px").set("margin-bottom", "0px");
        questionLayout.setHeightFull();

        /* The title of the exam is added to the pageLayout */
        Label titleLabel = new Label(exam.getName());
        titleLabel.getStyle().set("font-size", "22pt").set("margin", "0px");
        pageLayout.add(titleLabel);

        /* The arrowLeft and arrowRight-buttons gets added to the pageLayout as well as a label,
           that tells the user at what question they are right now */
        Button arrowLeft = new Button(new Icon(VaadinIcon.ARROW_LEFT));
        arrowLeft.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        arrowLeft.addClickListener(event -> previousQuestion());

        pageLabel = new Label("Frage " + currentQuestion + "/" + questionCount);

        Button arrowRight = new Button(new Icon(VaadinIcon.ARROW_RIGHT));
        arrowRight.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        arrowRight.addClickListener(event -> nextQuestion());

        HorizontalLayout toolbar = new HorizontalLayout(arrowLeft, pageLabel, arrowRight);
        toolbar.setAlignItems(FlexComponent.Alignment.CENTER);
        pageLayout.add(toolbar);

        /* The first question of the test gets displayed and added to the questionLayout */
        displayQuestions();
        borderLayout.add(questionLayout);

        /* "Weiter"-button gets added to the pageLayout */
        Button nextButton = new Button("Weiter");
        nextButton.addClickListener(event -> nextQuestion());
        borderLayout.add(nextButton);
        borderLayout.setAlignSelf(FlexComponent.Alignment.END, nextButton);
        pageLayout.add(borderLayout);

        /* "Abgeben"-button gets added to the pageLayout */
        Button submitButton = new Button("Abgeben");
        submitButton.addClickListener(event -> createSubmitDialog());
        submitButton.setMinWidth("350px");
        submitButton.setWidth("50%");
        pageLayout.add(submitButton);

        /* The finished layout gets displayed */
        setContent(pageLayout);
    }

    /**
     * The Navbar displays information relevant to the user like the users name, the version of the software and the time left for the test
     * The DrawerToggle can be collapsed and unfolded and allows you to navigate directly to a chosen question
     */
    private void createToggleAndNavbar(){
        QuestionViewNavbarService navbarServices = new QuestionViewNavbarService();
        DrawerToggle toggle = new DrawerToggle();
        tabs = navbarServices.getTabs(questionCount);
        tabs.addSelectedChangeListener(selectedChangeEvent -> updateQuestion(tabs.getSelectedIndex()+1));
        addToDrawer(tabs);

        H1 title = navbarServices.getTitleForNavbar("Aufgabe", "left" ,"0px", "static");
        H1 name = navbarServices.getTitleForNavbar(examinee.getName() + " (" + examinee.getMatriculationNumber() + ")", "left" ,"30%", "absolute");
        H1 duration = navbarServices.getTitleForNavbar("", "left" ,"70%", "absolute");
        H1 version = navbarServices.getTitleForNavbar("v"+ applicationInformation.getVersion(), "right" ,"1px", "absolute");

        duration.setId("timeLeft");

        addToNavbar(toggle, title, name, duration, version);
    }

    /**
     * If the "PfeilRechts"-Button or the "Weiter"-Button is clicked the next question in the list gets displayed and the index in tabs changes accordingly
     * If the user has already arrived at the last question simply nothing is going to happen
     */
    private void nextQuestion() {
        if(currentQuestion == questionCount)
            return;
        tabs.setSelectedIndex(currentQuestion);
    }

    /**
     * If the "PfeilLinks"-Button is clicked the previous question in the list gets displayed and the index in tabs changes accordingly
     * If the student is still working on the first question simply nothing is going to happen
     */
    private void previousQuestion() {
        if(currentQuestion == 1)
            return;
        tabs.setSelectedIndex(currentQuestion - 2);
    }

    /**
     * The components in the question layout get changed the localLayout gets removed and a new localLayout gets added in the displayQuestions-Method
     * @param questionNumber The number of the question that is supposed to get displayed now
     */
    public void updateQuestion(int questionNumber) {
        currentQuestion = questionNumber;
        pageLabel.setText("Frage " + currentQuestion + "/" + questionCount);
        questionLayout.remove(localLayout);
        displayQuestions();
    }

    /**
     * Method, that checks if a Question-Object is a ChoiceQuestion or a TextQuestion and creates the localLayout accordingly
     * Adds the updated localLayout to the questionLayout
     */
    private void displayQuestions(){
        var question = testQuestions.get(currentQuestion -1);

        if(question instanceof ChoiceQuestion) {
            localLayout = createChoiceView((ChoiceQuestion) question);
        } else if(question instanceof TextQuestion){
            localLayout = createTextView((TextQuestion) question);
        }
        localLayout.getStyle().set("padding", "0px");
        questionLayout.add(localLayout);

        sendBroadcast();
    }

    /**
     * For a TextQuestion there gets to be a text field displayed, where the student can fill in their answer
     * @param question Must be a TextQuestion
     * @return A new VerticalLayout to replace the old localLayout
     */
    private VerticalLayout createTextView(TextQuestion question) {
        VerticalLayout local = new VerticalLayout();
        local.setHeightFull();
        local.setSpacing(false);

        TextArea answerArea = new TextArea("Antwort");
        answerArea.setWidthFull();
        answerArea.setHeightFull();
        answerArea.setAutofocus(true);
        answerArea.setPlaceholder("Antwort");
        borderLayout.setMinHeight("300px");
        if(((TextAnswer) testAnswers.get(currentQuestion-1)).getAnswer() != null)
            answerArea.setValue(((TextAnswer) testAnswers.get(currentQuestion-1)).getAnswer());
        answerArea.addValueChangeListener(e -> {
            ((TextAnswer) testAnswers.get(currentQuestion-1)).setAnswer(answerArea.getValue());
            tabs.getSelectedTab().getElement().setChild(0, new QuestionViewNavbarService().changeTabIcon(answerArea.isEmpty()).getElement()); // New CHECK_CIRCLE if the question has been answered
        });
        local.add(services.createLabelBar(question, "Freitext-Frage"),
                new Html("<div>" + question.getQuestionHTML() + "</div>"), answerArea);
        return local;
    }

    /**
     * For a ChoiceQuestion there is either a group of RadioButtons or Checkboxes being displayed and the user can choose their answer
     * @param question Must be a ChoiceQuestion
     * @return A new VerticalLayout to replace the old localLayout
     */
    private VerticalLayout createChoiceView(ChoiceQuestion question) {
        VerticalLayout local = new VerticalLayout();
        local.setSpacing(false);

        if(question instanceof MultipleChoiceQuestion){ // Checkboxes for Multiple-Choice-Questions
            CheckboxGroup<Choice> answerButtons = new CheckboxGroup<>();
            List<Choice> randomChoices = services.randomiseOrderOfChoices(exam.isRandomlyMixChoices(), question.getChoices()); // The order, in which the Checkboxes appear, is supposed to be random every time
            answerButtons.setItemLabelGenerator((ItemLabelGenerator<Choice>) Choice::getText);
            answerButtons.setLabel("Antwortmöglichkeiten");
            answerButtons.setItems(randomChoices);
            borderLayout.setMinHeight(services.minHeightForBorder(randomChoices.size()));
            answerButtons.addThemeVariants(CheckboxGroupVariant.LUMO_VERTICAL);
            answerButtons.select(services.getSelectedChoices(question.getChoices(), testAnswers, currentQuestion-1)); // If the question has already been answered by the user their answer get displayed again
            answerButtons.addValueChangeListener(e -> { // When a Checkbox is clicked the new value (true for selected, false for unselected) is handed over to the according answer
                for(int i = 0; i <question.getChoices().size(); i++){
                    ((ChoiceAnswer) testAnswers.get(currentQuestion-1)).getAnswers().set(i,
                            answerButtons.getSelectedItems().contains(question.getChoices().get(i))); // If the Choice is part of the selected Items the answer is true
                }
                tabs.getSelectedTab().getElement().setChild(0, new QuestionViewNavbarService().changeTabIcon(answerButtons.getSelectedItems().size() == 0).getElement()); // New CHECK_CIRCLE if the question has been answered
            });

            local.add(services.createLabelBar(question, "Multiple-Choice-Frage"), new Html("<div>" + question.getQuestionHTML() + "</div>"), answerButtons);
        } else { // RadioButtons for Single-Choice-Questions
            RadioButtonGroup<Choice> answerButtons = new RadioButtonGroup<>();
            List<Choice> randomChoices = services.randomiseOrderOfChoices(exam.isRandomlyMixChoices(), question.getChoices()); // The order, in which the RadioButtons appear, is supposed to be random every time
            answerButtons.setItemLabelGenerator((ItemLabelGenerator<Choice>) Choice::getText);
            answerButtons.setLabel("Antwortmöglichkeiten");
            answerButtons.setItems(randomChoices);
            borderLayout.setMinHeight(services.minHeightForBorder(randomChoices.size()));
            answerButtons.addThemeVariants(RadioGroupVariant.LUMO_VERTICAL);
            if(!(services.getSelectedChoices(question.getChoices(), testAnswers, currentQuestion-1).isEmpty()))
                answerButtons.setValue(services.getSelectedChoices(question.getChoices(), testAnswers, currentQuestion-1).get(0)); // If the question has already been answered by the user their answer get displayed again
            answerButtons.addValueChangeListener(e -> { // When a RadioButton is clicked the new value (true for selected, false for unselected) is handed over to the according answer
                for(int i = 0; i <question.getChoices().size(); i++){
                    ((ChoiceAnswer) testAnswers.get(currentQuestion-1)).getAnswers().set(i,
                            e.getValue() == question.getChoices().get(i)); // If the Choice is tho one chosen the answer is true
                }
                tabs.getSelectedTab().getElement().setChild(0, new QuestionViewNavbarService().changeTabIcon(false).getElement()); // New CHECK_CIRCLE to show, that the question has been answered
            });

            local.add(services.createLabelBar(question, "Single-Choice-Frage"),
                    new Html("<div>" + question.getQuestionHTML() + "</div>"), answerButtons);
        }

        return local;
    }

    /**
     * Creates two timers with different purposes
     * 1. Submit the users answers and navigate back to the CurrentExamsView
     * 2. Update the timeLeft-counter in the Navbar
     */
    private void createSubmitTimer(){
        var end = endTime.atZone(ZoneId.systemDefault()).toInstant();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(getUI().isPresent()) {
                    getUI().get().access(() -> {
                        submit();
                        Notification.show("Die Zeit ist abgelaufen. Deine Antworten wurden eingereicht");
                    });
                }
            }
        }, Date.from(end));

        UI.getCurrent().getPage().executeJs(
            "endTime = new Date(" + end.getEpochSecond() + ");" +
                    "const interval = setInterval(function() {" +
                        "if(document.getElementById('timeLeft') == null) {" +
                            "clearInterval(interval);" +
                            "return;" +
                        "}" +
                        "var timeLeft = Math.round(endTime.getTime() - Date.now() / 1000);" +
                        "document.getElementById('timeLeft').innerHTML = (timeLeft > 60 ? Math.ceil(timeLeft / 60) + ' Minuten' : timeLeft + ' Sekunden') + ' verbleibend';" +
                    "}, 1000);"
        );
    }

    /**
     * When the "Abgeben"-Button is clicked a little window appears asking the user if they truly want to submit
     */
    private void createSubmitDialog() {
        ConfirmDialog dialog = new ConfirmDialog();
        dialog.setHeader("Möchten Sie die Prüfung wirklich abgeben?");
        dialog.setText("Es besteht keine Möglichkeit mehr, die Antworten zu ändern.");

        dialog.setCancelable(true);
        dialog.setCancelText("Abbrechen");

        dialog.setConfirmText("Abgeben");
        dialog.setConfirmButtonTheme("primary");
        dialog.addConfirmListener(event -> submit());
        dialog.open();
    }

    /**
     * Save the users answers in the local Object examinee and save the examinee in the ExamineeRepository, then navigate back to the CurrentExamsView
     */
    private void submit(){
        examinee.setAnswers(services.unrandomiseAnswers(testAnswers, exam.getQuestions()));
        automaticAnswerEvaluation();
        examineeRepository.save(examinee);

        sendBroadcast(true);

        UI.getCurrent().navigate(CurrentExamsView.class);
        Notification.show("Prüfung erfolgreich eingereicht.").addThemeVariants(NotificationVariant.LUMO_SUCCESS);
    }

    /**
     * The answers of the examinee are being rated for their correctness
     */
    private void automaticAnswerEvaluation() {
        AnswerRatingService ratingService = new AnswerRatingService();
        examinee.getAnswers().forEach(ratingService::evaluate);
    }

    /**
     * When the View is being left the timer has to be canceled, if it is still running
     * @param beforeLeaveEvent Method gets called while leaving the View
     */
    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        timer.cancel();
    }

    /**
     * Sends broadcast with information about current exam progress
     *
     * @author Josh Knipping
     */
    public void sendBroadcast(boolean finished){
        ExamAppointment appointment = (ExamAppointment) UI.getCurrent().getSession().getAttribute("appointment");

        ExamStatusService.ExamStatus examStatus = new ExamStatusService.ExamStatus();
        examStatus.setExaminee(examinee);
        examStatus.setQuestionsAnswered((int) testAnswers.stream().filter(Answer::isAnswered).count());
        examStatus.setFinished(finished);

        examStatusService.broadcastUpdate(appointment, examStatus);
    }

    /**
     * Always false
     */
    public void sendBroadcast(){
        sendBroadcast(false);
    }
}
