package de.fh_bielefeld.online_testat.ui.examinee;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.model.exam.Examinee;
import de.fh_bielefeld.online_testat.repositories.ExamAppointmentRepository;
import de.fh_bielefeld.online_testat.ui.general.GeneralLayout;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;

import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * In this view the user selects their exam
 *
 * @author Ole Niediek
 */
@PageTitle("Prüfungen")
@Route(value = "participate/:examAppointmentID", layout = GeneralLayout.class)
@AnonymousAllowed
public class ParticipationView extends VerticalLayout implements BeforeEnterObserver, AfterNavigationObserver {

    private final Grid<Examinee> examineeGrid = new Grid<>(Examinee.class);

    private final ExamAppointmentRepository examAppointmentRepository;
    private ExamAppointment appointment;

    private final ExamUtils examUtils;

    /**
     * Constructor
     * @param examAppointmentRepository The data for this view is mainly going to come from ExamAppointments from this repository
     */
    public ParticipationView(ExamAppointmentRepository examAppointmentRepository, ExamUtils examUtils) {
        this.examAppointmentRepository = examAppointmentRepository;
        this.examUtils = examUtils;
    }

    /**
     * The appointment the user has chosen is taken from the repository
     * If something goes wrong, for example the examAppointmentID cannot be found, the user gets redirected to the CurrentExamsView
     * @param event Method gets called while entering the View
     */
    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Optional<String> examAppointmentParam = event.getRouteParameters().get("examAppointmentID");

        if(UI.getCurrent().getSession().getAttribute("submitTime") == null) {
            event.rerouteTo(CurrentExamsView.class);
            Notification.show("Ein Fehler ist aufgetreten. Bitte wählen Sie Ihre Prüfung erneut")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
        }

        if(examAppointmentParam.isEmpty()) {
            event.forwardTo(CurrentExamsView.class);
            Notification.show("Der gewählte Prüfungstermin konnte nicht gefunden werden!")
                    .addThemeVariants(NotificationVariant.LUMO_ERROR);
            return;
        }

        Optional<ExamAppointment> loadedAppointment = examUtils.getExamAppointmentFromBeforeEvent(event);

        loadedAppointment.ifPresentOrElse(appointment -> {
            this.appointment = appointment;
        }, () -> {
            event.forwardTo(CurrentExamsView.class);
        });

    }

    /**
     * The chart with all the examinees who are registered for this exam at this appointment
     * @param afterNavigationEvent Method gets called immediately after entering the View
     */
    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        setAlignItems(Alignment.CENTER);

        // Title
        Label titleLabel = new Label(appointment.getExam().getName());
        titleLabel.getStyle().set("font-size", "24pt");

        // Time
        Label timeLabel = new Label(formatAppointmentDate());
        timeLabel.getStyle().set("font-size", "16pt");

        // Instructions
        Label instructionLabel = new Label("Bitte wählen Sie Ihren Namen aus, um die Prüfung für Sie zu starten.");

        // A new layout is needed to center the examinee grid
        var examineeGridLayout = new VerticalLayout();
        examineeGridLayout.setWidth("50%");

        // Exminee List
        examineeGrid.setSelectionMode(Grid.SelectionMode.NONE);

        examineeGrid.setColumns("name"); //Name of the examinee
        examineeGrid.getColumnByKey("name").setHeader("Name").setWidth("38%");
        examineeGrid.addColumn(Examinee::getMatriculationNumber).setHeader("Matrikelnummer").setWidth("38%"); //The examinees matriculationnumber
        examineeGrid.addComponentColumn(examinee ->  {//The butten to choose your exam
            Button participateButton = new Button("Teilnehmen");
            participateButton.addClickListener(e -> {
                UI.getCurrent().getSession().setAttribute("examinee", examinee);
                UI.getCurrent().getSession().setAttribute("appointment", appointment);
                if(UI.getCurrent().getSession().getAttribute("examinee") != null)
                    UI.getCurrent().navigate(QuestionView.class, new RouteParameters("examID", appointment.getExam().getID().toString()));
            });
            if(!examinee.getAnswers().isEmpty()) {//If an examinee has already submitted their answers they cannot access the exam again
                participateButton.setVisible(false);
                participateButton.setEnabled(false);
            }
            return participateButton;
        }).setWidth("24%");

        examineeGrid.setItems(appointment.getExaminees());

        examineeGridLayout.add(examineeGrid);

        add(titleLabel, timeLabel, instructionLabel, examineeGridLayout);
    }

    /**
     * A method to display the start- and endtime of an appointment properly
     * @return String consisting of the start- and endtime
     */
    private String formatAppointmentDate() {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

        String startTime = timeFormatter.format(appointment.getPlannedStartTime());
        String endTime = timeFormatter.format(appointment.getPlannedStartTime().plusMinutes(appointment.getExam().getDuration()));

        return startTime + " - " + endTime + " Uhr";
    }
}
