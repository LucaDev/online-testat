package de.fh_bielefeld.online_testat.ui.examinee;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.repositories.ExamAppointmentRepository;
import de.fh_bielefeld.online_testat.services.FilterExamAppointments;
import de.fh_bielefeld.online_testat.ui.examiner.LiveResultsView;
import de.fh_bielefeld.online_testat.ui.general.GeneralLayout;
import de.fh_bielefeld.online_testat.ui.security.SecurityUtils;
import de.fh_bielefeld.online_testat.ui.utils.BadgeUtils;
import de.fh_bielefeld.online_testat.ui.utils.DateTimeUtils;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * View for displaying all exams sorted by current date
 * -> When an examiner is logged in, he can call up the live view for one of his current exams
 * -> An examinee can participate in a current exam
 *
 * @author Lucie Jahn, Ole Niediek
 */
@PageTitle("Prüfungen")
@Route(value = "/", layout = GeneralLayout.class)
@AnonymousAllowed
public class CurrentExamsView extends VerticalLayout {
    private final ExamAppointmentRepository examAppointmentRepository;
    private final Grid<ExamAppointment> currentExamsGrid = new Grid<>(ExamAppointment.class);

    /**
     * Constructor
     * @param examAppointmentRepository The data for this view is mainly going to come from ExamAppointments from this repository
     */
    public CurrentExamsView(ExamAppointmentRepository examAppointmentRepository) {
        this.examAppointmentRepository = examAppointmentRepository;

        var currentDate = new Span("Prüfungen Heute - " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        currentDate.getStyle().set("font-weight", "bold");

        this.setHeightFull();
        add(currentDate, getCurrentExamsGrid());
    }

    /**
     * configure grid by setting all columns & sort by current dateTime
     * @return Grid<ExamAppointment> Grid that displays all currently or in future accessible appointments to different exams
     */
    private Grid<ExamAppointment> getCurrentExamsGrid() {
        currentExamsGrid.setSelectionMode(Grid.SelectionMode.NONE);
        currentExamsGrid.setWidthFull();
        currentExamsGrid.setHeightFull();

        currentExamsGrid.removeAllColumns();
        currentExamsGrid.addComponentColumn(appointment -> {
                    Span span = new Span(appointment.getExam().getName() + "  ");
                    span.add(BadgeUtils.getExamStatusBadge(appointment));

                    return span;
                })
                .setHeader("Prüfung")
                .setAutoWidth(true);
        currentExamsGrid.addColumn(appointment -> DateTimeUtils.formatDuration(appointment.getExam()))
                .setHeader("Dauer")
                .setAutoWidth(true);
        currentExamsGrid.addColumn(DateTimeUtils::formatAppointmentDate)
                .setHeader("Termin")
                .setSortable(true)
                .setComparator(ExamAppointment::getPlannedStartTime)
                .setAutoWidth(true)
                .setKey("appointment");

        var sort = List.of(
                new GridSortOrder<>(currentExamsGrid.getColumnByKey("appointment"), com.vaadin.flow.data.provider.SortDirection.ASCENDING)
        );
        currentExamsGrid.sort(sort);

        if(SecurityUtils.isUserLoggedIn()){
            addShowLiveResultsButtonColumn();
        } else {
            addParticipationButtonColumn();
        }

        filterCurrentExams();
        return currentExamsGrid;
    }

    /**
     * only show current & future exams in grid
     */
    private void filterCurrentExams() {
        FilterExamAppointments filter = new FilterExamAppointments();
        GridListDataView<ExamAppointment> currentExamsDataView = currentExamsGrid.setItems(filter.filterAppointmentsForDate(examAppointmentRepository));
        currentExamsDataView.addFilter(filter::checkActualStartTime);
    }

    /**
     * Default: When no one is logged in, there will be a column with participation buttons for current exams
     */
    private void addParticipationButtonColumn() {
        currentExamsGrid.addComponentColumn(appointment -> {
            Button participateButton = new Button("Teilnehmen");
            addVisibilityFilterToParticipationButton(appointment, participateButton);
            participateButton.addClickListener(e -> {
                UI.getCurrent().getSession().setAttribute("submitTime", appointment.getActualStartTime().plusMinutes(appointment.getExam().getDuration()));
                UI.getCurrent().navigate(ParticipationView.class, new RouteParameters("examAppointmentID", appointment.getID().toString()));
            });

            return participateButton;
        }).setAutoWidth(true);
    }

    /**
     * only show participation button for appointment, if plannedStartTime is after currentTime & endTime is before currentTime
     * @param appointment The appointment in question
     * @param button The button, that will direct the user to the ParticipationView
     */
    private void addVisibilityFilterToParticipationButton(ExamAppointment appointment, Button button) {
        if(!appointment.hasStarted()) {
            button.setVisible(false);
            return;
        }

        LocalDateTime endTime = ExamUtils.getAppointmentEndTime(appointment.getActualStartTime(), appointment);
        button.setVisible(LocalDateTime.now().isBefore(endTime));
    }

    /**
     * If examiner is logged in, there will be a column with livebuttons for current exams
     */
    private void addShowLiveResultsButtonColumn() {
        currentExamsGrid.addComponentColumn(appointment -> {
            Button liveResultsButton = new Button("Live");
            liveResultsButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
            addVisibilityFilterToLiveButton(appointment, liveResultsButton);
            liveResultsButton.addClickListener(e -> UI.getCurrent().navigate(LiveResultsView.class, new RouteParameters("examAppointmentID", appointment.getID().toString())));

            return liveResultsButton;
        }).setAutoWidth(true);
    }

    /**
     * only show live button for appointment, if plannedStartTime is after currentTime & endTime is before currentTime
     * @param appointment The appointment in question
     * @param button The button, that will direct the user to the LifeView
     */
    private void addVisibilityFilterToLiveButton(ExamAppointment appointment, Button button) {
        LocalDateTime startTime = appointment.getPlannedStartTime();
        LocalDateTime endTime = ExamUtils.getAppointmentEndTime(startTime, appointment);

        button.setVisible(LocalDateTime.now().isAfter(startTime) && LocalDateTime.now().isBefore(endTime));
    }
}
