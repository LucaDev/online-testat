package de.fh_bielefeld.online_testat.services;

import com.opencsv.*;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import de.fh_bielefeld.online_testat.model.exam.Examinee;
import de.fh_bielefeld.online_testat.model.question.*;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;
import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author Luca Kröger, Josh Knipping
 */
@Service
public class ExamSerializationService {
    // Support csv and xslx (Excel > 2007)
    public static final String EXCEL_FORMAT = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String CSV_FORMAT = "text/csv";
    public static final String[] ACCEPTED_FORMATS = {CSV_FORMAT, EXCEL_FORMAT};

    private static final char CSV_SEPERATOR = CSVWriter.DEFAULT_SEPARATOR;

    // Based of Vaadins rich text editor whitelist
    private static final Safelist JSOUP_SAFELIST = org.jsoup.safety.Safelist.basic()
            .addTags("img", "h1", "h2", "h3", "s")
            .addAttributes("img", "align", "alt", "height", "src", "title", "width")
            .addAttributes(":all", "style")
            .addProtocols("img", "src", "data");

    private final Logger logger = LoggerFactory.getLogger(ExamSerializationService.class);
    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    /**
     * Reads {@link Examinee}s from a file
     * @param filename the filename of the file - decides if excel or csv deserialization will be tried
     * @param fileInputStream the file to be read
     * @return List of {@link Examinee}s contained in the file
     * @throws ImportException on import errors
     */
    public List<Examinee> readExamineesFromFile(String filename, InputStream fileInputStream) throws ImportException {
        List<Examinee> examinees = new ArrayList<>();

        try {
            if(filename.toLowerCase().endsWith(".xlsx")) {
                var workbook = new XSSFWorkbook(fileInputStream);
                var iterator = workbook.getSheetAt(0).iterator();

                iterator.forEachRemaining(row -> {
                    var examinee = new Examinee(
                            Double.valueOf(row.getCell(0).getNumericCellValue()).intValue(),
                            row.getCell(1).getStringCellValue()
                    );

                    Set<ConstraintViolation<Examinee>> violations = validator.validate(examinee);

                    if(!violations.isEmpty())
                        throw new ImportException(violations.iterator().next().getMessage());

                    examinees.add(examinee);
                });
            } else {
                CsvToBean<Examinee> cb = new CsvToBeanBuilder<Examinee>(new InputStreamReader(fileInputStream))
                    .withType(Examinee.class)
                    .withSeparator(CSV_SEPERATOR)
                    .build();

                cb.forEach(examinee -> {
                    Set<ConstraintViolation<Examinee>> violations = validator.validate(examinee);

                    if(!violations.isEmpty())
                        throw new ImportException(violations.iterator().next().getMessage());

                    examinees.add(examinee);
                });
            }
        } catch (IOException | NumberFormatException e) {
            logger.warn("Error importing examinees from file", e);
            throw new ImportException("Die Datei besitzt ein fehlerhaftes Format");
        }

        return examinees;
    }

    /**
     * Reads {@link Question}s from a file
     * @param filename the filename of the file - decides if excel or csv deserialization will be tried
     * @param fileInputStream the file to be read
     * @return List of {@link Question}s contained in the file
     * @throws ImportException on import errors
     */
    public List<Question> readQuestionsFromFile(String filename, InputStream fileInputStream) {
        List<Question> questions = new ArrayList<>();

        try {
            if(filename.toLowerCase().endsWith(".xlsx")) {
                var workbook = new XSSFWorkbook(fileInputStream);
                var iterator = workbook.getSheetAt(0).iterator();

                iterator.forEachRemaining(row -> {
                    String questionText = row.getCell(0).getStringCellValue();
                    double maxPoints = row.getCell(1).getNumericCellValue();
                    var questionType = row.getCell(2).getStringCellValue();

                    List<Choice> choices = new ArrayList<>();
                    for(int i=3; i < row.getLastCellNum(); i+=2) {
                        choices.add(new Choice(row.getCell(i).getStringCellValue(), row.getCell(i + 1).getBooleanCellValue()));
                    }

                    var question = constructQuestionFromText(questionType, questionText, maxPoints, choices);

                    questions.add(question);
                });
            } else {
                CSVParser parser = new CSVParserBuilder().withSeparator(CSV_SEPERATOR).build();
                CSVReader reader = new CSVReaderBuilder(new InputStreamReader(fileInputStream)).withCSVParser(parser).build();

                reader.forEach(line -> {
                    String questionText = line[0];
                    double maxPoints = Double.parseDouble(line[1]);
                    String questionType = line[2];

                    List<Choice> choices = new ArrayList<>();

                    if(line.length > 3) {
                        for (int i = 3; i < line.length; i += 2) {
                            choices.add(new Choice(line[i], Boolean.parseBoolean(line[i + 1])));
                        }
                    }

                    var question = constructQuestionFromText(questionType, questionText, maxPoints, choices);

                    questions.add(question);
                });
            }
        } catch (IOException | NumberFormatException e) {
            logger.warn("Error importing questions from file", e);
            throw new ImportException("Die Datei besitzt ein fehlerhaftes Format");
        }

        questions.forEach(question -> {
            Set<ConstraintViolation<Question>> violations = validator.validate(question);

            if(!violations.isEmpty())
                throw new ImportException(violations.iterator().next().getMessage());
        });

        return questions;
    }

    /**
     * Constructs a Question from raw text input parameters
     * @param questionType question type field content
     * @param questionText question text field content
     * @param maxPoints points field content
     * @param choices List of choices if applicable - null if not
     * @return deserialized question
     * @throws ImportException on construction errors
     */
    private Question constructQuestionFromText(String questionType, String questionText, double maxPoints, List<Choice> choices) throws ImportException {
        questionText = sanitizeHTMLInput(questionText);

        try {
            switch (QuestionType.fromName(questionType)) {
                case TEXT -> {
                    return new TextQuestion(questionText, maxPoints);
                }
                case MULTIPLE_CHOICE -> {
                    return new MultipleChoiceQuestion(
                            questionText,
                            choices,
                            maxPoints
                    );
                }
                case SINGLE_CHOICE -> {
                    var correctAnswers = choices.stream().filter(Choice::isCorrectAnswer).count();
                    if(correctAnswers != 1)
                        throw new ImportException("Single Choice Frage \"" + questionText.substring(Math.min(questionText.length(), 15)) + "...\" besitzt nicht eine einzige richtige Antwort");

                    return new SingleChoiceQuestion(
                            questionText,
                            choices,
                            maxPoints
                    );
                }
                default -> throw new ImportException("Fragetyp \""+questionType+"\" nicht implementiert");
            }
        } catch (IllegalArgumentException ex) {
            throw new ImportException("Der Fragetyp \""+questionType+"\" ist unbekannt");
        }
    }

    /**
     * Serializes a List of {@link Question}s into a CSV file
     * @param questions List of {@link Question}s to serialize
     * @return CSV File as String
     */
    @SneakyThrows
    public String exportQuestionsToCSV(List<Question> questions) {
        var writer = new StringWriter();
        var csvWriter = new CSVWriter(writer);

        questions.forEach(question -> {
            List<String> lines = new ArrayList<>();

            lines.add(question.getQuestionHTML());
            lines.add(String.valueOf(question.getMaxPoints()));
            lines.add(question.getType().getName());

            if(question instanceof ChoiceQuestion) {
                ((ChoiceQuestion) question).getChoices().forEach(choice -> {
                    lines.add(choice.getText());
                    lines.add(String.valueOf(choice.isCorrectAnswer()));
                });
            }

            csvWriter.writeNext(lines.toArray(new String[]{}));
        });

        return writer.toString();
    }

    /**
     * Serializes a List of {@link Examinee}s into a CSV file
     * @param questions List of {@link Examinee}s to serialize
     * @return CSV File as String
     */
    @SneakyThrows
    public String exportExamineesToCSV(List<Examinee> examinees) {
        var writer = new StringWriter();

        StatefulBeanToCsv<Examinee> beanWriter = new StatefulBeanToCsvBuilder<Examinee>(writer)
            .withSeparator(CSV_SEPERATOR)
            .build();

        beanWriter.write(examinees);

        return writer.toString();
    }

    /**
     * Sanitizes a string based on VAADINs rules for richtext
     * @param text string to be sanitized
     * @return sanitized text
     */
    private String sanitizeHTMLInput(String text) {
        return Jsoup.clean(text, JSOUP_SAFELIST);
    }

    /**
     * Creates a .xlsx (Excel) file and writes results of the provides examinees into it.
     * @param examinees Collection of questions
     * @return returns a xlsx file with the currently displayed examinees including percents, grades and averages
     *
     * @author Luca Kröger, Josh Knipping
     */
    public byte[] exportResultsToExcel(Collection<Examinee> examinees) {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Prüfungsergebnisse");

        try (ByteArrayOutputStream fileOut = new ByteArrayOutputStream()){

            //Header Row
            Row headerRow = sheet.createRow(0);
            Cell name = headerRow.createCell(0);
            name.setCellValue("Name");
            Cell percent = headerRow.createCell(1);
            percent.setCellValue("Prozent");
            Cell grade = headerRow.createCell(2);
            grade.setCellValue("Note");

            int rowNumber = 1;
            for (Examinee examinee: examinees) {
                Row row = sheet.createRow(rowNumber++);
                row.createCell(0).setCellValue(examinee.getName()); //get name
                row.createCell(1).setCellValue(ExamUtils.getPercentage(examinee));  //get percent
                row.createCell(2).setCellValue(ExamUtils.getGrade(examinee)); // get grade
            }

            rowNumber++;
            Row footerRowHeader = sheet.createRow(rowNumber);
            Cell resultsTitle = footerRowHeader.createCell(0);
            resultsTitle.setCellValue("Durchschnitt");

            Cell percentAverage = footerRowHeader.createCell(1);
            percentAverage.setCellValue(ExamUtils.getPercentAverage(examinees));
            Cell gradeAverage = footerRowHeader.createCell(2);
            gradeAverage.setCellValue(ExamUtils.getGradeAverage(examinees));

            rowNumber++;
            Row footerRow = sheet.createRow(rowNumber);
            footerRow.createCell(0);

            //Resize all columns
            for(int i = 0; i < examinees.size()+3; i++){
                sheet.autoSizeColumn(i);
            }

            workbook.write(fileOut);
            fileOut.close();
            workbook.close();

            return fileOut.toByteArray();
        } catch (IOException exception) {
            throw new ImportException("Export Fehlgeschlagen!");
        }
    }

    /**
     * Creates a .xlsx file and writes the provided question informations into it.
     * @param questions Collection of questions
     * @return returns a xlsx file with the currently displayed questions including text, typ points and choices
     *
     * @author Josh Knipping
     */
    public byte[] exportQuestionsToExcel(Collection<Question> questions){

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Prüfungsaufgaben");

        try (ByteArrayOutputStream fileOut = new ByteArrayOutputStream()) {

            int rowNumber = 0;
            int cellsMax = 0;
            for (Question question: questions) {
                Row row = sheet.createRow(rowNumber++);
                row.createCell(0).setCellValue(question.getQuestionHTML()); //get question text
                row.createCell(1).setCellValue(question.getMaxPoints()); // get points
                row.createCell(2).setCellValue(question.getType().getName());  //get question type

                if(question instanceof ChoiceQuestion){
                    int cellnumber = 3;
                    for (Choice choice : ((ChoiceQuestion) question).getChoices()) {
                        row.createCell(cellnumber).setCellValue(choice.getText());
                        cellnumber++;
                        row.createCell(cellnumber).setCellValue(choice.isCorrectAnswer());
                        cellnumber++;
                    }
                    cellsMax = cellnumber;
                }
            }

            //Resize all columns
            for(int i = 0; i < cellsMax+1; i++){
                sheet.autoSizeColumn(i);
            }

            workbook.write(fileOut);
            fileOut.close();
            workbook.close();

            return fileOut.toByteArray();
        } catch (IOException exception) {
            throw new ImportException("Export Fehlgeschlagen!");
        }
    }

    /**
     * Creates a .xlsx file and writes the provided examinee informations into it.
     * @param examinees Collection of examinees
     * @return returns a xlsx file with the currently displayed examinee information
     *
     * @author Josh Knipping
     */
    public byte[] exportExamineesToExcel(Collection<Examinee> examinees){

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Teilnehmer");

        try (ByteArrayOutputStream fileOut = new ByteArrayOutputStream()) {

            int rowNumber = 0;
            for (Examinee examinee: examinees) {
                Row row = sheet.createRow(rowNumber++);
                row.createCell(0).setCellValue(examinee.getMatriculationNumber()); //get matrikelnumber
                row.createCell(1).setCellValue(examinee.getName());  //get name
            }

            //Resize all columns
            for(int i = 0; i < examinees.size()+2; i++){
                sheet.autoSizeColumn(i);
            }

            workbook.write(fileOut);
            fileOut.close();
            workbook.close();

            return fileOut.toByteArray();
        } catch (IOException exception) {
            throw new ImportException("Export Fehlgeschlagen!");
        }
    }
}
