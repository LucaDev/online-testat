package de.fh_bielefeld.online_testat.services;

import de.fh_bielefeld.online_testat.model.examiner.Examiner;
import de.fh_bielefeld.online_testat.repositories.ExaminerRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Provides the user repository to spring security
 * {@inheritDoc}
 * @author Luca Kröger
 */
@Service
public class ExaminerDetailsService implements UserDetailsService {
    final ExaminerRepository examinerRepository;

    public ExaminerDetailsService(ExaminerRepository examinerRepository) {
        this.examinerRepository = examinerRepository;
    }

    /**
     *
     * @param username the case-insensitive username of the {@link de.fh_bielefeld.online_testat.model.exam.Examinee}
     * @return returns the User if found
     * @throws UsernameNotFoundException if the user could not be found
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Examiner examiner = examinerRepository.findByUsernameIgnoreCase(username);

        if (examiner == null)
            throw new UsernameNotFoundException(username);

        return examiner;
    }
}
