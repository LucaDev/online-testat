package de.fh_bielefeld.online_testat.services;

import com.vaadin.flow.shared.Registration;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.model.exam.Examinee;
import de.fh_bielefeld.online_testat.repositories.ExamAppointmentRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.atmosphere.cpr.Broadcaster;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Manages all status updates and keeps track of all currently attending examinees and their status
 * @author Luca Kröger
 */
@Service
public class ExamStatusService {
    private final Executor executor = Executors.newSingleThreadExecutor();

    private final HashMap<Long, LinkedList<Consumer<Collection<ExamStatus>>>> listeners = new HashMap<>();
    private final ConcurrentHashMap<Long, HashMap<Long, ExamStatus>> currentStatus = new ConcurrentHashMap<>();

    private final ExamAppointmentRepository appointmentRepository;

    public ExamStatusService(ExamAppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

    /**
     * Registers a new event listener for status updates on a specific appointment
     * @param appointment appointment to register a listener for
     * @param listener the function called when a new update was broadcast
     * @return a Registration for unsubscribing the broadcaster
     */
    public synchronized Registration register(ExamAppointment appointment, Consumer<Collection<ExamStatus>> listener) {
        listeners.computeIfAbsent(appointment.getID(), f -> new LinkedList<>());

        listeners.get(appointment.getID()).push(listener);

        return () -> {
            synchronized (Broadcaster.class) {
                listeners.get(appointment.getID()).remove(listener);

                if(listeners.get(appointment.getID()).size() == 0) {
                    listeners.remove(appointment.getID());
                }
            }
        };
    }

    /**
     * Broadcasts an update about the current status of an Examinee
     * @param appointment the appointment the update is for
     * @param statusMessage The update message that will be broadcast
     */
    public synchronized void broadcastUpdate(ExamAppointment appointment, ExamStatus statusMessage) {
        currentStatus.computeIfAbsent(appointment.getID(), k -> new HashMap<>());

        currentStatus.get(appointment.getID()).put(statusMessage.getExaminee().getID(), statusMessage);

        if(listeners.get(appointment.getID()) != null) {
            for (var listener : listeners.get(appointment.getID())) {
                executor.execute(() -> listener.accept(currentStatus.get(appointment.getID()).values()));
            }
        }
    }

    /**
     * Returns a set of ExamStatus for all Examinees of a specific Exam
     * @param appointment the appointment the status request is for
     * @return Set of the ExamStatus of all Examinees
     */
    public Collection<ExamStatus> getCurrentStatus(ExamAppointment appointment) {
        currentStatus.computeIfAbsent(appointment.getID(), k -> new HashMap<>());

        return currentStatus.getOrDefault(appointment.getID(), new HashMap<>()).values();
    }

    // Replace w/ record?
    /**
     * Contains all data for an exam status update
     */
    @Getter
    @Setter
    @ToString
    public static class ExamStatus {
        private Examinee examinee;
        private int questionsAnswered;
        boolean finished;
    }

    @Scheduled(fixedRate = 15, timeUnit = TimeUnit.MINUTES)
    public void cleanup() {
        currentStatus.keySet().forEach(appointmentID -> {
            var appointment = appointmentRepository.findByID(appointmentID);
            if(appointment.isPresent() && appointment.get().isOver()) {
                currentStatus.remove(appointmentID);
            }
        });
    }
}
