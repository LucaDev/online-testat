package de.fh_bielefeld.online_testat.services;

/**
 * An for file imports (e.g. a question import from an excel file)
 */
public class ImportException extends RuntimeException {
    /**
     * @param message human readable error message
     */
    public ImportException(String message) {
        super(message);
    }
}
