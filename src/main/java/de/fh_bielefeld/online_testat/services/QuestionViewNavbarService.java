package de.fh_bielefeld.online_testat.services;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;

/**
 * A Class containing methods to help create and design the Navbar and the DrawerToggle in the QuestionView
 *
 * @author Ole Niediek
 */
public class QuestionViewNavbarService {
    /**
     * Set the HTML-Style for every object in the navbar
     * @param text The displayed text
     * @param marginSite The site to orient the margin (left or right)
     * @param margin The margin to the left or right of the object (in % or px)
     * @param position Specifies the way the objects are positioned (static, relative, fixed, absolute, sticky)
     * @return The new H1 object with a set style
     */
    public H1 getTitleForNavbar(String text, String marginSite, String margin, String position){
        H1 title = new H1(text);
        title.getStyle().set("font-size", "var(--lumo-font-size-l)").set(marginSite, "var(--lumo-space-l)");
        if(marginSite.equals("left"))
            title.getStyle().set("margin", "0px 0px 0px " + margin).set("position", position);
        else if(marginSite.equals("right"))
            title.getStyle().set("margin", "0px " + margin + " 0px 0px").set("position", position);
        return title;
    }

    /**
     * For every Question in the test a Tab in the DrawerToggle gets created
     * If the user clicks on the Tab the updateQuestion-method called and the question gets displayed accordingly
     * @param questionCount number of questions to be displayed in the tabs
     * @return All the Tabs at ones in the order they will be displayed in
     */
    public Tabs getTabs(int questionCount) {
        Tabs tabs = new Tabs();
        for(int i = 1; i <= questionCount; i++){
            tabs.add(createTab(i));
        }
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        return tabs;
    }

    /**
     * A Tab for a single question gets created
     * @param questionNumber Number of the question in the test (always one higher than the questions index in tabs)
     * @return A single Tab
     */
    public Tab createTab(int questionNumber) {
        Tab tab = new Tab();
        Icon icon = VaadinIcon.CIRCLE_THIN.create();
        icon.getStyle().set("box-sizing", "border-box").set("margin-inline-end", "var(--lumo-space-m)")
                .set("margin-inline-start", "var(--lumo-space-xs)").set("padding", "var(--lumo-space-xs)");
        Span text = new Span("Frage " + questionNumber);
        tab.add(icon, text);
        return tab;
    }

    /**
     * When a question is answered a new icon called CHECK_CIRCLE is supposed to appear in the Tab
     * If the user deletes their answer the old CIRCLE_THIN appears again
     * @param questionUnanswered is true when the question is unanswered
     * @return VaadinIcon
     */
    public Icon changeTabIcon(boolean questionUnanswered) {
        Icon icon;
        if(questionUnanswered)
            icon = VaadinIcon.CIRCLE_THIN.create();
        else
            icon = VaadinIcon.CHECK_CIRCLE.create();
        icon.getStyle().set("box-sizing", "border-box").set("margin-inline-end", "var(--lumo-space-m)")
                .set("margin-inline-start", "var(--lumo-space-xs)").set("padding", "var(--lumo-space-xs)");
        return icon;
    }
}
