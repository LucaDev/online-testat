package de.fh_bielefeld.online_testat.services;

import de.fh_bielefeld.online_testat.model.answer.ChoiceAnswer;
import de.fh_bielefeld.online_testat.model.answer.TextAnswer;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.model.exam.Examinee;
import de.fh_bielefeld.online_testat.model.examiner.Examiner;
import de.fh_bielefeld.online_testat.model.question.*;
import de.fh_bielefeld.online_testat.repositories.ExamRepository;
import de.fh_bielefeld.online_testat.repositories.ExaminerRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates dirty test data
 * Warning: This service is solely for testing purposes and is only active with the "dev" profile.
 */
@Service
@Profile("dev")
public class TestService {
    final ExamRepository examRepository;
    final ExaminerRepository examinerRepository;

    public TestService(ExamRepository examRepository, ExaminerRepository examinerRepository) {
        this.examRepository = examRepository;
        this.examinerRepository = examinerRepository;

        // Add/set the "user" with the password "password".
        var admin = new Examiner("admin", "{noop}password");
        admin.setAdmin(true);

        var user = new Examiner("user", "{noop}password");

        try {
            examinerRepository.save(admin);
            examinerRepository.save(user);
        } catch (Exception ignored){}

        // Create a new exam to save it to the repo
        var exam = new Exam();
        exam.setName("Softwareprojekt (WI/P 123456) - Java Grundlagen");
        exam.setMixupQuestionsEnabled(true);
        exam.setRandomlyMixChoices(true);
        exam.setFullyChecked(false);
        exam.setDuration(90);

        ArrayList<Question> questions = generateExamQuestions2();
        exam.setQuestions(questions);

        var examinees = new ArrayList<Examinee>();
        for (int i = 0; i < 5; i++) {
            examinees.add(new Examinee(1000000+i, "Tester " + i));
        }
        //generateExamineeAnswers(questions, examinees);

        var examinees2 = new ArrayList<Examinee>();
        for (int i = 5; i < 10; i++) {
            examinees2.add(new Examinee(1000000+i, "Tester " + i));
        }
        //generateExamineeAnswers(questions, examinees2);

        var examinees3 = new ArrayList<Examinee>();
        for (int i = 10; i < 15; i++) {
            examinees3.add(new Examinee(1000000+i, "Tester " + i));
        }
        //generateExamineeAnswers(questions, examinees3);

        exam.addAppointment(new ExamAppointment(examinees, LocalDateTime.now()));
        exam.addAppointment(new ExamAppointment(examinees2, LocalDateTime.now().plusDays(1)));
        exam.addAppointment(new ExamAppointment(examinees3, LocalDateTime.now().plusMonths(1)));

        examRepository.save(exam);
    }

    private ArrayList<Question> generateExamQuestions() {
        var questions = new ArrayList<Question>();
        for (int i = 0; i < 3; i++) {
            questions.add(new TextQuestion("<p>Freitext Frage Nr." + i + "</p>", i+1));
            questions.add(new MultipleChoiceQuestion("<p>Multiple-Choice Frage Nr." + i + "</p>", List.of(
                    new Choice("Multiple-Choice 1 für Frage Nr." + i, false),
                    new Choice("Multiple-Choice 2 für Frage Nr." + i, true),
                    new Choice("Multiple-Choice 3 für Frage Nr." + i, false),
                    new Choice("Multiple-Choice 4 für Frage Nr." + i, true)
            ), i+1d));
            questions.add(new SingleChoiceQuestion("<p>SingleChoice-Frage Nr." + i + "</p>", List.of(
                    new Choice("Single-Choice 1 für Frage Nr." + i, true),
                    new Choice("Single-Choice 2 für Frage Nr." + i, false),
                    new Choice("Single-Choice 3 für Frage Nr." + i, false),
                    new Choice("Single-Choice 4 für Frage Nr." + i, false)
            ), i+1));
        }
        return questions;
    }

    private ArrayList<Question> generateExamQuestions2(){
        var questions = new ArrayList<Question>();
        questions.add(new TextQuestion("<p>Freitext Frage Nr." + 0 + "</p>", 1));
        questions.add(new MultipleChoiceQuestion("<p>Multiple-Choice Frage Nr." + 1 + "</p>", List.of(
                new Choice("Multiple-Choice 1 für Frage Nr." + 1, false),
                new Choice("Multiple-Choice 2 für Frage Nr." + 1, true)
        ), 2d));
        questions.add(new SingleChoiceQuestion("<p>SingleChoice-Frage Nr." + 2 + "</p>", List.of(
                new Choice("Single-Choice 1 für Frage Nr." + 2, true),
                new Choice("Single-Choice 2 für Frage Nr." + 2, false),
                new Choice("Single-Choice 3 für Frage Nr." + 2, false)
        ), 3));
        questions.add(new MultipleChoiceQuestion("<p>Multiple-Choice Frage Nr." + 3 + "</p>", List.of(
                new Choice("Multiple-Choice 1 für Frage Nr." + 3, false),
                new Choice("Multiple-Choice 2 für Frage Nr." + 3, false),
                new Choice("Multiple-Choice 3 für Frage Nr." + 3, true),
                new Choice("Multiple-Choice 4 für Frage Nr." + 3, true)
        ), 4d));
        questions.add(new SingleChoiceQuestion("<p>SingleChoice-Frage Nr." + 4 + "</p>", List.of(
                new Choice("Single-Choice 1 für Frage Nr." + 4, true),
                new Choice("Single-Choice 2 für Frage Nr." + 4, false),
                new Choice("Single-Choice 3 für Frage Nr." + 4, false),
                new Choice("Single-Choice 4 für Frage Nr." + 4, false),
                new Choice("Single-Choice 5 für Frage Nr." + 4, false)
        ), 5));
        questions.add(new MultipleChoiceQuestion("<p>Multiple-Choice Frage Nr." + 5 + "</p>", List.of(
                new Choice("Multiple-Choice 1 für Frage Nr." + 5, false),
                new Choice("Multiple-Choice 2 für Frage Nr." + 5, false),
                new Choice("Multiple-Choice 3 für Frage Nr." + 5, false),
                new Choice("Multiple-Choice 4 für Frage Nr." + 5, true),
                new Choice("Multiple-Choice 5 für Frage Nr." + 5, true),
                new Choice("Multiple-Choice 6 für Frage Nr." + 5, true)
        ), 6d));
        questions.add(new SingleChoiceQuestion("<p>SingleChoice-Frage Nr." + 6 + "</p>", List.of(
                new Choice("Single-Choice 1 für Frage Nr." + 6, true),
                new Choice("Single-Choice 2 für Frage Nr." + 6, false),
                new Choice("Single-Choice 3 für Frage Nr." + 6, false),
                new Choice("Single-Choice 4 für Frage Nr." + 6, false),
                new Choice("Single-Choice 5 für Frage Nr." + 6, false),
                new Choice("Single-Choice 6 für Frage Nr." + 6, false),
                new Choice("Single-Choice 7 für Frage Nr." + 6, false)
        ), 7));
        questions.add(new MultipleChoiceQuestion("<p>Multiple-Choice Frage Nr." + 7 + "</p>", List.of(
                new Choice("Multiple-Choice 1 für Frage Nr." + 7, false),
                new Choice("Multiple-Choice 2 für Frage Nr." + 7, false),
                new Choice("Multiple-Choice 3 für Frage Nr." + 7, false),
                new Choice("Multiple-Choice 4 für Frage Nr." + 7, false),
                new Choice("Multiple-Choice 5 für Frage Nr." + 7, true),
                new Choice("Multiple-Choice 6 für Frage Nr." + 7, true),
                new Choice("Multiple-Choice 7 für Frage Nr." + 7, true),
                new Choice("Multiple-Choice 8 für Frage Nr." + 7, true)
        ), 8d));
        questions.add(new SingleChoiceQuestion("<p>SingleChoice-Frage Nr." + 8 + "</p>", List.of(
                new Choice("Single-Choice 1 für Frage Nr." + 8, true),
                new Choice("Single-Choice 2 für Frage Nr." + 8, false),
                new Choice("Single-Choice 3 für Frage Nr." + 8, false),
                new Choice("Single-Choice 4 für Frage Nr." + 8, false),
                new Choice("Single-Choice 5 für Frage Nr." + 8, false),
                new Choice("Single-Choice 6 für Frage Nr." + 8, false),
                new Choice("Single-Choice 7 für Frage Nr." + 8, false),
                new Choice("Single-Choice 8 für Frage Nr." + 8, false),
                new Choice("Single-Choice 9 für Frage Nr." + 8, false)
        ), 9));
        questions.add(new MultipleChoiceQuestion("<p>Multiple-Choice Frage Nr." + 9 + "</p>", List.of(
                new Choice("Multiple-Choice 1 für Frage Nr." + 9, false),
                new Choice("Multiple-Choice 2 für Frage Nr." + 9, false),
                new Choice("Multiple-Choice 3 für Frage Nr." + 9, false),
                new Choice("Multiple-Choice 4 für Frage Nr." + 9, false),
                new Choice("Multiple-Choice 5 für Frage Nr." + 9, false),
                new Choice("Multiple-Choice 6 für Frage Nr." + 9, true),
                new Choice("Multiple-Choice 7 für Frage Nr." + 9, true),
                new Choice("Multiple-Choice 8 für Frage Nr." + 9, true),
                new Choice("Multiple-Choice 9 für Frage Nr." + 9, true),
                new Choice("Multiple-Choice 10 für Frage Nr." + 9, true)
        ), 10d));
        questions.add(new TextQuestion("<p>Freitext Frage Nr." + 10 + "</p>", 10));
        return questions;
    }

    private void generateExamineeAnswers(ArrayList<Question> questions, ArrayList<Examinee> examinees) {
        examinees.forEach(examinee -> {
            var answers = examinee.getAnswers();

            for(var question : questions){
                if(question.getType() == QuestionType.TEXT){
                    var textAnswer = new TextAnswer(question, "Textantwort");
                    answers.add(textAnswer);
                } else if(question.getType() == QuestionType.MULTIPLE_CHOICE) {
                    var choiceAnswer = new ChoiceAnswer(question);
                    choiceAnswer.getAnswers().set(0, true);
                    choiceAnswer.getAnswers().set(1, false);
                    choiceAnswer.getAnswers().set(2, false);
                    choiceAnswer.getAnswers().set(3, true);
                    answers.add(choiceAnswer);
                } else {
                    var choiceAnswer = new ChoiceAnswer(question);
                    choiceAnswer.getAnswers().set(0, true);
                    choiceAnswer.getAnswers().set(1, false);
                    choiceAnswer.getAnswers().set(2, false);
                    choiceAnswer.getAnswers().set(3, false);
                    answers.add(choiceAnswer);
                }
            }
        });
    }
}
