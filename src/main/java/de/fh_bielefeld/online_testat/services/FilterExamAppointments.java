package de.fh_bielefeld.online_testat.services;

import de.fh_bielefeld.online_testat.model.exam.ExamAppointment;
import de.fh_bielefeld.online_testat.repositories.ExamAppointmentRepository;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Service class to filter objects in the ExamAppointmentRepository
 *
 * @author Ole Niediek
 */
@Service
public class FilterExamAppointments {

    /**
     * Sorts ExamAppointments out, if their EndTime has been superseded
     * @param repository The repository, where the ExamAppointments are taken from
     * @return List<ExamAppointment> all the appointments, which are still timely
     */
    public List<ExamAppointment> filterAppointmentsForDate(ExamAppointmentRepository repository){
        List<ExamAppointment> examAppointments = new ArrayList<>();

        for(ExamAppointment appointment : repository.findAll()) {
            Duration duration = Duration.ofMinutes(appointment.getExam().getDuration());
            LocalDateTime appointmentEndDateTime = (LocalDateTime) duration.addTo(appointment.getPlannedStartTime());
            if(LocalDateTime.now().isBefore(appointmentEndDateTime)){
                examAppointments.add(appointment);
            }
        }
        return examAppointments;
    }

    /**
     * A check that returns if an exam can right now be taken or not
     * @param appointment The exam in question and its appointment to get plannedStartTime, actualStartTime and endTime from
     * @return boolean true means the exam can be taken
     */
    public boolean checkActualStartTime(ExamAppointment appointment){
        boolean showAppointment = false;
        LocalDateTime plannedStartTime = appointment.getPlannedStartTime();
        LocalDateTime actualStartTime = appointment.getActualStartTime();
        LocalDateTime endTime = ExamUtils.getAppointmentEndTime(plannedStartTime, appointment);

        if(actualStartTime != null)
            endTime = ExamUtils.getAppointmentEndTime(actualStartTime, appointment);

        if(LocalDateTime.now().isAfter(plannedStartTime) && LocalDateTime.now().isBefore(endTime) || LocalDateTime.now().isBefore(plannedStartTime))
            showAppointment = true;

        return showAppointment;
    }
}
