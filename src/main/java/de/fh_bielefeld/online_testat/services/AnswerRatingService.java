package de.fh_bielefeld.online_testat.services;

import de.fh_bielefeld.online_testat.model.answer.Answer;
import de.fh_bielefeld.online_testat.model.answer.ChoiceAnswer;
import de.fh_bielefeld.online_testat.model.question.Choice;
import de.fh_bielefeld.online_testat.model.question.ChoiceQuestion;
import de.fh_bielefeld.online_testat.model.question.MultipleChoiceQuestion;
import de.fh_bielefeld.online_testat.model.question.SingleChoiceQuestion;
import de.fh_bielefeld.online_testat.ui.utils.ExamUtils;
import org.springframework.stereotype.Service;

/**
 * Rates a multiple-choice question based on the number of correct answers given
 *
 * @author Lucie Jahn, Ole Niediek
 */
@Service
public class AnswerRatingService {
    /**
     * Checks how many of the boxes in a ChoiceAnswer of both types are checked correctly by the user
     * @param answer The answer in question
     */
    public void evaluate(Answer answer) {
        if (answer.getQuestion() instanceof ChoiceQuestion choiceQuestion) {
            int numberCorrectAnswersFromExaminee = 0;
            int selectedAnswers = 0;

            for (Choice choice : choiceQuestion.getChoices()) {
                boolean correctAnswer = choice.isCorrectAnswer();
                boolean examineeAnswer = ((ChoiceAnswer) answer).getAnswers().get(choiceQuestion.getChoices().indexOf(choice));

                /*For a SingleChoiceQuestion the method checks, if the chosen answer is the correct one
                  If it is the numberCorrectAnswersFromExaminee is set to maximum */
                if(answer.getQuestion() instanceof SingleChoiceQuestion singleChoiceQuestion && correctAnswer && examineeAnswer) {
                    numberCorrectAnswersFromExaminee = singleChoiceQuestion.getChoices().size();
                }

                /*For a MultipleChoiceQuestion the method checks, if a answer is correctly chosen and raises the counter accordingly
                  False answers, whose boxes are not checked count as correctly given answers as well */
                if(answer.getQuestion() instanceof MultipleChoiceQuestion && correctAnswer == examineeAnswer) {
                    numberCorrectAnswersFromExaminee++;
                }

                if(examineeAnswer)
                    selectedAnswers++;
            }
            if(selectedAnswers == 0)
                numberCorrectAnswersFromExaminee = 0;

            setPointsForExamineeAnswer(answer, numberCorrectAnswersFromExaminee, ((ChoiceQuestion) answer.getQuestion()).getChoices().size());
        } else {
            if(String.valueOf(ExamUtils.formatTextAnswer(answer)).equals("null") || String.valueOf(ExamUtils.formatTextAnswer(answer)).equals(""))
                answer.setPoints(0);
        }
    }

    /**
     * According to the numberCorrectAnswersFromExaminee the points for this exercise are being set
     * Maximum number means full points, only one below maximum means half points, everything below means zero
     * @param answer The answer in question
     * @param numberCorrectAnswersFromExaminee The count of correct answers the examinee has given to the question
     * @param size the maximum possible number of correctly given answers
     */
    public void setPointsForExamineeAnswer(Answer answer, int numberCorrectAnswersFromExaminee, int size) {
        if (numberCorrectAnswersFromExaminee == size-1 || size >= 8 && numberCorrectAnswersFromExaminee == size-2) {
            answer.setPoints((float) (answer.getQuestion().getMaxPoints() / 2));
        } else if (numberCorrectAnswersFromExaminee == size) {
            answer.setPoints((float) answer.getQuestion().getMaxPoints());
        } else {
            answer.setPoints(0);
        }
    }
}
