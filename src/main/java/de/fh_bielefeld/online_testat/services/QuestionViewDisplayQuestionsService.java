package de.fh_bielefeld.online_testat.services;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import de.fh_bielefeld.online_testat.model.answer.Answer;
import de.fh_bielefeld.online_testat.model.answer.ChoiceAnswer;
import de.fh_bielefeld.online_testat.model.answer.TextAnswer;
import de.fh_bielefeld.online_testat.model.question.Choice;
import de.fh_bielefeld.online_testat.model.question.Question;
import de.fh_bielefeld.online_testat.model.question.QuestionType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * A few methods used in the QuestionView mostly in regard to displaying the questions and answer windows
 *
 * @author Ole Niediek
 */
public class QuestionViewDisplayQuestionsService {
    /**
     * Since the order of questions in an exam can be random a new randomised List gets created
     * @param randomise Are the Questions supposed to be displayed in a random order or not
     * @param normalList The List<Question> in the correct order
     * @return The new List<Question> in a random order or just the old normalList
     */
    public List<Question> randomiseOrderOfQuestions(boolean randomise, List<Question> normalList){
        if(randomise) {
            List<Question> randomQuestions = new ArrayList<>();
            Random rand = new Random();
            while (randomQuestions.size() != normalList.size()) {
                Question ranQuestion = normalList.get(rand.nextInt(normalList.size()));
                if (!randomQuestions.contains(ranQuestion))
                    randomQuestions.add(ranQuestion);
            }
            return randomQuestions;
        }
        return normalList;
    }

    /**
     * A List of answers gets created as a local Object
     * The List resembles the List of questions from the exam both in length and structure
     * @param questions For every question an answer will be created
     */
    public List<Answer> setAnswerList(List<Question> questions){
        List<Answer> testAnswers = new ArrayList<>();
        for(Question question : questions){
            if(question.getType() == QuestionType.TEXT) {
                testAnswers.add(new TextAnswer(question));
            } else {
                testAnswers.add(new ChoiceAnswer(question));
            }
        }
        return testAnswers;
    }

    /**
     * Since the order, in which Checkboxes and RadioButtons are displayed, has to be random a new randomised List gets created
     * @param normalList The List<Choice> in the correct order
     * @return The new List<Choice> in a random order
     */
    public List<Choice> randomiseOrderOfChoices(boolean randomise, List<Choice> normalList){
        if(randomise) {
            List<Choice> randomChoices = new ArrayList<>();
            Random rand = new Random();
            while (randomChoices.size() != normalList.size()) {
                Choice ranChoice = normalList.get(rand.nextInt(normalList.size()));
                if (!randomChoices.contains(ranChoice))
                    randomChoices.add(ranChoice);
            }
            return randomChoices;
        }
        return normalList;
    }

    /**
     * Depending on the number of choices to a ChoiceQuestion the minHeight of the border can vary
     * @param size the number of choices to a ChoiceQuestion (should be between 2 and 10)
     * @return minHeight for the borderLayout
     */
    public String minHeightForBorder(int size){
        return switch (size) {
            case 2 -> "275px";
            case 3 -> "300px";
            case 4 -> "325px";
            case 6 -> "375px";
            case 7 -> "400px";
            case 8 -> "425px";
            case 9 -> "450px";
            case 10 -> "475px";
            default -> "350px";
        };
    }

    /**
     * If a question has been answered before by the user the previously selected Choices get added to the selectedList
     * Uses the List of Answers as reference
     * @param choiceList List of all possible Choices to a ChoiceQuestion
     * @param testAnswers List of all answers to a ChoiceQuestion
     * @param questionIndex The chosen ChoiceQuestion
     * @return List with all previously selected Choices
     */
    public List<Choice> getSelectedChoices(List<Choice> choiceList, List<Answer> testAnswers, int questionIndex){
        List<Choice> selectedList = new ArrayList<>();
        for(int i = 0; i < ((ChoiceAnswer) testAnswers.get(questionIndex)).getAnswers().size(); i++){
            if(((ChoiceAnswer) testAnswers.get(questionIndex)).getAnswers().get(i)){ // If the answer is true the question gets selected
                selectedList.add(choiceList.get(i));
            }
        }
        return selectedList;
    }

    /**
     * In the labelBar information about the current Question get displayed horizontally
     * @param question The current question
     * @param questionType "Single-Choice-Frage", "Multiple-Choice-Frage", or "Freitext-Frage"
     * @return Finished HorizontalLayout
     */
    public HorizontalLayout createLabelBar(Question question, String questionType){
        Label typeLabel = new Label(questionType);
        typeLabel.getStyle().set("font-size", "20px");
        Label pointsLabel = new Label(question.getMaxPoints() + " Punkte");
        pointsLabel.getStyle().set("font-size", "15px");
        HorizontalLayout labelBar = new HorizontalLayout(typeLabel, pointsLabel);
        labelBar.setMinWidth("100%");
        labelBar.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        labelBar.setAlignItems(FlexComponent.Alignment.CENTER);
        return labelBar;
    }

    /**
     * Since the questions can appear in a random order in the exam the order of answers will be random as well
     * This method makes sure, that the order of answers gets fixed
     * @param answers a list of answers, that might be in random order
     * @param questions the questions from the exam as a reference for the correct order
     */
    public List<Answer> unrandomiseAnswers(List<Answer> answers, List<Question> questions){
        List<Answer> answersInCorrectOrder = new ArrayList<>();
        for(Question question : questions){
            for(Answer answer : answers){
                if(answer.getQuestion() == question){
                    answersInCorrectOrder.add(answer);
                }
            }
        }
        return answersInCorrectOrder;
    }
}
