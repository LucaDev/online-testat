package de.fh_bielefeld.online_testat.services;

import de.fh_bielefeld.online_testat.repositories.ExamRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * Automatically deletes exams older than three months.
 * Runs every hour by default.
 *
 * @author Luca Kröger
 */
@Service
public class ExamCleanupService {
    private final ExamRepository examRepository;
    private final Logger logger = LoggerFactory.getLogger(ExamCleanupService.class);

    public ExamCleanupService(ExamRepository examRepository) {
        this.examRepository = examRepository;
    }

    @Scheduled(fixedDelay = 1, timeUnit = TimeUnit.HOURS)
    public void clean() {
        long examsRemoved = examRepository.deleteByCreateTimeBefore(LocalDateTime.now().minusMonths(3));

        if (examsRemoved > 0) {
            logger.info(examsRemoved + " exam(s) removed during automatic cleaning");
        }
    }
}
