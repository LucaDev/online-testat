package de.fh_bielefeld.online_testat.model;

import lombok.Getter;
import org.hibernate.Hibernate;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

/**
 * Extension of {@link DatabaseEntry} with an included unique ID
 * @author Luca Kröger
 */
@Getter
@MappedSuperclass
public abstract class IDedDatabaseEntry extends DatabaseEntry {
    /**
     * Represents an auto-generated unique ID.
     */
    @Id
    @GeneratedValue
    private Long ID;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        IDedDatabaseEntry that = (IDedDatabaseEntry) o;
        return ID != null && Objects.equals(ID, that.ID);
    }
}
