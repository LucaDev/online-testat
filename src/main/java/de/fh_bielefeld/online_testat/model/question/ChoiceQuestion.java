package de.fh_bielefeld.online_testat.model.question;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cascade;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * A choice question can either be a {@link SingleChoiceQuestion} or a {@link MultipleChoiceQuestion}.
 * It represents a list of choices the Examinee can choose from.
 *
 * @author Luca Kröger
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public abstract class ChoiceQuestion extends Question {
    /**
     * List of all choosable choices
     */
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @ToString.Exclude
    @JoinTable(name = "QUESTION_CHOICES") // Need to be explicitly set because of bug in JPA Buddys Flyway generator
    @NotEmpty(message = "Es müssen mindestens zwei Optionen gegeben sein")
    @Size(min = 2, message = "Es müssen mindestens zwei Optionen gegeben sein")
    private List<Choice> choices = new ArrayList<>();

    /**
     * @param question The text-based question the {@link de.fh_bielefeld.online_testat.model.exam.Examinee} need to answer
     * @param choices The list of {@link Choice}s the {@link de.fh_bielefeld.online_testat.model.exam.Examinee}
     *                will be able to choose from
     */
    public ChoiceQuestion(String question, List<Choice> choices, double points) {
        super(question, points);
        this.choices = choices;
    }
}
