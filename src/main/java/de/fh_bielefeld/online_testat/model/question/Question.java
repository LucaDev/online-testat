package de.fh_bielefeld.online_testat.model.question;

import de.fh_bielefeld.online_testat.model.IDedDatabaseEntry;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * Represents a question in a exam
 *
 * @author Luca Kröger
 */
@Entity
@Inheritance(
    strategy = InheritanceType.SINGLE_TABLE
)
@DiscriminatorColumn(name = "question_type")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public abstract class Question extends IDedDatabaseEntry {
    /**
     * Provides the question-text
     */
    @NotEmpty
    @Size(min = 5)
    @Lob
    @Column(columnDefinition = "text")
    private String questionHTML;

    /**
     * Maximum amount of points the question awards
     */
    @DecimalMin(value = "0.5")
    private double maxPoints;

    /**
     * @return {@link QuestionType} of the current question
     */
    public QuestionType getType() {
        if(this instanceof TextQuestion)
            return QuestionType.TEXT;
        else if (this instanceof MultipleChoiceQuestion)
            return QuestionType.MULTIPLE_CHOICE;
        else
            return QuestionType.SINGLE_CHOICE;
    }
}
