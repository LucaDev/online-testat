package de.fh_bielefeld.online_testat.model.question;

import de.fh_bielefeld.online_testat.model.IDedDatabaseEntry;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;

/**
 * Provides a choice for an {@link ChoiceQuestion}
 * @author Luca Kröger
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Choice extends IDedDatabaseEntry {
    /**
     * Contains a choices text
     */
    @NonNull
    @NotEmpty
    @Length(min = 1, max = 45)
    private String text;

    /**
     * Contains the right answer to a choice
     */
    private boolean correctAnswer;
}
