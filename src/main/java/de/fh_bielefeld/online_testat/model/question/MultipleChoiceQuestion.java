package de.fh_bielefeld.online_testat.model.question;

import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

/**
 * A MultipleChoiceQuestion is a {@link ChoiceQuestion} which allows multiple {@link Choice} to be correct.
 *
 * @author Luca Kröger
 */
@Entity
@DiscriminatorValue("multiple-choice")
@NoArgsConstructor
public class MultipleChoiceQuestion extends ChoiceQuestion {
    /**
     * {@inheritDoc}
     */
    public MultipleChoiceQuestion(String question, List<Choice> choices, Double points) {
        super(question, choices, points);
    }
}
