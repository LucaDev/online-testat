package de.fh_bielefeld.online_testat.model.question;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Provides all available Question Types
 * @author Luca Kröger
 */
@AllArgsConstructor
@Getter
public enum QuestionType {
    TEXT("Freitext"),
    MULTIPLE_CHOICE("Multiple-Choice"),
    SINGLE_CHOICE("Single-Choice");

    private final String name;

    public static QuestionType fromName(String name) {
        for(QuestionType type : values())
            if(type.getName().equalsIgnoreCase(name)) return type;
        throw new IllegalArgumentException();
    }
}
