package de.fh_bielefeld.online_testat.model.question;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * A TextQuestion is a {@link Question} which allows the Examinee to answer using freely written Text.
 *
 * @author Luca Kröger
 */
@Entity
@DiscriminatorValue("text")
public class TextQuestion extends Question {
    public TextQuestion(String question, double points) {
        super(question, points);
    }

    public TextQuestion() {
        super();
    }
}
