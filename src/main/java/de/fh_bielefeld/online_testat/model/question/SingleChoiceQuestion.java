package de.fh_bielefeld.online_testat.model.question;

import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

/**
 * A SingleChoiceQuestion is a {@link ChoiceQuestion} which allows only one {@link Choice} to be correct.
 *
 * @author Luca Kröger
 */
@Entity
@DiscriminatorValue("single-choice")
@NoArgsConstructor
public class SingleChoiceQuestion extends ChoiceQuestion {
    /**
     * {@inheritDoc}
     */
    public SingleChoiceQuestion(String question, List<Choice> choices, double points) {
        super(question, choices, points);
    }
}
