package de.fh_bielefeld.online_testat.model;

import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * Super class for all entities. Contains create and update timestamps for all database entries.
 * @author Luca Kröger
 */
@Getter
@MappedSuperclass
public abstract class DatabaseEntry {
    /**
     * Represents the initial creation time of the object
     */
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime createTime;

    /**
     * Represents the time the object was last updated
     */
    @UpdateTimestamp
    private LocalDateTime updateTime;
}
