package de.fh_bielefeld.online_testat.model.examiner;

import de.fh_bielefeld.online_testat.model.IDedDatabaseEntry;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.ui.security.Roles;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Represents an examiner (e.g. a professor) which is able to create {@link Exam}s.
 *
 * @author Luca Kröger
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
public class Examiner extends IDedDatabaseEntry implements UserDetails {
    /**
     * The unique username of an Examiner (case insensitive on login)
     */
    @NonNull
    @Length(min = 3, max = 32, message = "Der Benutzername muss zwischen 3 und 32 Zeichen lang sein")
    @Column(length = 32, unique = true)
    private String username;

    /**
     * The encoded password of an Examiner
     */
    @NonNull
    @NotEmpty
    private String password;

    /**
     * Decides whether the account is active or not. Defaults to true
     */
    private boolean enabled = true;

    /**
     * Decides if the Examiner is an admin (e.g. has the privileges to manage users)
     */
    private boolean admin = false;

    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @ToString.Exclude
    private List<Exam> exams = new ArrayList<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return isAdmin() ? List.of(Roles.getAuthority(Roles.ADMIN)) : List.of();
    }

    /**
     * Currently unused spring security fields
     */
    private boolean accountNonExpired = true;
    private boolean AccountNonLocked = true;
    private boolean CredentialsNonExpired = true;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Examiner examiner = (Examiner) o;
        return Objects.equals(username, examiner.username);
    }
}
