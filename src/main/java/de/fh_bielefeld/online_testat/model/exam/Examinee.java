package de.fh_bielefeld.online_testat.model.exam;

import com.opencsv.bean.CsvBindByPosition;
import de.fh_bielefeld.online_testat.model.IDedDatabaseEntry;
import de.fh_bielefeld.online_testat.model.answer.Answer;
import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an examinee (the individual who attends the exam)
 *
 * @author Luca Kröger
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
public class Examinee extends IDedDatabaseEntry {
    /**
     * The examinees matriculation number
     */
    @NonNull
    @NotNull(message = "Die Matrikelnummer darf nicht leer sein.")
    @CsvBindByPosition(position = 0)
    @Min(value = 1000000, message = "Die Matrikelnummer muss 7 Zeichen lang sein und mit einer 1 beginnen.")
    @Max(value = 1999999, message = "Die Matrikelnummer muss 7 Zeichen lang sein und mit einer 1 beginnen.")
    private Integer matriculationNumber;

    /**
     * The examinees name
     */
    @NonNull
    @NotEmpty(message = "Der Name darf nicht leer sein.")
    @Length(min = 3, max = 40, message = "Der Name muss zwischen 3 und 40 Zeichen besitzen.")
    @CsvBindByPosition(position = 1)
    private String name;

    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @ToString.Exclude
    private List<Answer> answers = new ArrayList<>();

    /**
     * Decides if a examinee was fully reviewed
     */
    private boolean isReviewed;

    public Examinee(int matriculationNumber, @NonNull String name) {
        this.matriculationNumber = matriculationNumber;
        this.name = name;
    }
}
