package de.fh_bielefeld.online_testat.model.exam;

import de.fh_bielefeld.online_testat.model.IDedDatabaseEntry;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a timespan (start-time and duration) for an {@link Exam} to be taken
 *
 * @author Luca Kröger
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@Setter
@ToString
public class ExamAppointment extends IDedDatabaseEntry {
    public ExamAppointment(@NonNull List<Examinee> examinees, @NonNull LocalDateTime plannedStartTime) {
        this.examinees = examinees;
        this.plannedStartTime = plannedStartTime;
    }

    @NonNull
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private List<Examinee> examinees = new ArrayList<>();

    /**
     * The planned start time
     */
    @NonNull
    @NotNull
    private LocalDateTime plannedStartTime;

    /**
     * The time the appointment actually started (after press on "Start" button)
     * null if not started
     */
    private LocalDateTime actualStartTime;

    @ManyToOne(optional = false)
    @JoinColumn(name = "exam_id")
    @Setter(AccessLevel.PACKAGE)
    private Exam exam;

    /**
     * true if the whole ExamAppointment was checked
     */
    private boolean fullyChecked;

    /**
     * @return true if the appointment has started yet; otherwise false
     */
    public boolean hasStarted() {
        return actualStartTime != null && actualStartTime.isBefore(LocalDateTime.now());
    }

    /**
     * @return true if the appointment is currently running; otherwise false
     */
    public boolean isRunning() {
        return hasStarted() && getEndTime().isAfter(LocalDateTime.now());
    }

    /**
     * @return end time of the exam (actual start time + duration)
     */
    public LocalDateTime getEndTime() {
        return actualStartTime.plusMinutes(getExam().getDuration());
    }

    /**
     * @return true if the appointment is over
     */
    public boolean isOver() {
        if (hasStarted()) {
            return getEndTime().isBefore(LocalDateTime.now());
        } else {
            return false;
        }
    }
}
