package de.fh_bielefeld.online_testat.model.exam;

import de.fh_bielefeld.online_testat.model.IDedDatabaseEntry;
import de.fh_bielefeld.online_testat.model.examiner.Examiner;
import de.fh_bielefeld.online_testat.model.question.Question;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an Exam created by an {@link Examiner} to be taken by an {@link Examinee}
 *
 * @author Luca Kröger
 */
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
public class Exam extends IDedDatabaseEntry {
    /**
     * The Exams display-name
     */
    @NonNull
    @NotEmpty(message = "Der Prüfungsname darf nicht leer sein.")
    @Length(min = 5, message = "Der Prüfungsname muss mindestens 5 Zeichen enthalten")
    private String name;

    /**
     * Represents all @{@link Question}
     */
    @NotEmpty(message = "Die Prüfung benötigt mindestens eine Frage.")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private List<Question> questions = new ArrayList<>();

    /**
     * Represents all @{link {@link ExamAppointment}s for a given Exam
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    @Setter(AccessLevel.PRIVATE)
    private List<ExamAppointment> appointments = new ArrayList<>();

    @NonNull
    @NotNull(message = "Eine Prüfung muss mindestens eine Minute lang sein")
    @Min(value = 1, message = "Eine Prüfung muss mindestens eine Minute lang sein")
    private int duration = 5;

    /**
     * Decides if a random question order is for each exam used or not
     */
    private boolean mixupQuestionsEnabled = false;

    /**
     * Decides if the choices in a {@link de.fh_bielefeld.online_testat.model.question.ChoiceQuestion} are
     * put in a random order for each exam
     */
    private boolean randomlyMixChoices = false;

    /**
     * true if the whole exam was checked
     */
    private boolean fullyChecked = false;

    public void addAppointment(ExamAppointment appointment) {
        appointment.setExam(this);

        if(!this.appointments.contains(appointment))
            this.appointments.add(appointment);
    }
}
