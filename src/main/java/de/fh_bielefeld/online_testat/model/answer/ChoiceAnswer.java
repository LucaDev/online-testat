package de.fh_bielefeld.online_testat.model.answer;

import de.fh_bielefeld.online_testat.model.question.Question;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.BitSet;

/**
 * {@inheritDoc}
 * @author Luca Kröger
 */
@ToString
@Getter
@Setter
@Entity
@NoArgsConstructor
@OnDelete(action = OnDeleteAction.CASCADE)
public class ChoiceAnswer extends Answer {
    @ToString.Exclude
    @Transient
    private BitSet answers = new BitSet();

    @Access(AccessType.PROPERTY)
    @Column(name = "answers")
    private byte[] getAnswersInDbRepresentation() {
        return answers.toByteArray();
    }

    private void setAnswersInDbRepresentation(byte[] data) {
        answers = BitSet.valueOf(data);
    }

    public ChoiceAnswer(Question question) {
        super(question);
    }
}
