package de.fh_bielefeld.online_testat.model.answer;

import de.fh_bielefeld.online_testat.model.question.Question;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;

/**
 * {@inheritDoc}
 * @author Luca Kröger
 */
@ToString
@Getter
@Setter
@Entity
@NoArgsConstructor
@OnDelete(action = OnDeleteAction.CASCADE)
public class TextAnswer extends Answer {
    private String answer;

    public TextAnswer(Question question, String answer) {
        super(question);
        this.answer = answer;
    }

    public TextAnswer(Question question) {
        super(question);
    }
}
