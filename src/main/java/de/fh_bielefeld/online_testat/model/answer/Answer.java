package de.fh_bielefeld.online_testat.model.answer;

import de.fh_bielefeld.online_testat.model.IDedDatabaseEntry;
import de.fh_bielefeld.online_testat.model.exam.Exam;
import de.fh_bielefeld.online_testat.model.question.Question;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * This represents an answer to a {@link Question} for an {@link Exam}
 *
 * @author Luca Kröger
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Answer extends IDedDatabaseEntry {
    /**
     * Provides the @{link {@link Question}} the answer is for
     */
    @NonNull
    @ManyToOne
    @JoinColumn(name = "question_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Question question;

    /**
     * Points gained by answering the {@link Question}
     * -1 = unanswered
     */
    private float points = -1;

    /**
     * @return true if the question was reviewed, false if not
     */
    public boolean isReviewed() {
        return points >= 0;
    }

    public boolean isAnswered() {
        if (this instanceof ChoiceAnswer choiceAnswer) {
            return choiceAnswer.getAnswers() != null && !choiceAnswer.getAnswers().isEmpty();
        } else if (this instanceof TextAnswer textAnswer) {
            return textAnswer.getAnswer()!= null && !textAnswer.getAnswer().isBlank();
        }

        return false;
    }
}
