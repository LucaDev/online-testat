INSERT INTO EXAMINER("ID",
                     "CREATE_TIME",
                     "UPDATE_TIME",
                     "ACCOUNT_NON_LOCKED",
                     "CREDENTIALS_NON_EXPIRED",
                     "ACCOUNT_NON_EXPIRED",
                     "ADMIN",
                     "ENABLED",
                     "PASSWORD",
                     "USERNAME"
            ) VALUES(
                    NEXT VALUE FOR HIBERNATE_SEQUENCE,
                    CURRENT_TIMESTAMP(),
                    CURRENT_TIMESTAMP(),
                    TRUE,
                    TRUE,
                    TRUE,
                    TRUE,
                    TRUE,
                    '{noop}password',
                    'admin'
            );
