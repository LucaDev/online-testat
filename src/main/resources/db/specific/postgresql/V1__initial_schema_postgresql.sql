CREATE SEQUENCE IF NOT EXISTS hibernate_sequence START WITH 1 INCREMENT BY 1;

CREATE TABLE answer
(
    id          BIGINT NOT NULL,
    create_time TIMESTAMP WITHOUT TIME ZONE,
    update_time TIMESTAMP WITHOUT TIME ZONE,
    question_id BIGINT,
    points      FLOAT,
    CONSTRAINT pk_answer PRIMARY KEY (id)
);

CREATE TABLE choice
(
    id             BIGINT NOT NULL,
    create_time    TIMESTAMP WITHOUT TIME ZONE,
    update_time    TIMESTAMP WITHOUT TIME ZONE,
    text           VARCHAR(255),
    correct_answer BOOLEAN,
    CONSTRAINT pk_choice PRIMARY KEY (id)
);

CREATE TABLE choice_answer
(
    id      BIGINT NOT NULL,
    answers BYTEA,
    CONSTRAINT pk_choiceanswer PRIMARY KEY (id)
);

CREATE TABLE exam
(
    id                      BIGINT NOT NULL,
    create_time             TIMESTAMP WITHOUT TIME ZONE,
    update_time             TIMESTAMP WITHOUT TIME ZONE,
    name                    VARCHAR(255),
    duration                INTEGER,
    mixup_questions_enabled BOOLEAN,
    fully_checked           BOOLEAN,
    CONSTRAINT pk_exam PRIMARY KEY (id)
);

CREATE TABLE exam_appointment
(
    id                 BIGINT NOT NULL,
    create_time        TIMESTAMP WITHOUT TIME ZONE,
    update_time        TIMESTAMP WITHOUT TIME ZONE,
    planned_start_time TIMESTAMP WITHOUT TIME ZONE,
    actual_start_time  TIMESTAMP WITHOUT TIME ZONE,
    exam_id            BIGINT NOT NULL,
    fully_checked      BOOLEAN,
    CONSTRAINT pk_examappointment PRIMARY KEY (id)
);

CREATE TABLE exam_appointment_examinees
(
    exam_appointment_id BIGINT NOT NULL,
    examinees_id        BIGINT NOT NULL
);

CREATE TABLE exam_appointments
(
    exam_id         BIGINT NOT NULL,
    appointments_id BIGINT NOT NULL
);

CREATE TABLE exam_questions
(
    exam_id      BIGINT NOT NULL,
    questions_id BIGINT NOT NULL
);

CREATE TABLE examinee
(
    id                   BIGINT NOT NULL,
    create_time          TIMESTAMP WITHOUT TIME ZONE,
    update_time          TIMESTAMP WITHOUT TIME ZONE,
    matriculation_number INTEGER,
    name                 VARCHAR(255),
    is_reviewed          BOOLEAN,
    CONSTRAINT pk_examinee PRIMARY KEY (id)
);

CREATE TABLE examinee_answers
(
    examinee_id BIGINT NOT NULL,
    answers_id  BIGINT NOT NULL
);

CREATE TABLE examiner
(
    id                      BIGINT NOT NULL,
    create_time             TIMESTAMP WITHOUT TIME ZONE,
    update_time             TIMESTAMP WITHOUT TIME ZONE,
    username                VARCHAR(32),
    password                VARCHAR(255),
    enabled                 BOOLEAN,
    account_non_expired     BOOLEAN,
    account_non_locked      BOOLEAN,
    credentials_non_expired BOOLEAN,
    admin                   BOOLEAN,
    CONSTRAINT pk_examiner PRIMARY KEY (id)
);

CREATE TABLE examiner_exams
(
    examiner_id BIGINT NOT NULL,
    exams_id    BIGINT NOT NULL
);

CREATE TABLE question
(
    id                   BIGINT NOT NULL,
    question_type        VARCHAR(31),
    create_time          TIMESTAMP WITHOUT TIME ZONE,
    update_time          TIMESTAMP WITHOUT TIME ZONE,
    questionhtml         TEXT,
    max_points           DOUBLE PRECISION,
    randomly_mix_choices BOOLEAN,
    CONSTRAINT pk_question PRIMARY KEY (id)
);

CREATE TABLE question_choices
(
    choice_question_id BIGINT NOT NULL,
    choices_id         BIGINT NOT NULL
);

CREATE TABLE text_answer
(
    id     BIGINT NOT NULL,
    answer VARCHAR(255),
    CONSTRAINT pk_textanswer PRIMARY KEY (id)
);

ALTER TABLE exam_appointment_examinees
    ADD CONSTRAINT uc_exam_appointment_examinees_examinees UNIQUE (examinees_id);

ALTER TABLE exam_appointments
    ADD CONSTRAINT uc_exam_appointments_appointments UNIQUE (appointments_id);

ALTER TABLE exam_questions
    ADD CONSTRAINT uc_exam_questions_questions UNIQUE (questions_id);

ALTER TABLE examinee_answers
    ADD CONSTRAINT uc_examinee_answers_answers UNIQUE (answers_id);

ALTER TABLE examiner_exams
    ADD CONSTRAINT uc_examiner_exams_exams UNIQUE (exams_id);

ALTER TABLE examiner
    ADD CONSTRAINT uc_examiner_username UNIQUE (username);

ALTER TABLE question_choices
    ADD CONSTRAINT uc_question_choices_choices UNIQUE (choices_id);

ALTER TABLE answer
    ADD CONSTRAINT FK_ANSWER_ON_QUESTION FOREIGN KEY (question_id) REFERENCES question (id) ON DELETE CASCADE;

ALTER TABLE choice_answer
    ADD CONSTRAINT FK_CHOICEANSWER_ON_ID FOREIGN KEY (id) REFERENCES answer (id);

ALTER TABLE exam_appointment
    ADD CONSTRAINT FK_EXAMAPPOINTMENT_ON_EXAM FOREIGN KEY (exam_id) REFERENCES exam (id);

ALTER TABLE text_answer
    ADD CONSTRAINT FK_TEXTANSWER_ON_ID FOREIGN KEY (id) REFERENCES answer (id);

ALTER TABLE examinee_answers
    ADD CONSTRAINT fk_exaans_on_answer FOREIGN KEY (answers_id) REFERENCES answer (id);

ALTER TABLE examinee_answers
    ADD CONSTRAINT fk_exaans_on_examinee FOREIGN KEY (examinee_id) REFERENCES examinee (id);

ALTER TABLE exam_appointments
    ADD CONSTRAINT fk_exaapp_on_exam FOREIGN KEY (exam_id) REFERENCES exam (id);

ALTER TABLE exam_appointments
    ADD CONSTRAINT fk_exaapp_on_exam_appointment FOREIGN KEY (appointments_id) REFERENCES exam_appointment (id);

ALTER TABLE exam_appointment_examinees
    ADD CONSTRAINT fk_exaappexa_on_exam_appointment FOREIGN KEY (exam_appointment_id) REFERENCES exam_appointment (id);

ALTER TABLE exam_appointment_examinees
    ADD CONSTRAINT fk_exaappexa_on_examinee FOREIGN KEY (examinees_id) REFERENCES examinee (id);

ALTER TABLE examiner_exams
    ADD CONSTRAINT fk_exaexa_on_exam FOREIGN KEY (exams_id) REFERENCES exam (id);

ALTER TABLE examiner_exams
    ADD CONSTRAINT fk_exaexa_on_examiner FOREIGN KEY (examiner_id) REFERENCES examiner (id);

ALTER TABLE exam_questions
    ADD CONSTRAINT fk_exaque_on_exam FOREIGN KEY (exam_id) REFERENCES exam (id);

ALTER TABLE exam_questions
    ADD CONSTRAINT fk_exaque_on_question FOREIGN KEY (questions_id) REFERENCES question (id);

ALTER TABLE question_choices
    ADD CONSTRAINT fk_quecho_on_choice FOREIGN KEY (choices_id) REFERENCES choice (id);

ALTER TABLE question_choices
    ADD CONSTRAINT fk_quecho_on_choice_question FOREIGN KEY (choice_question_id) REFERENCES question (id);
