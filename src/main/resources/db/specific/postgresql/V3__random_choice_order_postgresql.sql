ALTER TABLE question DROP COLUMN randomly_mix_choices;

ALTER TABLE exam ADD randomly_mix_choices BOOLEAN NOT NULL;
