INSERT INTO examiner("id",
                     "create_time",
                     "update_time",
                     "account_non_locked",
                     "credentials_non_expired",
                     "account_non_expired",
                     "admin",
                     "enabled",
                     "password",
                     "username"
            ) VALUES(
                    NEXTVAL('hibernate_sequence'),
                    CURRENT_TIMESTAMP,
                    CURRENT_TIMESTAMP,
                    TRUE,
                    TRUE,
                    TRUE,
                    TRUE,
                    TRUE,
                    '{noop}password',
                    'admin'
            );
