FROM eclipse-temurin:17
ADD /target/onlinetestat-1.0.0.jar /opt/app.jar
ENV TZ Europe/Berlin
ENV LC_ALL de_DE.UTF-8
RUN locale-gen de_DE.UTF-8
HEALTHCHECK CMD "curl --fail http://localhost:8080/actuator/health || exit 1"
ENTRYPOINT ["java", "-Xss512k", "-jar", "/opt/app.jar"]
