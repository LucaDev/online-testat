[![pipeline status](https://gitlab.com/LucaDev/online-testat/badges/master/pipeline.svg)](https://gitlab.com/LucaDev/online-testat/-/commits/master)
[![coverage report](https://gitlab.com/LucaDev/online-testat/badges/master/coverage.svg)](https://gitlab.com/LucaDev/online-testat/-/commits/master)
[![Latest Release](https://gitlab.com/LucaDev/online-testat/-/badges/release.svg)](https://gitlab.com/LucaDev/online-testat/-/releases)

# Online Testat
Dieses Tool entsteht im Rahmen des Softwareprojektes an der FH-Bielefeld.

Ziel ist es ein Online-Testat Werkzeug für Prüfer und Prüfende zu programmieren.
Die genauen Anforderungen sind dem PDF von Prof. Förster zu entnehmen

Eine aktuelle Instanz ist unter [online-testat.herokuapp.com](https://online-testat.herokuapp.com/) zu finden.
Die Deploymentsteuerung ist unter [Umgebungen - Heroku Prod](https://gitlab.com/LucaDev/online-testat/-/environments/10656799) zu finden.

## Entwicklung
Die Entwicklung und technische Dokumentation abseits von dieser Datei erfolgt in englischer Sprache.

### Eingestzte Frameworks und Libraries
- Spring Boot
- Spring Actuator
- Spring Security
- Vaadin - Web UI
- Lombok - Entfernung von Boilerplate-Code
- Flyway - Datenbankmigrationen

### Tools
- [Lokale Datenbank Konsole](http://localhost:8080/h2-console)
- [Spring Actuator](http://localhost:8080/actuator)
- [Aktuelle JavaDocs](https://lucadev.gitlab.io/online-testat/)

### Datenbanken
Folgende Datenbanktreiber werden standardmäßig mitgeliefert:
- H2
- PostgreSQL

### Beschleunigung der Entwicklung
Mit dem [HotswapAgent](http://hotswapagent.org/) in Kombination mit einem modifizierten JDK (siehe OnlineTestat + HotswapAgent runConfig) ist es möglich
Klassen vollständig auszutauschen und nicht nur methodeninhalte.

Eine Installationsanleitung für Java 17 ist [hier](http://hotswapagent.org/mydoc_quickstart-jdk17.html) zu finden
